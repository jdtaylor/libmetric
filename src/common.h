
/*
 * Common Stuff
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#ifndef METRIC_COMMON_H
#define METRIC_COMMON_H


#include <stdlib.h>
#include <string.h>

#define MAX_FNAME_LEN 255
#define MAX_LOG_LEN 1023
#define MAX_CONSOLE_MSG 128

#ifndef FALSE
enum { FALSE=0, TRUE=1 };
#endif

#ifndef NULL
#define NULL 0
#endif

#ifndef ASSERT
#include <assert.h>
#define ASSERT(test) assert(test)
#endif

/* I don't see a value.h on winblows */
#ifndef MINFLOAT
  #define MINFLOAT -3.40282347e+38F 
#endif
#ifndef MAXFLOAT
  #define MAXFLOAT 3.40282347e+38F 
#endif
#ifndef MINLONG
  #define MINLONG 0x80000000
#endif
#ifndef MAXLONG
  #define MAXLONG 0x7fffffff
#endif

// swap bytes in 'a' and return the result
#define BYTESWAP16(a) ((((a) & 0xFF) << 8) | (((a) & 0xFF00) >> 8))
#define BYTESWAP32(a) ((((a) & 0xFF) << 24) | \
		       (((a) & 0xFF00) << 8) | \
		       (((a) & 0xFF0000) >> 8) | \
		       (((a) & 0xFF000000) >> 24))
#define BYTESWAP32F(f) \
{ \
    char *b = (char*)&(f); \
    char temp; \
    temp = b[0]; \
    b[0] = b[4]; \
    b[4] = temp; \
    temp = b[1]; \
    b[1] = b[2]; \
    b[2] = temp; \
}

/* logging facilities
 */
enum {
	LOG_USER=0, // usually destined for user
	LOG_DUMP, // dumping of data for dubuging
	LOG_WARN, // warning.. routine should continue
	LOG_LOW, // application errors.. routine fails
	LOG_MED, // Not sure If I've use these two
	LOG_HIGH,
	LOG_SYS // system errors. uses "perror"
};

#ifndef NO_LOG
#  ifndef _WIN32
#    define VLOG_MSG(l,t,args...) vlogMsg(l,t,args)
#  else
     int VLOG_MSG(int l, const char *t, ...); /* MSVC has no vararg macros */
#  endif
#  define LOG_MSG(l,t) logMsg(l,t)
#else
#  ifndef _WIN32
#    define VLOG_MSG(l,t,args...) 1
#  else
     int VLOG_MSG(int l, const char *t, ...); /* MSVC has no vararg macros */
#  endif
#  define LOG_MSG(l,t) 1
#endif

#define MALLOC(s)	malloc(s)
#define FREE(s)		free(s)
#define REALLOC(s,i)	realloc(s,i)
#define CALLOC(n,s)	calloc(n,s)
#ifdef WIN32
#  define snprintf _snprintf
#  define vsnprintf _vsnprintf
#endif

/* OMG, this is crazy hack TODO btw */
#ifndef PATH_MAX
#define PATH_MAX 256
#endif

/* configure will insert any #defines after this comment */
/*##sub##*/
#define HAVE_METRIC_PIPES 1
#define HAVE_METRIC_THREADS 1
#define HAVE_METRIC_PIPES 1
#define HAVE_METRIC_THREADS 1
#define HAVE_METRIC_PIPES 1
#define HAVE_METRIC_THREADS 1


#ifdef __cplusplus
extern "C" {
#endif


char *Strncat(char *dst, const char *src, size_t dstSize); /* intuitive ver */
int Strntok(char *dst, char **src, char token, size_t dstSize);
int StrEscape(char *str, size_t strSize);
int Snprintf(char *dst, size_t dstSize, const char *format, ...);

typedef int (*logFunc)(char *logmsg);
int initLogCallback(logFunc func);
int logMsg(int type, const char *msg); /* sends msg to logFunc */
int vlogMsg(int type, char *format, ...);

int path2dir(char *path, char *dir); /* returns dir portion of a path */
int path2file(char *path, char *file); /* returns file portion */


#ifdef __cplusplus
}
#endif

#endif

