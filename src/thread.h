
/*
 * Library for handling process threads
 *
 * Copyright (c) 2001-2007,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 *         Rajiv Bakulesh Shah <brainix@gmail.com>
 */

#ifndef METRIC_THREAD_H
#define METRIC_THREAD_H

#ifdef WIN32
/*
 * Native win32 threads
 */
#include <windows.h>

typedef HANDLE thread_t;
typedef DWORD (*thread_entry_t)(LPVOID arg);
typedef HANDLE mutex_t;
typedef struct
{
	CRITICAL_SECTION lock; /* Lock to protect count, bcast. */
	int count;             /* Number of waiting threads. */
	HANDLE sema;           /* Queue of waiting threads. */
	HANDLE done;           /* Whether all waiting threads have woken up. */
	BOOL bcast;            /* TRUE if broadcasting, FALSE if signaling. */
} cond_t;

#else
/*
 * POSIX Threads
 */
#include <errno.h>
#include <pthread.h>

typedef pthread_t thread_t;
typedef void* (*thread_entry)(void *arg);
typedef pthread_mutex_t mutex_t;
typedef pthread_cond_t cond_t;

#endif

#ifdef __cplusplus
extern "C" {
#endif

int thread_create(thread_t *thread, thread_entry fnEntry, void *arg);
int thread_exit();
int thread_wait(thread_t *thread);

int mutex_init(mutex_t *mutex);
int mutex_try_lock(mutex_t *mutex);
int mutex_lock(mutex_t *mutex);
int mutex_unlock(mutex_t *mutex);
int mutex_destroy(mutex_t *mutex);

int cond_init(cond_t *cond, void *attr);
int cond_wait(cond_t *cond, mutex_t *mutex);
int cond_signal(cond_t *cond);
int cond_broadcast(cond_t *cond);
int cond_destroy(cond_t *cond);

#ifdef __cplusplus
}
#endif

#endif

