
/* LossyPriorityQueue.h
 *
 * James D. Taylor
 * james.d.taylor@gmail.com
 *
 */

#ifndef _LOSSY_PRIORITY_QUEUE_H
#define _LOSSY_PRIORITY_QUEUE_H

class LossyPriorityQueueEmptyException {};


template <class T>
class LossyPriorityQueue
{
public:
	/*!
	 * allocate n elements in queue.  The queue will not hold more
	 * than this.  any elements inserted that are greater than all the
	 * elements in the queue will be silently dropped.
	 */
	LossyPriorityQueue(unsigned int n);
	virtual ~LossyPriorityQueue();

	void alloc(unsigned int n);

	/*!
	 * Insert a copy of the object 'val' into the LossyPriorityQueue
	 * Requirements: queue should not be full.
	 */
	void push(T& val);

	/*!
	 * Removes and returns the smallest of the values
	 * contained in the LossyPriorityQueue.  I return by value to
	 * force the user to copy the data.  If the user had a
	 * reference to the object, then the object could change
	 * while still being held..
	 * Exception: throws a LossyPriorityQueueEmptyException if queue is empty.
	 */
	T pop();

	/*!
	 * returns reference of element on top of priority queue
	 */
	T& top();

	/*!
	 * returns true if queue is empty
	 */
	bool empty() const { return m_num ? false : true; }

private:
	unsigned int m_max;	// maximum number of elements we can store
	unsigned int m_num;	// current number of elements in queue
	T* m_pQueue;

	/*!
	 * corrects the heap by starting at 'pos' and moving 'up'.
	 * 'percolate_up' will bubble the value at 'pos' up until it
	 * meets the tree requirement that its parent is smaller than
	 * itself.
	 */
	void percolate_up(unsigned int pos);

	/*!
	 * corrects the heap by starting at 'pos' and moving down.
	 */
	void percolate_down(unsigned int pos);

	/*!
	 * removes the worst element in the queue and returns its position
	 * in the array.  If the 'val' is the worst, then it returns m_max.
	 */
	unsigned int find_worst(T &val);
	/*!
	 * purposely disbled copy-constructor and assignment operator to
	 * protect queue buffers.  (Law of Big Three)
	 */
	LossyPriorityQueue();
	LossyPriorityQueue(const LossyPriorityQueue&);
	const LossyPriorityQueue& operator=(const LossyPriorityQueue&);
};

template <class T>
LossyPriorityQueue<T>::LossyPriorityQueue(unsigned int n)
{
	if(n == 0)
		throw LossyPriorityQueueEmptyException();

	m_max = 0;
	m_num = 0;
	m_pQueue = NULL;
	alloc(n);
}

template <class T>
LossyPriorityQueue<T>::~LossyPriorityQueue()
{
	if(m_pQueue == NULL)
		return;
	delete[] m_pQueue;
}

template <class T>
void LossyPriorityQueue<T>::alloc(unsigned int n)
{
	if(m_pQueue != NULL)
		delete[] m_pQueue;

	m_max = n; // max objects is specified from the template

	m_pQueue = new T[m_max+1]; // +1 to skip zero index
	if(m_pQueue == NULL) {
		// allocation failed.  we should probably be throwing an
		// exception here, but it isn't part of the assignment.
		// setting max to zero will force an exception to be
		// thrown later on access.
		m_max = 0;
		return;
	}
}

template <class T>
void LossyPriorityQueue<T>::push(T& val)
{
	unsigned int pos;

	if(m_num >= m_max)
	{
		pos = find_worst(val);
		if(pos == m_max+1)
			return; // val is the worst of the leaves, don't insert
	}
	else
	{
		m_num++;
		pos = m_num;
	}

	m_pQueue[pos] = val; // insert 'val' at the end of array.
	percolate_up(pos);	// bubble new object up the tree.
}

template <class T>
T LossyPriorityQueue<T>::pop()
{
	if(m_num == 0)
		throw LossyPriorityQueueEmptyException();
	//
	// swap the first (root) buffer with the last
	//
	T val = m_pQueue[1];
	m_pQueue[1] = m_pQueue[m_num];
	m_pQueue[m_num] = val;
	m_num--;
	//
	// shift root node down until the tree is correct again.
	//
	percolate_down(1);

	return val; // return reference to smallest value
}

template <class T>
T& LossyPriorityQueue<T>::top()
{
	if(m_num == 0)
		throw LossyPriorityQueueEmptyException();
	return m_pQueue[1];
}

template <class T>
void LossyPriorityQueue<T>::percolate_up(unsigned int pos)
{
	unsigned int parent = pos / 2; // compiler should optimize to a shift

	T val = m_pQueue[pos];

	while(parent > 0 && val < m_pQueue[parent])
	{
		//
		// the parent is greater than pos.  swap
		//
		m_pQueue[pos] = m_pQueue[parent];
		pos = parent;
		parent /= 2; // get index of next parent
	}
	m_pQueue[pos] = val;  // suitable location for target node
}

template <class T>
void LossyPriorityQueue<T>::percolate_down(unsigned int pos)
{
	unsigned int child = pos * 2; // first child of pos 

	// avoid swapping by saving node until a valid position for it
	// is found.
	T val = m_pQueue[pos];

	//
	// shift smallest child up and repeat.
	//
	while(child <= m_num)
	{
		// determine which child to shift. (which is smaller)
		if(child+1 <= m_num && m_pQueue[child+1] < m_pQueue[child])
			child++;

		// do a single comparison with smaller child to see if we
		// can stop and leave pVal here.
		if(val < m_pQueue[child])
			break;

		// shift child up
		m_pQueue[pos] = m_pQueue[child];
		pos = child;
		child *= 2;
	}
	m_pQueue[pos] = val;
}

template <class T>
unsigned int LossyPriorityQueue<T>::find_worst(T &val)
{
	int i;
	unsigned int pos;
	T* pWorstVal = &val;
	unsigned int worstPos = m_max;
	//
	// compare all leaf nodes to 'val' and to each other to find the
	// smallest.  return its position unless 'val' is the largest.
	//
	int nLeaf = m_num - (m_num / 2) - 1;

	for(i = 0; i < nLeaf; i++)
	{
		pos = m_num - i;
		if(!(m_pQueue[pos] < *pWorstVal)) {
			worstPos = pos;
			pWorstVal = &m_pQueue[pos];
		}
	}
	if(pWorstVal == &val)
		return m_max+1;
	return worstPos;
}

#endif

