
/*
 * Common Stuff
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#include <metric/common.h>


/*!
 * Version of strncat
 *
 * CAUTION: this implementation is different from strncat!!!
 *
 *    Append "src" to the end of "dst".  "dstSize" is the size
 *    allocated to "dst" in bytes.  Strncat will not copy past
 *    the end of "dst".
 *
 * returns:
 * 	returns "dst"
 */
char *Strncat(char *dst, const char *src, size_t dstSize)
{
	char *cur = dst;
	char *end = dst + dstSize - 1;
	//
	// search for end of "dst"
	while(*cur && cur < end)
		cur++;
	//
	// start copying "src" onto the end of "dst"
	while(*src && cur < end)
		*(cur++) = *(src++); // copy and increment both pointers
	// 
	// terminate the string
	*cur = '\0';
	return dst;
}

/*!
 * Version of strtok()
 *
 *   splits "src" into substrings seperated by the "token" char.
 *   tokens are returned in "dst".  "dst" should be allocated "dstLen"
 *   bytes.  "&src" will point to the remaining string after "dst" has
 *   been removed.
 *
 * return -1
 *     The string returned in "dst" is the last sub-string
 * return 0
 *     "src" is NULL, when a -1 is returned it makes "src" NULL
 * return 1
 *     everything went ok and a valid sub-string was returned in "dst"
 */
int Strntok(char *dst, char **src, char token, size_t dstSize)
{
	char c;
	unsigned int i, j;
	dst[0] = '\0';
	if(*src == NULL)
		return 0;
	if(!(**src)) {
		*src = NULL;
		return 0; // no string returned
	}
	//
	// skip any tokens from the beginning of "src"
	i = 0;
	while((*src)[i]  &&  (*src)[i] == token)
		i++;
	if(!(*src)[i]) {
		// "src" is all token characters
		*src = NULL;
		return 0; // no string returned
	}
	//
	// search for first token in "src"
	j = 0;
	c = (*src)[i++];
	while(c  &&  j < dstSize  &&  c != token) {
		dst[j++] = c;
		c = (*src)[i++];
	}
	if(j < dstSize)
		dst[j] = '\0';
	else
		dst[dstSize-1] = '\0';
	if(!c) {
		*src = NULL;
		return -1; // last sub-string
	}
	// search ran into a token before end of "src" 
	// OR dst buffer has run out.  either way, we need to
	// update the src pointer to skip what we have already copied.
	*src = (*src) + i;
	if(!(**src)) {
		*src = NULL;
		return -1; // last sub-string
	}
	return 1; // more sub-strings left
}

int StrEscape(char *str, size_t strSize)
{
	unsigned int i;
	unsigned int bufPos;
	char szChar[5]; // "0x44\0"
	char *buf;
	buf = (char*)MALLOC(strSize);
	buf[strSize-1] = '\0';
	bufPos = 0;
	i = 0;
	while(str[i])
	{
		if(isprint((int)str[i]))
		{
			if(bufPos >= strSize-1)
				break;
			buf[bufPos++] = str[i];
			i++;
			continue;
		}
		if(bufPos >= strSize-5)
			break;
		snprintf(szChar, 5, "[%02x]", (unsigned int)str[i]);
		szChar[4] = '\0';
		buf[bufPos] = '\0';
		Strncat(buf, szChar, strSize);
		bufPos += 4;
		i++;
	}
	buf[bufPos] = '\0';
	strcpy(str, buf);
	FREE(buf);
	return 1;
}

int Snprintf(char *dst, size_t dstSize, const char *format, ...)
{
	int r;
	va_list vl;
	if(!dstSize) {
		LOG_MSG(LOG_LOW,"Snprintf - dstSize == 0");
		return -1;
	}
	va_start(vl, format);
	r = vsnprintf(dst, dstSize, format, vl);
	va_end(vl);
	dst[dstSize-1] = '\0'; // if r < 0, this is necessary
	return r;
}

int path2dir(char *path, char *dir)
{
	int l;
	char *p;
	/*    char buf[MAX_FNAME_LEN]; */

	/*
	   if(path[0] != '/')
	   {
	   Strncat(getcwd(buf, MAX_FNAME_LEN), "/", MAX_FNAME_LEN);
	   Strncat(buf, path, MAX_FNAME_LEN);
	   }
	 */
	dir[0] = '\0';
	if( (p = strrchr(path, '/')) == NULL)
		return 0;
	strncpy(dir, path, MAX_FNAME_LEN);
	dir[MAX_FNAME_LEN-1] = '\0';
	l = strlen(dir);
	l -= strlen(p);
	if(l < 0)
		return -1;
	dir[l+1] = '\0';

	return 1;
}

/* TODO this doesn't look finished */
int path2file(char *path, char *file)
{
	int l;
	l = strlen(path);
	return 1;
}

static logFunc g_logCallback = NULL;

int initLogCallback(logFunc func)
{
	g_logCallback = func;
	return 1;
}

/*
 * TODO:
 *   might be neat to have an irc interface in these
 *   next 2 functions... 
 */
int logMsg(int type, const char *msg)
{
	char buf[MAX_LOG_LEN];
	switch(type)
	{
	case LOG_USER:
		strcpy(buf,"[m ] "); /* msg */
		if(g_logCallback == NULL)
			return 0;
		Strncat(buf, msg, MAX_LOG_LEN);
		buf[MAX_LOG_LEN-1] = '\0';
		StrEscape(buf, MAX_LOG_LEN);
		(*g_logCallback)(buf);
		break;
	case LOG_DUMP:
		strcpy(buf,"[d ] "); /* msg */
		if(g_logCallback == NULL)
			return 0;
		Strncat(buf, msg, MAX_LOG_LEN);
		buf[MAX_LOG_LEN-1] = '\0';
		StrEscape(buf, MAX_LOG_LEN);
		(*g_logCallback)(buf);
		break;
	case LOG_WARN:
		strcpy(buf,"[w ] "); /* warning */ 
		Strncat(buf, msg, MAX_LOG_LEN);
		buf[MAX_LOG_LEN-1] = '\0';
		StrEscape(buf, MAX_LOG_LEN);
		fprintf(stderr, "%s\n", buf);
		if(g_logCallback == NULL)
			return 0;
		(*g_logCallback)(buf);
		break;
	case LOG_LOW:
		strcpy(buf,"[E1] "); /* error low */ 
		Strncat(buf, msg, MAX_LOG_LEN);
		buf[MAX_LOG_LEN-1] = '\0';
		StrEscape(buf, MAX_LOG_LEN);
		fprintf(stderr, "%s\n", buf);
		if(g_logCallback == NULL)
			return 0;
		(*g_logCallback)(buf);
		break;
	case LOG_MED:
		strcpy(buf,"[E2] "); /* error medium */ 
		Strncat(buf, msg, MAX_LOG_LEN);
		buf[MAX_LOG_LEN-1] = '\0';
		StrEscape(buf, MAX_LOG_LEN);
		fprintf(stderr, "%s\n", buf);
		if(g_logCallback == NULL)
			return 0;
		(*g_logCallback)(buf);
		break;
	case LOG_HIGH:
		strcpy(buf,"[E3] "); /* error high */ 
		Strncat(buf, msg, MAX_LOG_LEN);
		buf[MAX_LOG_LEN-1] = '\0';
		StrEscape(buf, MAX_LOG_LEN);
		fprintf(stderr, "%s\n", buf);
		if(g_logCallback == NULL)
			return 0;
		(*g_logCallback)(buf);
		break;
	case LOG_SYS:
		strcpy(buf,"[ES] ");
		Strncat(buf, msg, MAX_LOG_LEN);
		buf[MAX_LOG_LEN-1] = '\0';
		StrEscape(buf, MAX_LOG_LEN);
		perror(buf);
		if(g_logCallback == NULL)
			return 0;
		(*g_logCallback)(buf);
		break;
	}
	return 1;
}

int vlogMsg(int type, char *format, ...)
{
	char buf[MAX_LOG_LEN];
	va_list vl;
	va_start(vl, format);
	vsnprintf(buf, MAX_LOG_LEN, format, vl);
	va_end(vl);
	buf[MAX_LOG_LEN-1] = '\0';
	logMsg(type, buf);
	return 1;
}

#ifdef _WIN32
int VLOG_MSG(int l, const char *t, ...)
{
	char buf[MAX_LOG_LEN];
	va_list vl;
	va_start(vl, t);
	vsnprintf(buf, MAX_LOG_LEN, t, vl);
	va_end(vl);
	buf[MAX_LOG_LEN-1] = '\0';
	logMsg(l, buf);
	return 1;
}
#endif


