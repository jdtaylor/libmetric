
/*
 * Cross-platform File Access Routines
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#ifndef WIN32
  #include <unistd.h>
  #include <sys/mman.h>
#endif
#include <string.h>
#include <metric/common.h>
#include <metric/fs.h>

#ifdef WIN32

int fileWrite(file_des fd, void *buf, int n)
{
	DWORD written;
	WriteFile(fd, buf, n, &written, NULL);
	return (int)written;
}

int fileRead(file_des fd, void *buf, int n)
{
	DWORD read;
	ReadFile(fd, buf, n, &read, NULL);
	return (int)read;
}

int fileSeek(file_des fd, int n, int whence)
{
	DWORD rtrn;
	rtrn = SetFilePointer(fd, n, NULL, whence);
	return (int)rtrn;
}

void *fileMap(file_des fd, int offset, int len)
{
	HANDLE hMap;
	void *addr;
	hMap = CreateFileMapping(fd, NULL, PAGE_READONLY, 0, 0, NULL);
	if(hMap == NULL) {
		LOG_MSG(LOG_SYS,"fileMap - CreateFileMapping failed");
		return NULL;
	}
	addr = MapViewOfFile(hMap, FILE_MAP_READ, 0, offset, len);
	if(addr == NULL) {
		LOG_MSG(LOG_SYS,"fileMap - MapViewOfFile failed");
		return NULL;
	}
	return addr;
}

int fileUnMap(void *addr, int len)
{
	if(!UnmapViewOfFile(addr))
		return -1;
	return 1;
}

int fileClose(file_des fd)
{
	int i = CloseHandle(fd);
	if(!i)
		return -1;
	return 1;
}

#else

int fileWrite(file_des fd, void *buf, int n)
{
	return write(fd, buf, n);
}

int fileRead(file_des fd, void *buf, int n)
{
	return read(fd, buf, n);
}

int fileSeek(file_des fd, int n, int whence)
{
	return lseek(fd, n, whence);
}

void *fileMap(file_des fd, int offset, int len)
{
	return mmap(0, len, PROT_READ, MAP_PRIVATE, fd, offset);
}

int fileUnMap(void *addr, int len)
{
	if(munmap(addr, len) < 0) {
		LOG_MSG(LOG_SYS,"fileUnMap - munmap failed");
		return -1;
	}
	return 1;
}

int fileClose(file_des fd)
{
	int i = close(fd);
	if(i < 0) {
		VLOG_MSG(LOG_SYS,"fileClose - failed on %d", fd);
		return -1;
	}
	return 1;
}
#endif


