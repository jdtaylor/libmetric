
/*
 * Library for handling unicode character encodings
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#ifndef METRIC_CHAR_H
#define METRIC_CHAR_H

#include <metric/type.h>

enum {
	UCS4 = 0,
	UTF8,
	UTF16,
	MAX_UNICODE
};

#ifdef __cplusplus
extern "C" {
#endif

//! unicode_verify()
//    verifies 'nByte' bytes in 'stream' are valid unicode using
//    the encoding 'type'. e.g. UNICODE_UTF8.  If nByte is -1,
//    it will verify all elements until reaching 0x0.
//  returns:
//     > 0 if all characters are valid.
//     = 0 if at least one character is invalid
//     < 0 if an error has occured
int unicode_verify(void *stream, mUint32 type, mUint32 nElem);
int unicode_byteSwap(void *stream, mUint32 type, mUint32 nElem);
int unicode_encode(void *dst, mUint32 *src, mUint32 dstType, mUint32 nDstElem);
int unicode_decode(mUint32 *dst, void *src, mUint32 srcType, mUint32 nDstElem);
int unicode_cmp(void *stream1, void *stream2, mUint32 type, mUint32 nElem);
int unicode_cpy(void *pDst, const void *pSrc, mUint32 type, mUint32 nDstElem);
unsigned int unicode_len(void *stream, mUint32 type);

//! 
//	operations only on UCS4
//
int unicode_cat(mUint32 *pDst, mUint32 *pSrc, mUint32 nDstElem);
int unicode_tok(mUint32 *pDst, mUint32 **pSrc, mUint32 token, mUint32 nDstElem);

#ifdef __cplusplus
}
#endif

#endif


