
/*
 * Library for handling timed events.
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#include "metric/common.h"
#include "metric/mtime.h"

#ifdef HAVE_METRIC_SDL
  #include "SDL/SDL_timer.h"
#else
  #include <sys/time.h>
#endif

#define MAX_TIMER_QUEUE 10

/*
 * TODO: 
 * 	If you have time, replace the queue with a
 * 	"priority queue"/heap!
 */
struct taskNode {
	struct taskNode *next;
	timerTask_t task;
};

static struct taskNode *g_pQueue[MAX_TIMER_QUEUE];
static int g_bInitialized = FALSE;
#ifndef HAVE_METRIC_SDL
  static struct timeval g_startTime;
#endif

int timerSystemInit()
{
	int i;
	for(i = 0; i < MAX_TIMER_QUEUE; i++)
		g_pQueue[i] = NULL;
#ifdef HAVE_METRIC_SDL
	//SDL_StartTicks();
#else
	gettimeofday(&g_startTime, NULL);
#endif
	g_bInitialized = TRUE;
	return 1;
}

int timerSystemFree()
{
	int i;
	struct taskNode *pTask, *tempTask;
	if(!g_bInitialized)
		return 1;
	g_bInitialized = FALSE;
	//
	// free all queues
	for(i = 0; i < MAX_TIMER_QUEUE; i++)
	{
		if(g_pQueue[i] == NULL)
			continue;
		pTask = g_pQueue[i]->next;
		/* empty the queue */
		while(pTask != NULL) {
			tempTask = pTask;
			pTask = pTask->next;
			FREE(tempTask);
		}
		FREE(g_pQueue[i]);
		g_pQueue[i] = NULL;
	}
	return 1;
}

int timerTaskCopy(timerTask_t *pDst, timerTask_t *pSrc)
{
	pDst->fnTask = pSrc->fnTask;
	pDst->pData = pSrc->pData;
	pDst->time = pSrc->time;
	pDst->incr = pSrc->incr;
	return 1;
}

int timerTaskInit(timerTask_t *pTask)
{
	pTask->fnTask = NULL;
	pTask->pData = NULL;
	pTask->time = 0;
	pTask->incr = 0;
	return 1;
}

int timerQueueCreate()
{
	int i;
	if(!g_bInitialized) {
		timerSystemInit();
	}
	i = 0;
	while(i < MAX_TIMER_QUEUE && g_pQueue[i] != NULL)
		i++;
	if(i >= MAX_TIMER_QUEUE) {
		LOG_MSG(LOG_LOW,"timerQueueCreate - too many queues");
		return -1;
	}
	g_pQueue[i] = (struct taskNode*)MALLOC(sizeof(struct taskNode));
	if(g_pQueue[i] == NULL) {
		LOG_MSG(LOG_SYS,"timerQueueCreate - MALLOC failed");
		return -1;
	}
	g_pQueue[i]->next = NULL;
	timerTaskInit(&g_pQueue[i]->task);
	return i;
}

/*!
 * delete entire queue and all of the tasks that
 * are in it.
 */
int timerQueueDelete(int hQueue)
{
	struct taskNode *pTask, *tempTask;
	if(!g_bInitialized) {
		LOG_MSG(LOG_LOW,"timer - timer subsystem not initialized yet");
		return -1;
	}
	if(hQueue < 0 || hQueue >= MAX_TIMER_QUEUE) {
		LOG_MSG(LOG_LOW,"timerQueueDelete - invalid queue");
		return 1; /* task doesn't exist */
	}
	if(g_pQueue[hQueue] == NULL) {
		LOG_MSG(LOG_LOW,"timerQueueDelete - queue is NULL.. dude");
		return -1;
	}
	pTask = g_pQueue[hQueue]->next;
	/* empty the queue */
	while(pTask != NULL) {
		tempTask = pTask;
		pTask = pTask->next;
		FREE(tempTask);
	}
	FREE(g_pQueue[hQueue]);
	g_pQueue[hQueue] = NULL;
	return 1;
}

/*!
 * add task to "hQueue" with the following properties:
 *
 * alarmStart:	in how many milliseconds from now should
 * 		this task be started in.
 * alarmIncr:	delay in milliseconds between execution of tasks.
 * fnTask	function to call when task is executed
 * d1,d2,d3	parameters to pass the fnTask when called
 *
 * task won't go away until fnTask returns <= 0
 */
int timerTaskAdd(int hQueue, timerTask_t *pTask)
{
	struct taskNode *pTaskNode;
	if(!g_bInitialized) {
		LOG_MSG(LOG_LOW,"timer - timer subsystem not initialized yet");
		return -1;
	}
	if(hQueue < 0 || hQueue >= MAX_TIMER_QUEUE) {
		LOG_MSG(LOG_LOW,"timerTaskAdd - invalid hQueue");
		return 1; /* task doesn't exist */
	}
	pTaskNode = g_pQueue[hQueue];
	if(pTaskNode == NULL) {
		LOG_MSG(LOG_LOW,"timerTaskAdd - hQueue doesn't exist");
		return -1;
	}
	while(pTaskNode->next != NULL)
		pTaskNode = pTaskNode->next;
	pTaskNode->next = (struct taskNode*)MALLOC(sizeof(struct taskNode));
	if(pTaskNode->next == NULL) {
		LOG_MSG(LOG_SYS,"timerTaskAdd - MALLOC failed");
		return -1;
	}
	pTaskNode = pTaskNode->next;
	pTaskNode->next = NULL;
	timerTaskCopy(&pTaskNode->task, pTask);
	return 1;
}

/*! 
 * return generic time in milliseconds
 * (the value returned should only be used
 *  for comparing with other values that have
 *  been returned by this function)
 */
#ifdef HAVE_METRIC_SDL
long timerCurrent()
{
	long currentTime = 0;
	if(!g_bInitialized) {
		LOG_MSG(LOG_LOW,"timer - timer subsystem not initialized yet");
		return -1;
	}
	// TODO: undefined reference when win32 linking
	//currentTime = (long)SDL_GetTicks();
	return currentTime;
}
#else
long timerCurrent()
{
	long currentTime;
	struct timeval now;
	if(!g_bInitialized) {
		LOG_MSG(LOG_LOW,"timer - timer subsystem not initialized yet");
		return -1;
	}
	gettimeofday(&now, NULL);
	currentTime = (now.tv_sec - g_startTime.tv_sec) * 1000 +
		(now.tv_usec - g_startTime.tv_usec) / 1000;
	return currentTime;
}
#endif

/*! 
 * return how long (in milliseconds) till the
 * next task in the queue "hQueue".
 */
long timerNextAlarm(int hQueue)
{
	struct taskNode *pTaskNode;
	long nextAlarm = MAXLONG;
	if(!g_bInitialized) {
		LOG_MSG(LOG_LOW,"timer - timer subsystem not initialized yet");
		return -1;
	}
	if(hQueue < 0 || hQueue >= MAX_TIMER_QUEUE) {
		LOG_MSG(LOG_LOW,"timerNextAlarm - invalid hQueue");
		return -1; /* invalid task hQueue */
	}
	pTaskNode = g_pQueue[hQueue];
	if(pTaskNode == NULL) {
		LOG_MSG(LOG_LOW,"timerNextAlarm - hQueue doesn't exist");
		return -1;
	}
	if(pTaskNode->next == NULL)
		return -1; /* no tasks */
	while(pTaskNode->next != NULL) {
		pTaskNode = pTaskNode->next;
		if(pTaskNode->task.time + pTaskNode->task.incr < nextAlarm)
			nextAlarm = pTaskNode->task.time + pTaskNode->task.incr;
	}
	if(nextAlarm == MAXLONG)
		return -1; /* Well SHEEEET, I'll be damned.. */
	return nextAlarm;
}

/*!
 * execute all tasks, in "hQueue", that are scheduled to run now
 * or who's time has already passed
 */
int timerDispatch(int hQueue)
{
	struct taskNode *pTaskNode;
	struct taskNode *prevTaskNode;
	long currentTime;
	int status;
	if(!g_bInitialized) {
		LOG_MSG(LOG_LOW,"timer - timer subsystem not initialized yet");
		return -1;
	}
	if(hQueue < 0 || hQueue >= MAX_TIMER_QUEUE) {
		LOG_MSG(LOG_LOW,"timerDispatch - invalid hQueue");
		return 1; /* task doesn't exist */
	}
	pTaskNode = g_pQueue[hQueue];
	if(pTaskNode == NULL) {
		LOG_MSG(LOG_LOW,"timerDispatch - hQueue doesn't exist");
		return -1;
	}
	currentTime = timerCurrent(); /* loopaloofaloota! */
	while(pTaskNode->next != NULL)
	{
		prevTaskNode = pTaskNode;
		pTaskNode = pTaskNode->next;
		/* determine if this task needs to be called */
		if(currentTime > pTaskNode->task.time + pTaskNode->task.incr) {
			pTaskNode->task.time = currentTime;
			status = pTaskNode->task.fnTask(pTaskNode->task.pData);
			if(status <= 0) {
				/* delete this task from the queue */
				prevTaskNode->next = pTaskNode->next;
				FREE(pTaskNode);
				pTaskNode = prevTaskNode; /* don't mess with Texas */
			}
			if(status < 0) 
				return -1;
			/* continue to dispatch other tasks */
		}
	}
	return 1;
}

int timerIsQueue(int hQueue)
{
	if(hQueue < 0 || hQueue >= MAX_TIMER_QUEUE) {
		return 0;
	}
	return g_pQueue[hQueue] != NULL;
}


