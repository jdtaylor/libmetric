
/*
 * Library for PAK files
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <metric/pak.h>
#ifndef WIN32
#include <sys/mman.h>
#include <dirent.h>
#include <unistd.h>
#endif

/*
 * maximum number of pak files open at once. */
#define MAX_PAK_FILE 20

typedef struct {
	char name[MAX_FNAME_LEN]; /* filename of pakfile */
	/*caddr_t addr; */
	void *addr; /* starting address of mmaped area (start of node data)*/
	size_t len; /* length of mmaped area */
	file_des fd;   /* file descriptor of opened pak file */
	int foffset; /* offset of data relative to beginning of file */
} pakFile;

static pakFile *g_pakFile[MAX_PAK_FILE];

/*
 * header at the begining of every .pak file
 */
typedef struct {
	int size; /* size of this header */
	char pakName[MAX_PAKNAME_LEN]; /* generic name */
	int numNodes;  /* there should be a node header for each node */
} pakHeaderMain;

/*
 * header for every file contained in the pak file.
 * All pakHeaderNodes are stored at the begining of the .pak
 * file; right after the pakHeaderMain.
 */
typedef struct {
	int size; /* size of this header */
	char name[MAX_FNAME_LEN]; /* file name path w/ extension and dirs */
	ino_t inode;  /* inode of file on original filesystem (serial number)*/
	nodeType type; /* type of file. ie. bmp, mesh, mod.. */
	long offset;  /* offset relative to end of last node header */
	long len;    /* length of node relative to offset */
} pakHeaderNode;

/*
 * the data for the files is stored directly after
 * the last pakHeaderNode.
 */

#ifndef WIN32
static int recurseDirectories(char *path, int rootDirLen,
		int *currentNodeCount, /* incremented if file is appended */
		file_des fdNodeHeaders,/* file to append node headers to */
		file_des fdNodeData); /* file to append file data to */
static nodeType detectFileType(char *filename);
static int appendFile(file_des fdDst, char *srcFile);
static int getNodeByName(char *name, file_des pakFd, 
		pakHeaderNode *dstNode);
static int getNodeByInode(ino_t inode, file_des pakFd, 
		pakHeaderNode *dstNode);
static int writeNode(char *path, int rootDirLen, 
		file_des fdNodeHeader, file_des fdNodeData,
		pakHeaderNode *node, int *nodeCount);
static ino_t getInode(char *path);

/***********************************
 * start of function definitions
 */

int pakCreateFromDir(char *pakFile, char *dir, char *pakName)
{
	file_des fdNodeData, fdPak;
	int n;
	int nodeCount = 0;
	char tmpDataName[MAX_FNAME_LEN];
	pakHeaderMain headerMain;

	/* tmpDataName = tmpnam(NULL);
	 */
	if(path2dir(pakFile, tmpDataName) < 0) {
		LOG_MSG(LOG_HIGH,"pakCreateFromDir -- failed to extract pak dir name");
		return -1;
	}
	Strncat(tmpDataName, "tempDataThing", MAX_FNAME_LEN);
	/*
#ifdef WIN32
	fdNodeData = CreateFile(tmpDataName, GENERIC_READ | GENERIC_WRITE,
	FILE_SHARE_READ, NULL, CREATE_ALWAYS,
	FILE_ATTRIBUTE_TEMPORARY | FILE_FLAG_DELETE_ON_CLOSE,
	NULL);
	if(fdNodeData == INVALID_HANDLE_VALUE) {
	LOG_MSG(LOG_SYS,"pakCreateFromDir -- opening tmp file");
	return -1;
	}
	fdPak = CreateFile(pakFile, GENERIC_READ | GENERIC_WRITE,
	FILE_SHARE_READ, NULL, CREATE_ALWAYS,
	FILE_ATTRIBUTE_NORMAL, NULL);
	if(fdPak == INVALID_HANDLE_VALUE) {
	fileClose(fdNodeData);
	LOG_MSG(LOG_SYS,"pakCreateFromDir -- opening pak file");
	return -1;
}
#else
	 */
	if( (fdNodeData = open(tmpDataName, O_RDWR|O_CREAT|O_APPEND|O_TRUNC,
					S_IRUSR|S_IWUSR|S_IXUSR)) < 0) {
		LOG_MSG(LOG_SYS,"pakCreateFromDir -- opening tmp file");
		return -1;
	}
	if( (fdPak = open(pakFile, O_RDWR|O_CREAT|O_TRUNC,
					S_IRUSR|S_IWUSR|S_IXUSR)) < 0) {
		fileClose(fdNodeData);
		LOG_MSG(LOG_SYS,"pakCreateFromDir -- opening pak file");
		return -1;
	}
	/* #endif */
	headerMain.size = sizeof(pakHeaderMain);
	strncpy(headerMain.pakName, pakName, MAX_PAKNAME_LEN);
	headerMain.numNodes = 0;  /* we don't know this yet, so rewrite to file later */
	if((n=fileWrite(fdPak,&headerMain,headerMain.size)) != headerMain.size) {
		fileClose(fdNodeData);
		fileClose(fdPak);
		LOG_MSG(LOG_SYS,"pakCreateFromDir -- writing main header to pak file");
		return -1;
	}
	if(recurseDirectories(dir, strlen(dir), &nodeCount, fdPak, fdNodeData) < 0)
	{
		fileClose(fdNodeData); 
		fileClose(fdPak);
		LOG_MSG(LOG_HIGH,"pakCreateFromDir -- recurseDirectories failed");
		return -1;
	}
	headerMain.numNodes = nodeCount;
	fileSeek(fdPak, 0, FILE_SEEK_SET); /* position file offset to beginning */
	if((n=fileWrite(fdPak, &headerMain, headerMain.size)) != headerMain.size) {
		fileClose(fdNodeData); 
		fileClose(fdPak);
		LOG_MSG(LOG_SYS,"pakCreateFromDir -re-writing main header to pak file");
		return -1;
	}
	fileClose(fdNodeData);
	fileSeek(fdPak, 0, FILE_SEEK_END);
	appendFile(fdPak, tmpDataName); /* append data to the end of pak file */
	fileClose(fdPak);
	/*    #ifndef WIN32 */
	unlink(tmpDataName);
	/*    #endif */
	return 1;
}

int recurseDirectories(char *path, int rootDirLen, int *nodeCount,
						file_des fdNodeHeaders, file_des fdNodeData)
{
	struct stat   pathStat;
	struct stat	  headerStat;
	struct stat	  dataStat;
	struct dirent *dirp;
	DIR		  *dp;
	int		  ret;
	char	  *ptr;
	char	  realFile[MAX_FNAME_LEN];
	char	  realPath[MAX_FNAME_LEN];
	int		  n;
	pakHeaderNode node;
	pakHeaderNode tempNode;

	printf("%s\n", path);
	/* keep from reading our temp data file */
	if(strcmp(path, "tempDataThing") == 0) {
		LOG_MSG(LOG_WARN,"  -- tempDataThing kludge, skipping");
		return 1;
	}
	if(strcmp(path, ".pak") == 0) {/* hAck */
		LOG_MSG(LOG_WARN,"  -- .pak kludge, skipping");
		return 1;
	}
	if(lstat(path, &pathStat) < 0) {
		LOG_MSG(LOG_SYS,"recurseDirectories -- lstat pathStat");
		return -1;
	}
	if(fstat(fdNodeHeaders, &headerStat) < 0) {
		LOG_MSG(LOG_SYS,"recurseDirectories -- fstat headerStat");
		return -1;
	}
	if(fstat(fdNodeData, &dataStat) < 0) {
		LOG_MSG(LOG_SYS,"recurseDirectories -- fstat dataStat");
		return -1;
	}

	if(S_ISDIR(pathStat.st_mode)) { /* directory */
		ptr = path + strlen(path);
		*ptr++ = '/';
		*ptr = 0;
		if( (dp = opendir(path)) == NULL) {
			LOG_MSG(LOG_SYS,"recurseDirectories -- opening dir");
			return -1;
		}
		while( (dirp = readdir(dp)) != NULL) {
			if(strcmp(dirp->d_name, ".") == 0 ||
					strcmp(dirp->d_name, "..") == 0)
				continue;
			strcpy(ptr, dirp->d_name);
			ret = recurseDirectories(path, rootDirLen, nodeCount,
					fdNodeHeaders, fdNodeData);
		}
		ptr[-1] = 0;
		if(closedir(dp) < 0) {
			LOG_MSG(LOG_SYS,"recurseDirectories -- closing dir");
			return -1;
		}
		return ret;
	} else if(S_ISLNK(pathStat.st_mode)) { /* symbolic link */
		node.size = sizeof(pakHeaderNode);
		strcpy(node.name, path+rootDirLen);
		if( (n = readlink(path, realFile, MAX_FNAME_LEN)) < 0) {
			LOG_MSG(LOG_SYS,"recurseDirectories -- reading symlink");
			return -1;
		}
		realFile[n] = '\0';
		if(realFile[0] != '/') {
			path2dir(path, realPath);
			Strncat(realPath, "/", MAX_FNAME_LEN);
			Strncat(realPath, realFile, MAX_FNAME_LEN);
		}
		if(strlen(realPath) < (unsigned int)rootDirLen) {
			LOG_MSG(LOG_WARN,"  -- Linked file not in base tree, skipping");
			return -1;
		}
		node.type = detectFileType(realPath);
		node.inode = pathStat.st_ino;
		if(getNodeByInode(getInode(realPath), fdNodeHeaders, &tempNode) > 0) {
			node.offset = tempNode.offset;
			node.len = tempNode.len;
			lseek(fdNodeHeaders, 0, SEEK_END);
			if(write(fdNodeHeaders, &node, node.size) != node.size) {
				LOG_MSG(LOG_SYS,"recurseDirectories -- writing node header");
				return -1;
			}
			*nodeCount += 1;
		} else if(writeNode(realPath, rootDirLen, 
					fdNodeHeaders,fdNodeData,
					&tempNode, nodeCount) > 0) {
			node.offset = tempNode.offset;
			node.len = tempNode.len;
			lseek(fdNodeHeaders, 0, SEEK_END);
			if(write(fdNodeHeaders, &node, node.size) != node.size) {
				LOG_MSG(LOG_SYS,"recurseDirectories -- writing node header");
				return -1;
			}
			*nodeCount += 1;
		}
	} else if(S_ISREG(pathStat.st_mode)) { /* regular file */
		if(getNodeByInode(pathStat.st_ino, fdNodeHeaders, &tempNode) <= 0) {
			node.size = sizeof(pakHeaderNode);
			strcpy(node.name, path+rootDirLen);
			node.type = detectFileType(path);
			node.inode = pathStat.st_ino;
			node.offset = dataStat.st_size;
			node.len = pathStat.st_size;
			lseek(fdNodeHeaders, 0, SEEK_END);
			if(write(fdNodeHeaders, &node, node.size) != node.size) {
				LOG_MSG(LOG_SYS,"recurseDirectories -- writing node header");
				return -1;
			}
			if(appendFile(fdNodeData, path) < 0) {
				LOG_MSG(LOG_HIGH,"recurseDirectories -- appendFile failed");
				return -1;
			}
			*nodeCount += 1;
		}
	} else {
		LOG_MSG(LOG_WARN,"  -- Unknown file type");
		return -1;
	}

	return 1;
}

nodeType detectFileType(char *filename)
{
	char ext[5];
	int len;

	len = strlen(filename);
	if(len < 5)
		return NODE_GENERIC;
	strncpy(ext, filename+len-4, 4);
	ext[1] = tolower(ext[1]);
	ext[2] = tolower(ext[2]);
	ext[3] = tolower(ext[3]);
	ext[4] = '\0';

	if(strncmp(ext, ".bmp", 4) == 0)
		return NODE_BMP;
	if(strncmp(ext, ".png", 4) == 0)
		return NODE_PNG;
	if(strncmp(ext, ".jpg", 4) == 0)
		return NODE_JPG;
	if(strncmp(ext, ".fnt", 4) == 0)
		return NODE_FONT;
	if(strncmp(ext, ".3ds", 4) == 0)
		return NODE_3DS;
	if(strncmp(ext, ".ase", 4) == 0)
		return NODE_ASE;
	if(strncmp(ext, ".wav", 4) == 0)
		return NODE_WAV;
	if(strncmp(ext, ".mod", 4) == 0)
		return NODE_MOD;

	return NODE_GENERIC;
}


int appendFile(file_des fdDst, char *srcFile)
{
	struct stat	srcStat;
	char *buffer;
	int n, fdSrc;

	if( (fdSrc = open(srcFile, O_RDONLY)) < 0) {
		LOG_MSG(LOG_SYS,"appendFile -- opening src file");
		return -1;
	}
	if(fstat(fdSrc, &srcStat) < 0) {
		LOG_MSG(LOG_SYS,"appendFile -- fstat src file");
		return -1;
	}

	buffer = (char *)MALLOC(srcStat.st_blksize);
	lseek(fdDst, 0, SEEK_END);
	while( (n = read(fdSrc, buffer, srcStat.st_blksize)) > 0)
		if(write(fdDst, buffer, n) != n) {
			close(fdSrc);
			FREE(buffer);
			LOG_MSG(LOG_SYS,"appendFile -- writing data");
			return -1;
		}

	close(fdSrc);
	FREE(buffer);
	if(n < 0) {
		LOG_MSG(LOG_LOW,"  -- 0 bytes read");
		return -1;
	}
	return 1;
}

int pakPrintContents(char *pakfile)
{
	pakHeaderMain  mainHeader;
	pakHeaderNode  node;
	int pakFd;
	int i, headSize, seekOffset;

	headSize = sizeof(pakHeaderMain);
	if( (pakFd = open(pakfile, O_RDONLY)) < 0) {
		LOG_MSG(LOG_SYS,"pakPrintContents -- opening pakfile");
		return -1;
	}
	if(read(pakFd, &mainHeader, headSize) != headSize) {
		LOG_MSG(LOG_SYS,"pakPrintContents -- reading main header");
		return -1;
	}
	if(headSize != mainHeader.size)
		lseek(pakFd, mainHeader.size, SEEK_SET);
	headSize = sizeof(pakHeaderNode);
	if(read(pakFd, &node, headSize) != headSize) {
		LOG_MSG(LOG_SYS,"pakPrintContents -- reading node header");
		return -1;
	}
	if(headSize != node.size)
		seekOffset = node.size - headSize;
	else
		seekOffset = 0;
	lseek(pakFd, seekOffset, SEEK_CUR); 
	printf("%s - %ld - %ld\n", node.name, node.offset, node.len);
	for(i = 1; i < mainHeader.numNodes; i++) {
		if(read(pakFd, &node, headSize) != headSize) {
			LOG_MSG(LOG_SYS,"pakPrintContents -- reading node");
			return -1;
		}
		lseek(pakFd, seekOffset, SEEK_CUR);
		printf("%s - %ld - %ld\n", node.name, node.offset, node.len);
	}
	close(pakFd);
	return 1;
}

int getNodeByName(char *name, file_des pakFd, pakHeaderNode *dstNode)
{
	int headSize, seekOffset;
	pakHeaderMain mainHeader;

	headSize = sizeof(pakHeaderMain);
	lseek(pakFd, 0, SEEK_SET);
	if(read(pakFd, &mainHeader, headSize) != headSize) {
		LOG_MSG(LOG_SYS,"getNodeByName -- reading main header");
		return -1;
	}
	if(headSize != mainHeader.size)
		lseek(pakFd, mainHeader.size, SEEK_SET);
	headSize = sizeof(pakHeaderNode);
	if(read(pakFd, dstNode, headSize) != headSize)
		return 0; /* not an error if we havn't added any nodes yet */
	if(headSize != dstNode->size)
		seekOffset = dstNode->size - headSize;
	else
		seekOffset = 0;
	lseek(pakFd, seekOffset, SEEK_CUR); 
	if(strcmp(dstNode->name, name) == 0)
		return 1;

	while(read(pakFd, dstNode, headSize) == headSize) {
		lseek(pakFd, seekOffset, SEEK_CUR);
		if(strcmp(dstNode->name, name) == 0)
			return 1;
	}

	return 0;
}

int getNodeByInode(ino_t inode, file_des pakFd, pakHeaderNode *dstNode)
{
	int headSize, seekOffset;
	pakHeaderMain mainHeader;

	headSize = sizeof(pakHeaderMain);
	lseek(pakFd, 0, SEEK_SET);
	if(read(pakFd, &mainHeader, headSize) != headSize) {
		LOG_MSG(LOG_SYS,"getNodeByInode -- reading main header");
		return -1;
	}
	if(headSize != mainHeader.size)
		lseek(pakFd, mainHeader.size, SEEK_SET);
	headSize = sizeof(pakHeaderNode);
	if(read(pakFd, dstNode, headSize) != headSize)
		return 0; /* not an error if we havn't added any nodes yet */
	if(headSize != dstNode->size)
		seekOffset = dstNode->size - headSize;
	else
		seekOffset = 0;
	lseek(pakFd, seekOffset, SEEK_CUR); 
	if(dstNode->inode == inode)
		return 1;

	while(read(pakFd, dstNode, headSize) == headSize) {
		lseek(pakFd, seekOffset, SEEK_CUR);
		if(dstNode->inode == inode)
			return 1;
	}
	return 0;
}

int writeNode(char *path, int rootDirLen, 
		file_des fdNodeHeader, file_des fdNodeData,
		pakHeaderNode *node, int *nodeCount)
{
	struct stat	stLink;
	struct stat stNode;
	struct stat stData;
	if(stat(path, &stLink) < 0) {
		LOG_MSG(LOG_SYS,"writeNode -- stat path");
		return -1;
	}
	if(fstat(fdNodeHeader, &stNode) < 0) {
		LOG_MSG(LOG_SYS,"writeNode -- fstat header file");
		return -1;
	}
	if(fstat(fdNodeData, &stData) < 0) {
		LOG_MSG(LOG_SYS,"writeNode -- fstat data file");
		return -1;
	}

	strcpy(node->name, path+rootDirLen);
	node->size = sizeof(pakHeaderNode);
	node->type = detectFileType(path);
	node->inode = stLink.st_ino;
	node->offset = stData.st_size;
	node->len = stLink.st_size;
	lseek(fdNodeHeader, 0, SEEK_END);
	if(write(fdNodeHeader, node, node->size) != node->size) {
		LOG_MSG(LOG_SYS,"writeNode -- writing node header");
		return -1;
	}
	appendFile(fdNodeData, path);
	(*nodeCount)++;
	return 1;
}

ino_t getInode(char *path)
{
	struct stat	st;

	if(stat(path, &st) < 0) {
		LOG_MSG(LOG_SYS,"getInode -- stat path");
		return (ino_t)-1;
	}
	return st.st_ino;
}
#endif

#ifdef WIN32
int pakOpen(char *pak_name)
{
	int i;
	pakFile *pak;
	struct stat	pakStat;
	pakHeaderMain headerMain;
	pakHeaderNode headerNode;
	int headerSize;

	/* find an open pakFile slot */
	i = 0;
	while(i < MAX_PAK_FILE && g_pakFile[i] != NULL)
		i++;
	if(i >= MAX_PAK_FILE) {
		VLOG_MSG(LOG_LOW,"pakOpen - reached MAX_PAK_FILE of %d", MAX_PAK_FILE);
		return -1;
	}
	g_pakFile[i] = (pakFile*)MALLOC(sizeof(pakFile));
	if(g_pakFile[i] == NULL) {
		VLOG_MSG(LOG_SYS,"pakOpen - MALLOC failed on %s", pak_name);
		return -1;
	}
	pak = g_pakFile[i];
	strncpy(pak->name, pak_name, MAX_PAKNAME_LEN);
	VLOG_MSG(LOG_USER,"opening pak file: %s", pak_name);
	if(stat(pak_name, &pakStat) < 0) {
		VLOG_MSG(LOG_SYS,"pakOpen - stat failed on %s", pak_name);
		return -1;
	}
	pak->fd = CreateFile(pak_name, GENERIC_READ,
			FILE_SHARE_READ, NULL, OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL, NULL);
	if(pak->fd == INVALID_HANDLE_VALUE) {
		VLOG_MSG(LOG_SYS, "pakOpen - CreateFile failed on %s", pak_name);
		return -1;
	}
	headerSize = sizeof(pakHeaderMain);
	if(fileRead(pak->fd, &headerMain, headerSize) != headerSize) {
		LOG_MSG(LOG_SYS,"pakOpen -- failed reading main header");
		return -1;
	}
	fileSeek(pak->fd, headerMain.size-headerSize, FILE_SEEK_CURRENT);
	headerSize = sizeof(pakHeaderNode);
	if(fileRead(pak->fd, &headerNode, headerSize) != headerSize) {
		LOG_MSG(LOG_SYS,"pakOpen -- failed reading header node");
		return -1;
	}
	pak->foffset = headerMain.size + (headerNode.size * headerMain.numNodes);
	pak->len = pakStat.st_size;
	pak->addr = fileMap(pak->fd, 0, pak->len);
	if(pak->addr == (void*)-1) {
		LOG_MSG(LOG_SYS,"pakOpen -- mmap failed");
		return -1;
	}
	/* return the pakFile Id */
	return i;
}
#else
int pakOpen(char *pak_name)
{
	int i;
	pakFile *pak;
	struct stat	pakStat;
	pakHeaderMain headerMain;
	pakHeaderNode headerNode;
	int headerSize;

	/* find an open pakFile slot */
	i = 0;
	while(i < MAX_PAK_FILE && g_pakFile[i] != NULL)
		i++;
	if(i >= MAX_PAK_FILE) {
		VLOG_MSG(LOG_LOW,"pakOpen - reached MAX_PAK_FILE of %d", MAX_PAK_FILE);
		return -1;
	}
	g_pakFile[i] = (pakFile*)MALLOC(sizeof(pakFile));
	if(g_pakFile[i] == NULL) {
		VLOG_MSG(LOG_SYS,"pakOpen - MALLOC failed on %s", pak_name);
		return -1;
	}
	pak = g_pakFile[i];
	strncpy(pak->name, pak_name, MAX_PAKNAME_LEN);
	VLOG_MSG(LOG_USER,"opening pak file: %s", pak->name);
	if(stat(pak->name, &pakStat) < 0) {
		LOG_MSG(LOG_SYS,"pakOpen -- stat pakfile failed");
		return -1;
	}
	if((pak->fd = open(pak->name, O_RDONLY)) < 0) {
		LOG_MSG(LOG_SYS,"pakOpen -- opening pakfile failed");
		return -1;
	}
	headerSize = sizeof(pakHeaderMain);
	if(read(pak->fd, &headerMain, headerSize) != headerSize) {
		LOG_MSG(LOG_SYS,"pakOpen -- reading main header failed");
		return -1;
	}
	lseek(pak->fd, headerMain.size-headerSize, SEEK_CUR);
	headerSize = sizeof(pakHeaderNode);
	if(read(pak->fd, &headerNode, headerSize) != headerSize) {
		LOG_MSG(LOG_SYS,"pakOpen -- reading header node failed");
		return -1;
	}
	pak->foffset = headerMain.size + (headerNode.size * headerMain.numNodes);
	pak->len = pakStat.st_size;
	pak->addr = mmap(0, pak->len, PROT_READ, MAP_PRIVATE, pak->fd, 0);
	if(pak->addr == (void*)-1) {
		LOG_MSG(LOG_SYS,"pakOpen -- mmap failed");
		return -1;
	}
	/* return the pakFile Id */
	return i;
}
#endif

int pakClose(int pakId)
{
	pakFile *pak;
	int rtrn;
	if(pakId < 0 || pakId >= MAX_PAK_FILE) {
		VLOG_MSG(LOG_LOW,"pakClose - invalid pakId: %d", pakId);
		return -1;
	}
	pak = g_pakFile[pakId];
	if(pak == NULL) {
		VLOG_MSG(LOG_LOW,"pakClose - unknown pakId: %d", pakId);
		return -1;
	}
	VLOG_MSG(LOG_USER,"closing pak file: %s", pak->name);
	fileClose(pak->fd);
	rtrn = fileUnMap(pak->addr, pak->len);
	FREE(g_pakFile[pakId]);
	g_pakFile[pakId] = NULL;
	return rtrn;
}

void *pakGetResource(int pakId, const char *resName, int *len, nodeType *type)
{
	pakFile *pak;
	pakHeaderMain  mainHeader;
	pakHeaderNode  node;
	int i, headSize;
	char *ptr;

	if(pakId < 0 || pakId >= MAX_PAK_FILE) {
		VLOG_MSG(LOG_LOW,"pakGetResource - invalid pakId: %d", pakId);
		return NULL;
	}
	pak = g_pakFile[pakId];
	if(pak == NULL) {
		VLOG_MSG(LOG_LOW,"pakGetResource - unknown pakId: %d", pakId);
		return NULL;
	}
	ptr = (char*)pak->addr;
	*len = 0;
	headSize = sizeof(pakHeaderMain);
	memcpy(&mainHeader, ptr, headSize);
	ptr += mainHeader.size;
	headSize = sizeof(pakHeaderNode);
	memcpy(&node, ptr, headSize);
	ptr += node.size;
	if(strcmp(resName, node.name) == 0) {
		*len = node.len;
		*type = node.type;
		return ((char*)pak->addr) + pak->foffset + node.offset;
	}
	for(i = 1; i < mainHeader.numNodes; i++) {
		memcpy(&node, ptr, headSize);
		ptr += node.size;
		if(strcmp(resName, node.name) == 0) {
			*type = node.type;
			*len = node.len;
			return ((char*)pak->addr) + pak->foffset + node.offset;
		}
	}
	VLOG_MSG(LOG_LOW,"pakGetResource - failed to find: %s", resName);
	return NULL;
}

void nothing_is()
{
	getNodeByName(NULL, 0, NULL);
}


