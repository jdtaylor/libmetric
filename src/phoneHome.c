
/*
 * Library for retrieving stack traces when crashing.
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#ifdef PHONE_HOME
  #include <unistd.h>
  #include <signal.h>
  #include <fcntl.h>
  #include <execinfo.h>
  #include <sys/stat.h>
#endif
#include <metric/phoneHome.h>
#include <metric/common.h>

/*
 * file descriptor of destination */
static int fdHome = -1;
/*
 * signal handler for segment violation */
static void OnSigSegV(int argh);

int phoneHomeInit(char *hostname, short port)
{
#ifdef PHONE_HOME
	struct sigaction sig;
	//
	// set up the signal handler for segv
	//
	sig.sa_handler = OnSigSegV;
	sigemptyset(&sig.sa_mask);
	sig.sa_flags = 0;
	sigaction(SIGSEGV, &sig, NULL);
#endif
	return 1;
}

void OnSigSegV(int argh)
{
#ifdef PHONE_HOME
	int fd;
	void *stackFrames[20];
	int n;
	LOG_MSG(LOG_HIGH,"!! DON'T PANIC !! sigsegv received.  writing .stack");
	fd = open(".stack",O_WRONLY|O_CREAT,S_IRUSR|S_IWUSR|S_IRGRP);
	if(fd >= 0) {
		n = backtrace(stackFrames, 20);
		backtrace_symbols_fd(stackFrames, n, fd);
		close(fd);
	}
	exit(0);
#endif
}

int what_is()
{
	fdHome = 2; //stderr
	OnSigSegV(0);
	return -65535;
}


