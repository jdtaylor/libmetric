
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#include <metric/common.h>
#include <metric/set.h>

#define INT_INITIAL 32
#define GENERIC_MAGIC 0xDEAFC001

genericInfo_t genericInt = {
	size: sizeof(int),
	fnInit: NULL,
	fnAlloc: NULL,
	fnFree: NULL,
	fnCmp: genericIntCmp,
	fnCopy: NULL,
};

int genericInfoCopy(genericInfo_t *pDst, const genericInfo_t *pSrc)
{
	pDst->size = pSrc->size;
	pDst->fnInit = pSrc->fnInit;
	pDst->fnAlloc = pSrc->fnAlloc;
	pDst->fnFree = pSrc->fnFree;
	pDst->fnCmp = pSrc->fnCmp;
	pDst->fnCopy = pSrc->fnCopy;
	pDst->magic = pSrc->magic;
	return 1;
}

static int genMagicCheck(genericInfo_t *pInfo)
{
	int *debug_trap = 0;
	if(pInfo->magic != GENERIC_MAGIC) {
		LOG_MSG(LOG_LOW,"genMagicCheck - genericArray isn't initialized");
		debug_trap = (int*)*debug_trap;
	}
	return 1;
}

/*!
 * @Array ops
 */
int genericArrayInit(genericArray *pArray, const genericInfo_t *pInfo, 
					unsigned int nInitial, incrMethod_t incrMethod)
{
	genericInfoCopy(&pArray->element, pInfo);
	pArray->element.magic = GENERIC_MAGIC;
	pArray->array = NULL;
	pArray->flags = NULL;
	pArray->max = 0;
	pArray->num = 0;
	pArray->nInitial = nInitial;
	pArray->incrMethod = incrMethod;
	return 1;
}

int genericArrayAlloc(genericArray *pArray)
{
	unsigned int i;
	unsigned int elementSize = pArray->element.size;
	if(!genMagicCheck((genericInfo_t*)pArray))
		return -1;
	//
	// allocate inital number of generic element structures
	pArray->array = (unsigned char*)MALLOC(elementSize * pArray->nInitial);
	if(pArray->array == NULL) {
		LOG_MSG(LOG_SYS,"genericArrayAlloc - MALLOC failed");
		return -1;
	}
	//
	// allocated one flag set for each element
	pArray->flags = (unsigned int*)CALLOC(pArray->nInitial, sizeof(int));
	if(pArray->flags == NULL) {
		LOG_MSG(LOG_SYS,"genericArrayAlloc - CALLOC failed");
		return -1;
	}
	for(i = 0; i < pArray->nInitial; i++) {
		pArray->flags[i] = 0;  // implicitly turns off ELEMENT_USED
	}
	pArray->max = pArray->nInitial;
	pArray->num = 0;
	return 1;
}

int genericArrayFree(genericArray *pArray)
{
	if(!genMagicCheck((genericInfo_t*)pArray))
		return -1;
	if(pArray->array != NULL)
		FREE(pArray->array);
	if(pArray->flags != NULL)
		FREE(pArray->flags);
	pArray->array = NULL;
	pArray->flags = NULL;
	pArray->max = 0;
	pArray->num = 0;
	return 1;
}

int genericArrayAdd(genericArray *pArray, generic_t *pGeneric)
{
	void *genBuf;
	generic_t *pElement;
	unsigned int newMax;
	unsigned int elementSize = pArray->element.size;
	unsigned int i = 0;

	if(!genMagicCheck((genericInfo_t*)pArray))
		return -1;
	if(pArray->num >= pArray->max)
	{
		if(pArray->incrMethod == INCR_EXPONENTIAL) {
			// allocate new generic structures in an exponential fasion
			newMax = pArray->max * 2;
		} else {
			// allocate more generic structures in a linear fasion
			newMax = pArray->max + pArray->nInitial;
		}
		VLOG_MSG(LOG_DUMP,"genericArrayAdd - max: %d, newMax: %d.",
				pArray->max, newMax);
		genBuf = REALLOC(pArray->array, elementSize * newMax);
		if(genBuf == NULL) {
			LOG_MSG(LOG_SYS,"genericArrayAdd - REALLOC failed");
			return ELEMENT_NULL;
		}
		pArray->array = (unsigned char*)genBuf;
		//
		// allocate new flags for each new element
		genBuf = REALLOC(pArray->flags, newMax * sizeof(int));
		if(genBuf == NULL) {
			LOG_MSG(LOG_SYS,"genericArrayAdd - REALLOC failed");
			return ELEMENT_NULL;
		}
		pArray->flags = (unsigned int*)genBuf;
		//
		// initialize all the new elements
		//
		for(i = pArray->max; i < newMax; i++) {
			pElement = (generic_t*)(pArray->array + (i * elementSize));
			pArray->flags[i] = 0; // sets to unused
			if(pArray->element.fnInit != NULL)
				pArray->element.fnInit(pElement);
		}
		i = pArray->max; // reduce search time below
		pArray->max = newMax;
	}
	while(i < pArray->max && (pArray->flags[i] & ELEMENT_USED))
		i++;
	if(i >= pArray->max) {
		LOG_MSG(LOG_HIGH,"genericArrayAdd - couldn't find open slot");
		LOG_MSG(LOG_DUMP,"genericArray - dump: max: %d, num: %d");
		return ELEMENT_NULL;
	}
	pElement = (generic_t*)(pArray->array + (i * elementSize));
	if(pGeneric != NULL) {
		// copy pGeneric to pElement
		if(pArray->element.fnCopy == NULL) {
			memcpy(pElement, pGeneric, pArray->element.size);
		} else {
			if(pArray->element.fnCopy(pElement, pGeneric) < 0) {
				LOG_MSG(LOG_LOW,"genericArrayAdd - element's fnCopy failed");
				return ELEMENT_NULL;
			}
		}
	} else {
		if(pArray->element.fnInit != NULL)
			pArray->element.fnInit(pElement);
	}
	if(pArray->element.fnAlloc != NULL) {
		if(pArray->element.fnAlloc(pElement) < 0) {
			LOG_MSG(LOG_LOW,"genericArrayAdd - element's fnAlloc failed");
			return ELEMENT_NULL;
		}
	}
	pArray->flags[i] |= ELEMENT_USED;
	pArray->num++;
	return i;
}

int genericArrayDel(genericArray *pArray, unsigned int hGeneric)
{
	generic_t *pElement;
	unsigned int elementSize = pArray->element.size;
	if(!genMagicCheck((genericInfo_t*)pArray))
		return -1;
	if(hGeneric >= pArray->max)
		return -1; // no point in logging if you don't know where its from
	if(pArray->flags[hGeneric] & ELEMENT_USED)
	{
		pElement = (generic_t*)(pArray->array + (hGeneric * elementSize));
		if(pArray->element.fnFree)
			pArray->element.fnFree(pElement);
		if(pArray->element.fnInit)
			pArray->element.fnInit(pElement);
		pArray->flags[hGeneric] &= ~ELEMENT_USED;
		pArray->num--;
	}
	return 1;
}

generic_t *genericArrayGet(genericArray *pArray, unsigned int hGeneric)
{
	if(!genMagicCheck((genericInfo_t*)pArray))
		return NULL;
	if(hGeneric >= pArray->max)
		return NULL; // no point in logging if you don't know where its from
	if(!(pArray->flags[hGeneric] & ELEMENT_USED))
		return NULL;
	return (generic_t*)(pArray->array + (hGeneric * pArray->element.size));
}

generic_t *genericArrayIncr(genericArray *pArray, unsigned int *i)
{
	unsigned int ii = *i;
	if(!genMagicCheck((genericInfo_t*)pArray))
		return NULL;
	if(ii >= pArray->num)
		return NULL;
	//
	// skip unused elements
	while(ii < pArray->max && !(pArray->flags[ii] & ELEMENT_USED))
		ii++;
	if(ii >= pArray->max)
		return NULL;
	*i = ii+1; // increment
	return (generic_t*)(pArray->array + (ii * pArray->element.size));
}

int genericArrayFind(genericArray *pArray, generic_t *pGeneric)
{
	unsigned int i;
	generic_t *pElement;
	if(!genMagicCheck((genericInfo_t*)pArray))
		return ELEMENT_NULL;
	for(i = 0; i < pArray->max; i++)
	{
		if(!(pArray->flags[i] & ELEMENT_USED))
			continue;
		pElement = (generic_t*)(pArray->array + (i * pArray->element.size));
		if(pArray->element.fnCmp(pElement, pGeneric) == 0) {
			return i;
		}
	}
	return ELEMENT_NULL;
}

int genericArrayIns(genericArray *pArray, generic_t *pGen,
		unsigned int hGeneric)
{
	generic_t *pElement;
	unsigned int elementSize = pArray->element.size;

	if(!genMagicCheck((genericInfo_t*)pArray))
		return -1;
	if(hGeneric >= pArray->max) {
		VLOG_MSG(LOG_LOW,"genericArrayIns - hGeneric: %d >= pArray->max: %d.",
				hGeneric, pArray->max);
		LOG_MSG(LOG_LOW,"genericArrayIns - Use a hash table instead!");
		return -1;
	}
	if(pGen) {
		//
		// copy pGen into new elements position in array
		pElement = (generic_t*)(pArray->array + (hGeneric * elementSize));
		if(pArray->element.fnCopy == NULL) {
			memcpy(pElement, pGen, pArray->element.size);
		} else {
			if(pArray->element.fnCopy(pElement, pGen) < 0) {
				LOG_MSG(LOG_LOW,"genericArrayIns - element's fnCopy failed");
				return -1;
			}
		}
	}
	if(pArray->element.fnAlloc != NULL) {
		if(pArray->element.fnAlloc(pElement) < 0) {
			LOG_MSG(LOG_LOW,"genericArrayIns - element's fnAlloc failed");
			return -1;
		}
	}
	if((pArray->flags[hGeneric] & ELEMENT_USED) == 0) {
		// 
		// if this element wasn't previously used
		pArray->num++;
		pArray->flags[hGeneric] |= ELEMENT_USED;
	}
	return hGeneric;
}

int genericArrayCopy(genericArray *pDst, genericArray *pSrc)
{
	unsigned int i;
	generic_t *srcElement;
	generic_t *dstElement;
	if(!genMagicCheck((genericInfo_t*)pSrc))
		return -1;
	//genericArrayFree(pDst);
	genericInfoCopy(&pDst->element, &pSrc->element);
	pDst->max = pSrc->max;
	pDst->num = pSrc->num;
	pDst->nInitial = pSrc->nInitial;
	pDst->incrMethod = pSrc->incrMethod;
	if(pSrc->max == 0)
		return 1;
	pDst->array = (unsigned char*)MALLOC(pSrc->max * pSrc->element.size);
	if(pDst->array == NULL) {
		LOG_MSG(LOG_SYS,"genericArrayCopy - MALLOC failed");
		return -1;
	}
	if(pSrc->element.fnCopy == NULL) {
		//
		// no copy method, so just do a plain mem copy
		memcpy(pDst->array, pSrc->array, pSrc->max * pSrc->element.size);
	} else {
		//
		// call the fnCopy method for each element
		for(i = 0; i < pSrc->max; i++) {
			if(!(pSrc->flags[i] & ELEMENT_USED))
				continue;
			dstElement = (generic_t*)(pDst->array + (i * pSrc->element.size));
			srcElement = (generic_t*)(pSrc->array + (i * pDst->element.size));
			pDst->element.fnCopy(pDst, pSrc);
		}
	}
	pDst->flags = (unsigned int*)MALLOC(pSrc->max * sizeof(int));
	if(pDst->flags == NULL) {
		LOG_MSG(LOG_SYS,"genericArrayCopy - MALLOC failed");
		genericArrayFree(pDst);
		return -1;
	}
	memcpy(pDst->flags, pSrc->flags, pSrc->max * sizeof(int));
	return 1;
}

unsigned int genericArrayCount(genericArray *pArray)
{
	if(!genMagicCheck((genericInfo_t*)pArray))
		return 0;
	return pArray->num;
}


/*!
 * @IncrArray ops
 */

int genericIncrArrayInit(genericIncrArray *pArray, const genericInfo_t *pInfo, 
		unsigned int nInitial, incrMethod_t incrMethod)
{
	genericInfoCopy(&pArray->element, pInfo);
	pArray->element.magic = GENERIC_MAGIC;
	pArray->array = NULL;
	pArray->flags = NULL;
	pArray->used = NULL;
	pArray->max = 0;
	pArray->num = 0;
	pArray->nInitial = nInitial;
	pArray->incrMethod = incrMethod;
	return 1;
}

int genericIncrArrayAlloc(genericIncrArray *pArray)
{
	unsigned int i;
	unsigned int elementSize = pArray->element.size;

	if(!genMagicCheck((genericInfo_t*)pArray))
		return -1;
	//
	// allocate inital number of generic element structures
	pArray->array = (unsigned char*)MALLOC(elementSize * pArray->nInitial);
	if(pArray->array == NULL) {
		LOG_MSG(LOG_SYS,"genericIncrArrayAlloc - MALLOC failed");
		return -1;
	}
	//
	// allocated one flag set for each element
	pArray->flags = (unsigned int*)CALLOC(pArray->nInitial, sizeof(int));
	if(pArray->flags == NULL) {
		LOG_MSG(LOG_SYS,"genericIncrArrayAlloc - CALLOC failed");
		return -1;
	}
	pArray->used = (unsigned int*)MALLOC(pArray->nInitial * sizeof(int));
	if(pArray->array == NULL) {
		LOG_MSG(LOG_SYS,"genericIncrArrayAlloc - MALLOC failed");
		return -1;
	}
	for(i = 0; i < pArray->nInitial; i++) {
		pArray->flags[i] = 0;  // implicitly turns off ELEMENT_USED
	}
	pArray->max = pArray->nInitial;
	pArray->num = 0;
	return 1;
}

int genericIncrArrayFree(genericIncrArray *pArray)
{
	if(!genMagicCheck((genericInfo_t*)pArray))
		return -1;
	if(pArray->array != NULL)
		FREE(pArray->array);
	if(pArray->flags != NULL)
		FREE(pArray->flags);
	if(pArray->used != NULL)
		FREE(pArray->used);
	pArray->array = NULL;
	pArray->flags = NULL;
	pArray->used = NULL;
	pArray->max = 0;
	pArray->num = 0;
	return 1;
}

int genericIncrArrayAdd(genericIncrArray *pArray, generic_t *pGeneric)
{
	void *genBuf;
	generic_t *pElement;
	unsigned int newMax;
	unsigned int elementSize = pArray->element.size;
	unsigned int i = 0;

	if(!genMagicCheck((genericInfo_t*)pArray))
		return ELEMENT_NULL;
	if(pArray->num >= pArray->max)
	{
		if(pArray->incrMethod == INCR_EXPONENTIAL) {
			// allocate new generic structures in an exponential fasion
			newMax = pArray->max * 2;
		} else {
			// allocate more generic structures in a linear fasion
			newMax = pArray->max + pArray->nInitial;
		}
		VLOG_MSG(LOG_DUMP,"genericIncrArrayAdd - max: %d, newMax: %d.",
				pArray->max, newMax);
		genBuf = REALLOC(pArray->array, elementSize * newMax);
		if(genBuf == NULL) {
			LOG_MSG(LOG_SYS,"genericIncrArrayAdd - REALLOC failed");
			return ELEMENT_NULL;
		}
		pArray->array = (unsigned char*)genBuf;
		//
		// allocate new flags for each new element
		genBuf = REALLOC(pArray->flags, newMax * sizeof(int));
		if(genBuf == NULL) {
			LOG_MSG(LOG_SYS,"genericIncrArrayAdd - REALLOC failed");
			return ELEMENT_NULL;
		}
		pArray->flags = (unsigned int*)genBuf;
		//
		// allocate new used indices for each new element
		genBuf = REALLOC(pArray->used, newMax * sizeof(int));
		if(genBuf == NULL) {
			LOG_MSG(LOG_SYS,"genericIncrArrayAdd - REALLOC failed");
			return ELEMENT_NULL;
		}
		pArray->used = (unsigned int*)genBuf;
		//
		// initialize all the new elements
		//
		for(i = pArray->max; i < newMax; i++) {
			pElement = (generic_t*)(pArray->array + (i * elementSize));
			pArray->flags[i] = 0; // sets to unused
			if(pArray->element.fnInit != NULL)
				pArray->element.fnInit(pElement);
		}
		i = pArray->max; // reduce search time below
		pArray->max = newMax;
	}
	while(i < pArray->max && (pArray->flags[i] & ELEMENT_USED))
		i++;
	if(i >= pArray->max) {
		LOG_MSG(LOG_HIGH,"genericIncrArrayAdd - Impossible operation 1");
		return ELEMENT_NULL;
	}
	pElement = (generic_t*)(pArray->array + (i * elementSize));
	if(pGeneric) {
		if(pArray->element.fnCopy == NULL) {
			memcpy(pElement, pGeneric, pArray->element.size);
		} else {
			if(pArray->element.fnCopy(pElement, pGeneric) < 0) {
				LOG_MSG(LOG_LOW,"genericIncrArrayAdd - elements fnCopy failed");
				return ELEMENT_NULL;
			}
		}
	} else {
		if(pArray->element.fnInit != NULL)
			pArray->element.fnInit(pElement);
	}
	if(pArray->element.fnAlloc != NULL) {
		if(pArray->element.fnAlloc(pElement) < 0) {
			LOG_MSG(LOG_LOW,"genericIncrArrayAdd - element's fnAlloc failed");
			return ELEMENT_NULL;
		}
	}
	pArray->flags[i] |= ELEMENT_USED;
	pArray->used[pArray->num] = i;
	pArray->num++;
	return i;
}

int genericIncrArrayDel(genericIncrArray *pArray, unsigned int hGeneric)
{
	unsigned int i;
	generic_t *pElement;
	unsigned int elementSize = pArray->element.size;

	if(!genMagicCheck((genericInfo_t*)pArray))
		return -1;
	if(hGeneric >= pArray->max)
		return -1; // no point in logging if you don't know where its from
	if(pArray->flags[hGeneric] & ELEMENT_USED)
	{
		pElement = (generic_t*)(pArray->array + (hGeneric * elementSize));
		if(pArray->element.fnFree)
			pArray->element.fnFree(pElement);
		if(pArray->element.fnInit)
			pArray->element.fnInit(pElement);
		pArray->flags[hGeneric] &= ~ELEMENT_USED;
		i = 0;
		while(i < pArray->num) {
			if(pArray->used[i] == (unsigned int)hGeneric)
				break;
			i++;
		}
		// slide the rest of the array over used[i] to delete it
		while(i+1 < pArray->num) {
			pArray->used[i] = pArray->used[i+1];
			i++;
		}
		pArray->num--;
	}
	return 1;
}

generic_t *genericIncrArrayGet(genericIncrArray *pArray, unsigned int hGeneric)
{
	if(!genMagicCheck((genericInfo_t*)pArray))
		return NULL;
	if(hGeneric >= pArray->max)
		return NULL; // no point in logging if you don't know where its from
	if(!(pArray->flags[hGeneric] & ELEMENT_USED))
		return NULL;
	return (generic_t*)(pArray->array + (hGeneric * pArray->element.size));
}

generic_t *genericIncrArrayIncr(genericIncrArray *pArray, unsigned int *i)
{
	unsigned int ii = *i;
	unsigned int hGeneric;

	if(!genMagicCheck((genericInfo_t*)pArray))
		return NULL;
	if(ii >= pArray->num)
		return NULL;
	hGeneric = pArray->used[ii];
	if(hGeneric >= pArray->max || !(pArray->flags[hGeneric] & ELEMENT_USED))
		return NULL;
	(*i)++;
	return (generic_t*)(pArray->array + (hGeneric * pArray->element.size));
}


int genericIncrArrayFind(genericIncrArray *pArray, generic_t *pGeneric)
{
	generic_t *pElement;
	unsigned int i, hGeneric;
	unsigned int elementSize = pArray->element.size;

	i = 0;
	if(!genMagicCheck((genericInfo_t*)pArray))
		return -1;
	while(i < pArray->num) {
		hGeneric = pArray->used[i];
		pElement = (generic_t*)(pArray->array + (hGeneric * elementSize));
		if(pArray->element.fnCmp(pElement, pGeneric) == 0) {
			return i;
		}
		i++;
	}
	return ELEMENT_NULL;
}

int genericIncrArrayIns(genericIncrArray *pArray, generic_t *pGen,
		unsigned int hGeneric)
{
	generic_t *pElement;
	unsigned int elementSize = pArray->element.size;

	if(!genMagicCheck((genericInfo_t*)pArray))
		return -1;
	if(hGeneric >= pArray->max) {
		VLOG_MSG(LOG_LOW,"genericArrayIns - hGen: %d >= pArray->max: %d.",
				hGeneric, pArray->max);
		LOG_MSG(LOG_LOW,"genericArrayIns - Use a hash table instead!");
		return -1;
	}
	//
	// copy pGen into new elements position in array
	pElement = (generic_t*)(pArray->array + (hGeneric * elementSize));
	if(pGen) {
		if(pArray->element.fnCopy == NULL) {
			memcpy(pElement, pGen, pArray->element.size);
		} else {
			if(pArray->element.fnCopy(pElement, pGen) < 0) {
				LOG_MSG(LOG_LOW,"genericArrayIns - element's fnCopy failed");
				return -1;
			}
		}
	}
	if(pArray->element.fnAlloc != NULL) {
		if(pArray->element.fnAlloc(pElement) < 0) {
			LOG_MSG(LOG_LOW,"genericArrayIns - element's fnAlloc failed");
			return -1;
		}
	}
	if((pArray->flags[hGeneric] & ELEMENT_USED) == 0) {
		// 
		// this array position wasn't previously used
		pArray->flags[hGeneric] |= ELEMENT_USED;
		pArray->used[pArray->num] = hGeneric; // add new element to 'used' array
		pArray->num++;
	}
	return hGeneric;
}

int genericIncrArrayCopy(genericIncrArray *pDst, genericIncrArray *pSrc)
{
	unsigned int i;
	generic_t *srcElement;
	generic_t *dstElement;
	if(!genMagicCheck((genericInfo_t*)pSrc))
		return -1;
	//genericIncrArrayFree(pDst);
	genericInfoCopy(&pDst->element, &pSrc->element);
	pDst->max = pSrc->max;
	pDst->num = pSrc->num;
	pDst->nInitial = pSrc->nInitial;
	pDst->incrMethod = pSrc->incrMethod;
	if(pSrc->max == 0)
		return 1;
	pDst->array = (unsigned char*)MALLOC(pSrc->max * pSrc->element.size);
	if(pDst->array == NULL) {
		LOG_MSG(LOG_SYS,"genericIncrArrayCopy - MALLOC failed");
		return -1;
	}
	if(pSrc->element.fnCopy == NULL) {
		//
		// no copy method, so just do a plain mem copy
		memcpy(pDst->array, pSrc->array, pSrc->max * pSrc->element.size);
	} else {
		//
		// call the fnCopy method for each element
		for(i = 0; i < pSrc->max; i++) {
			if(!(pSrc->flags[i] & ELEMENT_USED))
				continue;
			dstElement = (generic_t*)(pDst->array + (i * pSrc->element.size));
			srcElement = (generic_t*)(pSrc->array + (i * pDst->element.size));
			pDst->element.fnCopy(pDst, pSrc);
		}
	}
	//
	// flag array
	pDst->flags = (unsigned int*)MALLOC(pSrc->max * sizeof(int));
	if(pDst->flags == NULL) {
		LOG_MSG(LOG_SYS,"genericIncrArrayCopy - MALLOC failed");
		genericIncrArrayFree(pDst);
		return -1;
	}
	memcpy(pDst->flags, pSrc->flags, pSrc->max * sizeof(int));
	//
	// used array
	pDst->used = (unsigned int*)MALLOC(pSrc->max * sizeof(int));
	if(pDst->used == NULL) {
		LOG_MSG(LOG_SYS,"genericIncrArrayCopy - MALLOC failed");
		genericIncrArrayFree(pDst);
		return -1;
	}
	memcpy(pDst->used, pSrc->used, pSrc->max * sizeof(int));
	return 1;
}

unsigned int genericIncrArrayCount(genericIncrArray *pArray)
{
	if(!genMagicCheck((genericInfo_t*)pArray))
		return 0;
	return pArray->num;
}

/*
 * @InsArray
 */

int genericInsArrayIns(genericIncrArray *pArray, generic_t *pGen,
		unsigned int hGeneric)
{
	void *genBuf;
	unsigned int i;
	unsigned int newMax;
	generic_t *pElement;
	unsigned int elementSize = pArray->element.size;
	if(!genMagicCheck((genericInfo_t*)pArray))
		return -1;
	if(hGeneric >= pArray->max)
	{
		if(pArray->incrMethod == INCR_EXPONENTIAL) {
			// allocate new generic structures in an exponential fasion
			newMax = pArray->max * 2;
		} else {
			// allocate more generic structures in a linear fasion
			newMax = pArray->max + pArray->nInitial;
		}
		// expand memory to fit new maximum index.
		// TODO: add sanity check on newMax
		if(newMax < hGeneric+1)
			newMax = hGeneric+1;
		VLOG_MSG(LOG_DUMP,"genericIncrArrayIns - max: %d, newMax: %d.",
				pArray->max, newMax);
		genBuf = REALLOC(pArray->array, elementSize * newMax);
		if(genBuf == NULL) {
			LOG_MSG(LOG_SYS,"genericInsArrayIns - REALLOC failed");
			return ELEMENT_NULL;
		}
		pArray->array = (unsigned char*)genBuf;
		//
		// allocate new flags for each new element
		genBuf = REALLOC(pArray->flags, newMax * sizeof(int));
		if(genBuf == NULL) {
			LOG_MSG(LOG_SYS,"genericInsArrayIns - REALLOC failed");
			return ELEMENT_NULL;
		}
		pArray->flags = (unsigned int*)genBuf;
		//
		// allocate new used indices for each new element
		genBuf = REALLOC(pArray->used, newMax * sizeof(int));
		if(genBuf == NULL) {
			LOG_MSG(LOG_SYS,"genericInsArrayIns - REALLOC failed");
			return ELEMENT_NULL;
		}
		pArray->used = (unsigned int*)genBuf;
		//
		// initialize all the new elements
		//
		for(i = pArray->max; i < newMax; i++) {
			pElement = (generic_t*)(pArray->array + (i * elementSize));
			pArray->flags[i] = 0; // sets to unused
			if(pArray->element.fnInit != NULL)
				pArray->element.fnInit(pElement);
		}
		pArray->max = newMax;
	}
	//
	// copy pGen into new elements position in array
	pElement = (generic_t*)(pArray->array + (hGeneric * elementSize));
	if(pGen) {
		if(pArray->element.fnCopy == NULL) {
			memcpy(pElement, pGen, pArray->element.size);
		} else {
			if(pArray->element.fnCopy(pElement, pGen) < 0) {
				LOG_MSG(LOG_LOW,"genericArrayIns - element's fnCopy failed");
				return -1;
			}
		}
	}
	if(pArray->element.fnAlloc != NULL) {
		if(pArray->element.fnAlloc(pElement) < 0) {
			LOG_MSG(LOG_LOW,"genericArrayIns - element's fnAlloc failed");
			return -1;
		}
	}
	if((pArray->flags[hGeneric] & ELEMENT_USED) == 0) {
		// 
		// this array position wasn't previously used
		pArray->flags[hGeneric] |= ELEMENT_USED;
		pArray->used[pArray->num] = hGeneric; // add new element to 'used' array
		pArray->num++;
	}
	return hGeneric;
}

/*!
 * @StaticQueue ops
 */
/*
   int genericStaticQueueInit(genericStaticQueue *pQ, const genericInfo_t *pInfo, 
   unsigned int nInitial, incrMethod_t incrMethod)
   {
   genericInfoCopy(&pQ->element, pInfo);
   pQ->array = NULL;
   pQ->max = 0;
   pQ->num = 0;
   pQ->first = 0;
   pQ->last = 0;
   pQ->nInitial = nInitial;
   pQ->incrMethod = incrMethod;
   return 1;
   }

   int genericStaticQueueAlloc(genericStaticQueue *pQ)
   {
   unsigned int i;
   unsigned int elementSize = pQ->element.size;
//
// allocate inital number of generic element structures
pQ->array = (unsigned char*)MALLOC(elementSize * pQ->nInitial);
if(pQ->array == NULL) {
LOG_MSG(LOG_SYS,"genericStaticQueueAlloc - MALLOC failed");
return -1;
}
pQ->max = pQ->nInitial;
pQ->num = 0;
pQ->first = 0;
pQ->last = 0;
return 1;
}

int genericStaticQueueFree(genericStaticQueue *pQ)
{
if(pQ->array != NULL)
FREE(pQ->array);
pQ->array = NULL;
pQ->max = 0;
pQ->num = 0;
pQ->first = 0;
pQ->last = 0;
return 1;
}

int genericStaticQueueAdd(genericStaticQueue *pQ, generic_t *pGeneric)
{
void *genBuf;
generic_t *pElement;
generic_t *pDst, *pSrc;
unsigned int newMax;
unsigned int elementSize = pQ->element.size;
unsigned int i;
if(pQ->num >= GENERIC_MAX) {
LOG_MSG(LOG_LOW,"genericStaticQueueAdd - reached GENERIC_MAX");
return -1;
}
if(pQ->num >= pQ->max) {
if(pQ->incrMethod == INCR_EXPONENTIAL) {
// allocate new generic structures in an exponentially
newMax = pQ->max * 2;
} else {
// allocate more generic structures in a linearly 
newMax = pQ->max + pQ->nInitial;
}
VLOG_MSG(LOG_DUMP,"genericStaticQueueAdd - max: %d, newMax: %d.",
pQ->max, newMax);
genBuf = REALLOC(pQ->array, elementSize * newMax);
if(genBuf == NULL) {
LOG_MSG(LOG_SYS,"genericStaticQueueAdd - REALLOC failed");
return ELEMENT_NULL;
}
pQ->array = (unsigned char*)genBuf;
if(pQ->num && pQ->last < pQ->first) {
	//
	// re-insert queue elements that are 'wrapped'
	for(i = 0; i < pQ->last; i++) {
		pSrc = (generic_t*)(pQ->array + (i * elementSize));
		pDst = (generic_t*)(pQ->array + ((pQ->max + i) * elementSize));
		if(pQ->element.fnCopy != NULL) {
			if(pQ->element.fnCopy(pDst, pSrc) < 0) {
				LOG_MSG(LOG_LOW,"genericStaticQueueAdd - fnCopy failed");
				memcpy(pDst, pSrc, elementSize);
			}
		} else {
			memcpy(pDst, pSrc, elementSize);
		}
	}
	pQ->last += pQ->max;
}
pQ->max = newMax;
}
i = pQ->last; // element idx to insert into
pElement = (generic_t*)(pQ->array + (i * elementSize));
if(pGeneric) {
	if(pQ->element.fnCopy == NULL) {
		memcpy(pElement, pGeneric, elementSize);
	} else {
		if(pQ->element.fnCopy(pElement, pGeneric) < 0) {
			LOG_MSG(LOG_LOW,"genericStaticQueueAdd - element fnCopy failed");
			return -1;
		}
	}
}
if(pQ->element.fnAlloc != NULL) {
	if(pQ->element.fnAlloc(pElement) < 0) {
		LOG_MSG(LOG_LOW,"genericStaticQueueAdd - element's fnAlloc failed");
		return -1;
	}
}
pQ->last = (pQ->last + 1) % pQ->max;
pQ->num++;
return i;
}

int genericStaticQueueDel(genericStaticQueue *pQ, unsigned int hGeneric)
{
	generic_t *pElement;
	unsigned int elementSize = pQ->element.size;
	if(hGeneric >= (int)pQ->max)
		return -1; // no point in logging if you don't know where its from
	if(pQ->flags[hGeneric] & ELEMENT_USED) {
		pElement = (generic_t*)(pQ->array + (hGeneric * elementSize));
		if(pQ->element.fnFree)
			pQ->element.fnFree(pElement);
		if(pQ->element.fnInit)
			pQ->element.fnInit(pElement);
		pQ->flags[hGeneric] &= ~ELEMENT_USED;
		pQ->num--;
	}
	return 1;
}

generic_t *genericStaticQueueGet(genericStaticQueue *pQ, unsigned int hGeneric)
{
	if(hGeneric >= (int)pQ->max)
		return NULL; // no point in logging if you don't know where its from
	if(!(pQ->flags[hGeneric] & ELEMENT_USED))
		return NULL;
	return (generic_t*)(pQ->array + (hGeneric * pQ->element.size));
}

generic_t *genericStaticQueueIncr(genericStaticQueue *pQ, unsigned int *i)
{
	unsigned int ii = *i;
	if(ii >= pQ->num)
		return NULL;
	//
	// skip unused elements
	while(ii < pQ->max && !(pQ->flags[ii] & ELEMENT_USED))
		ii++;
	*i = ii;
	if(ii >= pQ->max)
		return NULL;
	return (generic_t*)(pQ->array + (ii * pQ->element.size));
}

int genericStaticQueueFind(genericStaticQueue *pQ, generic_t *pGeneric)
{
	unsigned int i;
	generic_t *pElement;
	for(i = 0; i < pQ->max; i++) {
		if(!(pQ->flags[i] & ELEMENT_USED))
			continue;
		pElement = (generic_t*)(pQ->array + (i * pQ->element.size));
		if(pQ->element.fnCmp(pElement, pGeneric) == 0) {
			return i;
		}
	}
	return ELEMENT_NULL;
}

int genericStaticQueueIns(genericStaticQueue *pQ, generic_t *pGen,
		unsigned int hGeneric)
{
	generic_t *pElement;
	unsigned int elementSize = pQ->element.size;
	if(hGeneric >= pQ->max) {
		VLOG_MSG(LOG_LOW,"genericStaticQueueIns - hGen: %d >= pQ->max: %d.",
				hGeneric, pQ->max);
		LOG_MSG(LOG_LOW,"genericStaticQueueIns - Use a hash table instead!");
		return -1;
	}
	//
	// copy pGen into new elements position in array
	pElement = (generic_t*)(pQ->array + (hGeneric * elementSize));
	if(pGen) {
		if(pQ->element.fnCopy == NULL) {
			memcpy(pElement, pGen, pQ->element.size);
		} else {
			if(pQ->element.fnCopy(pElement, pGen) < 0) {
				LOG_MSG(LOG_LOW,"genericStaticQueueIns - element fnCopy failed");
				return -1;
			}
		}
	}
	if(pQ->element.fnAlloc != NULL) {
		if(pQ->element.fnAlloc(pElement) < 0) {
			LOG_MSG(LOG_LOW,"genericStaticQueueIns - element fnAlloc failed");
			return -1;
		}
	}
	if((pQ->flags[hGeneric] & ELEMENT_USED) == 0) {
		// 
		// if this element wasn't previously used
		pQ->num++;
		pQ->flags[hGeneric] |= ELEMENT_USED;
	}
	return 1;
}

int genericStaticQueueCopy(genericStaticQueue *pDst, genericStaticQueue *pSrc)
{
	unsigned int i;
	generic_t *srcElement;
	generic_t *dstElement;
	//genericStaticQueueFree(pDst);
	genericInfoCopy(&pDst->element, &pSrc->element);
	pDst->max = pSrc->max;
	pDst->num = pSrc->num;
	pDst->nInitial = pSrc->nInitial;
	pDst->incrMethod = pSrc->incrMethod;
	if(pSrc->max == 0)
		return 1;
	pDst->array = (unsigned char*)MALLOC(pSrc->max * pSrc->element.size);
	if(pDst->array == NULL) {
		LOG_MSG(LOG_SYS,"genericStaticQueueCopy - MALLOC failed");
		return -1;
	}
	if(pSrc->element.fnCopy == NULL) {
		//
		// no copy method, so just do a plain mem copy
		memcpy(pDst->array, pSrc->array, pSrc->max * pSrc->element.size);
	} else {
		//
		// call the fnCopy method for each element
		for(i = 0; i < pSrc->max; i++) {
			if(!(pSrc->flags[i] & ELEMENT_USED))
				continue;
			dstElement = (generic_t*)(pDst->array + (i * pSrc->element.size));
			srcElement = (generic_t*)(pSrc->array + (i * pDst->element.size));
			pDst->element.fnCopy(pDst, pSrc);
		}
	}
	pDst->flags = (unsigned int*)MALLOC(pSrc->max * sizeof(int));
	if(pDst->flags == NULL) {
		LOG_MSG(LOG_SYS,"genericStaticQueueCopy - MALLOC failed");
		genericStaticQueueFree(pDst);
		return -1;
	}
	memcpy(pDst->flags, pSrc->flags, pSrc->max * sizeof(int));
	return 1;
}
*/

/*!
 * @OctectTree ops
 */

#define OCTET(L1,L2,L3,L4) ((L1<<24)|(L2<<16)|(L3<<8)|L4)
#define OCTET1(oct) ((oct >> 24) & 0xFF)
#define OCTET2(oct) ((oct >> 16) & 0xFF)
#define OCTET3(oct) ((oct >> 8) & 0xFF)
#define OCTET4(oct) (oct & 0xFF)

int genericOctetTreeInit(genericOctetTree *pOct, const genericInfo_t *pInfo,
						unsigned int nInitial, incrMethod_t incrMode)
{
	int i;
	genericInfoCopy(&pOct->element, pInfo);
	pOct->element.magic = GENERIC_MAGIC;
	for(i = 0; i < 256; i++)
		pOct->base.pChild[i] = NULL;
	pOct->num = 0;
	return 1;
}

int genericOctetTreeAlloc(genericOctetTree *pOct)
{
	return 1;
}

int genericOctetTreeFree(genericOctetTree *pOct)
{
	int L1, L2, L3, L4; // level increments
	genericOctetTreeNode *p1, *p2, *p3;
	if(!genMagicCheck((genericInfo_t*)pOct))
		return -1;
	for(L1 = 0; L1 < 256; L1++)
	{
		if(pOct->base.pChild[L1] == NULL)
			continue;
		p1 = pOct->base.pChild[L1];
		for(L2 = 0; L2 < 256; L2++)
		{
			if(p1->pChild[L2] == NULL)
				continue;
			p2 = p1->pChild[L2];
			for(L3 = 0; L3 < 256; L3++)
			{
				if(p2->pChild[L3] == NULL)
					continue;
				p3 = p2->pChild[L3];
				for(L4 = 0; L4 < 256; L4++) {
					if(p3->pChild[L4] == NULL)
						continue;
					FREE(p3->pChild[L4]);
				}
				FREE(p3);
			}
			FREE(p2);
		}
		FREE(p1);
		pOct->base.pChild[L1] = NULL;
	}
	return 1;
}

//! TODO: this function will return < 0 ie. ERROR if the handle
// is too large.  I'm not sure what to do about it.
//
int genericOctetTreeAdd(genericOctetTree *pOct, generic_t *pGeneric)
{
	unsigned int L1, L2, L3, L4; // level increments
	genericOctetTreeNode *p1, *p2, *p3;
	generic_t *pElement;
	unsigned int szOct = sizeof(genericOctetTreeNode);
	unsigned int szEle = pOct->element.size;
	L1 = 0;
	if(!genMagicCheck((genericInfo_t*)pOct))
		return ELEMENT_NULL;
	while(L1 < 256 && pOct->base.pChild[L1] != NULL)
	{
		p1 = pOct->base.pChild[L1];
		L2 = 0;
		while(L2 < 256 && p1->pChild[L2] != NULL)
		{
			p2 = p1->pChild[L2];
			L3 = 0;
			while(L3 < 256 && p2->pChild[L3] != NULL)
			{
				p3 = p2->pChild[L3];
				L4 = 0;
				while(L4 < 256 && p3->pChild[L4] != NULL)
					L4++;
				if(p3->pChild[L4] == NULL)
				{
					pElement = MALLOC(szEle);
					if(pElement == NULL) {
						LOG_MSG(LOG_SYS,"genericOctetTreeAdd - MALLOC failed");
						return ELEMENT_NULL;
					}
					if(pGeneric) {
						if(pOct->element.fnCopy == NULL) {
							memcpy(pElement, pGeneric, pOct->element.size);
						} else {
							if(pOct->element.fnCopy(pElement, pGeneric) < 0) {
								LOG_MSG(LOG_LOW,"genericOctetTreeAdd - fnCopy");
								FREE(pElement);
								return ELEMENT_NULL;
							}
						}
					} else {
						if(pOct->element.fnInit)
							pOct->element.fnInit(pElement);
					}
					if(pOct->element.fnAlloc != NULL) {
						if(pOct->element.fnAlloc(pElement) < 0) {
							LOG_MSG(LOG_LOW,"genericOctetTreeAdd -Alloc failed");
							FREE(pElement);
							return ELEMENT_NULL;
						}
					}
					p3->pChild[L4] = (genericOctetTreeNode*)pElement;
					pOct->num++;
					return OCTET(L1,L2,L3,L4);
				}
				L3++;
			}
			if(p2->pChild[L3] == NULL)
			{
				p2->pChild[L3] = (genericOctetTreeNode*)MALLOC(szOct);
				if(p2->pChild[L3] == NULL) {
					LOG_MSG(LOG_SYS,"genericOctetTreeAdd - MALLOC failed");
					return ELEMENT_NULL;
				}
				p3 = p2->pChild[L3];
				for(L4 = 0; L4 < 256; L4++)
					p3->pChild[L4] = NULL;
				pElement = MALLOC(szEle);
				if(pElement == NULL) {
					LOG_MSG(LOG_SYS,"genericOctetTreeAdd - MALLOC failed");
					return ELEMENT_NULL;
				}
				if(pGeneric)
				{
					if(pOct->element.fnCopy == NULL) {
						memcpy(pElement, pGeneric, pOct->element.size);
					} else {
						if(pOct->element.fnCopy(pElement, pGeneric) < 0) {
							LOG_MSG(LOG_LOW,"genericOctetTreeAdd - Copy failed");
							FREE(pElement);
							return ELEMENT_NULL;
						}
					}
				} else {
					if(pOct->element.fnInit)
						pOct->element.fnInit(pElement);
				}
				if(pOct->element.fnAlloc != NULL) {
					if(pOct->element.fnAlloc(pElement) < 0) {
						LOG_MSG(LOG_LOW,"genericOctetTreeAdd -Alloc failed");
						FREE(pElement);
						return ELEMENT_NULL;
					}
				}
				p3->pChild[0] = (genericOctetTreeNode*)pElement;
				pOct->num++;
				return OCTET(L1,L2,L3,0);
			}
			L2++;
		}
		if(p1->pChild[L2] == NULL)
		{
			p1->pChild[L2] = (genericOctetTreeNode*)MALLOC(szOct);
			if(p1->pChild[L2] == NULL) {
				LOG_MSG(LOG_SYS,"genericOctetTreeAdd - MALLOC failed");
				return ELEMENT_NULL;
			}
			p2 = p1->pChild[0];
			for(L3 = 0; L3 < 256; L3++)
				p2->pChild[L3] = NULL;
			p2->pChild[0] = (genericOctetTreeNode*)MALLOC(szOct);
			if(p2->pChild[0] == NULL) {
				LOG_MSG(LOG_SYS,"genericOctetTreeAdd - MALLOC failed");
				return ELEMENT_NULL;
			}
			p3 = p2->pChild[0];
			for(L4 = 0; L4 < 256; L4++)
				p3->pChild[L4] = NULL;
			pElement = MALLOC(szEle);
			if(pElement == NULL) {
				LOG_MSG(LOG_SYS,"genericOctetTreeAdd - MALLOC failed");
				return ELEMENT_NULL;
			}
			if(pGeneric)
			{
				if(pOct->element.fnCopy == NULL) {
					memcpy(pElement, pGeneric, pOct->element.size);
				} else {
					if(pOct->element.fnCopy(pElement, pGeneric) < 0) {
						LOG_MSG(LOG_LOW,"genericOctetTreeAdd - Copy failed");
						FREE(pElement);
						return ELEMENT_NULL;
					}
				}
			} else {
				if(pOct->element.fnInit)
					pOct->element.fnInit(pElement);
			}
			if(pOct->element.fnAlloc != NULL) {
				if(pOct->element.fnAlloc(pElement) < 0) {
					LOG_MSG(LOG_LOW,"genericOctetTreeAdd -Alloc failed");
					FREE(pElement);
					return ELEMENT_NULL;
				}
			}
			p3->pChild[0] = (genericOctetTreeNode*)pElement;
			pOct->num++;
			return OCTET(L1,L2,0,0);
		}
		L1++;
	}
	if(pOct->base.pChild[L1] == NULL)
	{
		pOct->base.pChild[L1] = (genericOctetTreeNode*)MALLOC(szOct);
		if(pOct->base.pChild[L1] == NULL) {
			LOG_MSG(LOG_SYS,"genericOctetTreeAdd - MALLOC failed");
			return ELEMENT_NULL;
		}
		p1 = pOct->base.pChild[L1];
		for(L2 = 0; L2 < 256; L2++)
			p1->pChild[L2] = NULL;
		p1->pChild[0] = (genericOctetTreeNode*)MALLOC(szOct);
		if(p1->pChild[0] == NULL) {
			LOG_MSG(LOG_SYS,"genericOctetTreeAdd - MALLOC failed");
			return ELEMENT_NULL;
		}
		p2 = p1->pChild[0];
		for(L3 = 0; L3 < 256; L3++)
			p2->pChild[L3] = NULL;
		p2->pChild[0] = (genericOctetTreeNode*)MALLOC(szOct);
		if(p2->pChild[0] == NULL) {
			LOG_MSG(LOG_SYS,"genericOctetTreeAdd - MALLOC failed");
			return ELEMENT_NULL;
		}
		p3 = p2->pChild[0];
		for(L4 = 0; L4 < 256; L4++)
			p3->pChild[L4] = NULL;
		pElement = MALLOC(szEle);
		if(pElement == NULL) {
			LOG_MSG(LOG_SYS,"genericOctetTreeAdd - MALLOC failed");
			return ELEMENT_NULL;
		}
		if(pGeneric)
		{
			if(pOct->element.fnCopy == NULL) {
				memcpy(pElement, pGeneric, pOct->element.size);
			} else {
				if(pOct->element.fnCopy(pElement, pGeneric) < 0) {
					LOG_MSG(LOG_LOW,"genericOctetTreeAdd - Copy failed");
					FREE(pElement);
					return ELEMENT_NULL;
				}
			}
		} else {
			if(pOct->element.fnInit)
				pOct->element.fnInit(pElement);
		}
		if(pOct->element.fnAlloc != NULL) {
			if(pOct->element.fnAlloc(pElement) < 0) {
				LOG_MSG(LOG_LOW,"genericOctetTreeAdd -Alloc failed");
				FREE(pElement);
				return ELEMENT_NULL;
			}
		}
		p3->pChild[0] = (genericOctetTreeNode*)pElement;
		pOct->num++;
		return OCTET(L1,0,0,0);
	}
	return ELEMENT_NULL;
}

int genericOctetTreeDel(genericOctetTree *pOct, unsigned int hGeneric)
{
	unsigned char L1,L2,L3,L4;
	genericOctetTreeNode *p1, *p2, *p3;
	if(!genMagicCheck((genericInfo_t*)pOct))
		return -1;
	L1 = OCTET1(hGeneric);
	L2 = OCTET2(hGeneric);
	L3 = OCTET3(hGeneric);
	L4 = OCTET4(hGeneric);
	if(pOct->base.pChild[L1] != NULL) {
		p1 = pOct->base.pChild[L1];
		if(p1->pChild[L2] != NULL) {
			p2 = p1->pChild[L2];
			if(p2->pChild[L3] != NULL) {
				p3 = p2->pChild[L3];
				if(p3->pChild[L4] != NULL) {
					// TODO: call fnFree?
					FREE(p3->pChild[L4]);
					p3->pChild[L4] = NULL;
					pOct->num--;
				}
			}
		}
	}
	return 1;
}

int genericOctetTreeFind(genericOctetTree *pOct, generic_t *pGen)
{
	unsigned int L1, L2, L3, L4; // level increments
	genericOctetTreeNode *p1, *p2, *p3;
	if(!genMagicCheck((genericInfo_t*)pOct))
		return -1;
	if(pOct->element.fnCmp == NULL) {
		LOG_MSG(LOG_LOW,"genericOctetTreeFind - op requires fnCmp to be set");
		return -1;
	}
	//
	// x.0.0.0
	for(L1 = 0; L1 < 256; L1++)
	{
		if(pOct->base.pChild[L1] == NULL)
			continue;
		p1 = pOct->base.pChild[L1];
		//
		// x.x.0.0
		for(L2 = 0; L2 < 256; L2++)
		{
			if(p1->pChild[L2] == NULL)
				continue;
			p2 = p1->pChild[L2];
			//
			// x.x.x.0
			for(L3 = 0; L3 < 256; L3++)
			{
				if(p2->pChild[L3] == NULL)
					continue;
				p3 = p2->pChild[L3];
				//
				// x.x.x.x
				for(L4 = 0; L4 < 256; L4++)
				{
					if(p3->pChild[L4] == NULL)
						continue;
					if(!pOct->element.fnCmp(pGen, (generic_t*)p3->pChild[L4]))
						return OCTET(L1,L2,L3,L4);
				}
			}
		}
	}
	return ELEMENT_NULL;
}

int genericOctetTreeIns(genericOctetTree *pOct, generic_t *pGen,
		unsigned int hGeneric)
{
	int i;
	unsigned int L1,L2,L3,L4;
	generic_t *pElement;
	genericOctetTreeNode *p1, *p2, *p3;
	unsigned int szOct = sizeof(genericOctetTreeNode);
	unsigned int szEle = pOct->element.size;
	if(!genMagicCheck((genericInfo_t*)pOct))
		return -1;
	L1 = OCTET1(hGeneric);
	L2 = OCTET2(hGeneric);
	L3 = OCTET3(hGeneric);
	L4 = OCTET4(hGeneric);
	if(pOct->base.pChild[L1] == NULL) {
		pOct->base.pChild[L1] = (genericOctetTreeNode*)MALLOC(szOct);
		if(pOct->base.pChild[L1] == NULL) {
			LOG_MSG(LOG_SYS,"genericOctetTreeIns - MALLOC failed");
			return -1;
		}
		p1 = pOct->base.pChild[L1];
		for(i = 0; i < 256; i++)
			p1->pChild[i] = NULL;
	}
	p1 = pOct->base.pChild[L1];
	if(p1->pChild[L2] == NULL) {
		p1->pChild[L2] = (genericOctetTreeNode*)MALLOC(szOct);
		if(p1->pChild[L2] == NULL) {
			LOG_MSG(LOG_SYS,"genericOctetTreeIns - MALLOC failed");
			return -1;
		}
		p2 = p1->pChild[L2];
		for(i = 0; i < 256; i++)
			p2->pChild[i] = NULL;
	}
	p2 = p1->pChild[L2];
	if(p2->pChild[L3] == NULL) {
		p2->pChild[L3] = (genericOctetTreeNode*)MALLOC(szOct);
		if(p2->pChild[L3] == NULL) {
			LOG_MSG(LOG_SYS,"genericOctetTreeIns - MALLOC failed");
			return -1;
		}
		p3 = p2->pChild[L3];
		for(i = 0; i < 256; i++)
			p3->pChild[i] = NULL;
	}
	p3 = p2->pChild[L3];
	if(p3->pChild[L4] == NULL) {
		p3->pChild[L4] = (genericOctetTreeNode*)MALLOC(szEle);
		if(p3->pChild[L4] == NULL) {
			LOG_MSG(LOG_SYS,"genericOctetTreeIns - MALLOC failed");
			return -1;
		}
		pOct->num++;
	}
	pElement = (generic_t*)p3->pChild[L4];
	if(pGen)
	{
		if(pOct->element.fnCopy == NULL) {
			memcpy(pElement, pGen, pOct->element.size);
		} else {
			if(pOct->element.fnCopy(pElement, pGen) < 0) {
				LOG_MSG(LOG_LOW,"genericOctetTreeIns - element's fnCopy failed");
				FREE(pElement);
				p3->pChild[L4] = NULL;
				pOct->num--;
				return -1;
			}
		}
	} else {
		if(pOct->element.fnInit)
			pOct->element.fnInit(pElement);
	}
	if(pOct->element.fnAlloc != NULL) {
		if(pOct->element.fnAlloc(pElement) < 0) {
			LOG_MSG(LOG_LOW,"genericOctetTreeIns - element's fnAlloc failed");
			FREE(pElement);
			p3->pChild[L4] = NULL;
			pOct->num--;
			return -1;
		}
	}
	return hGeneric;
}

int genericOctetTreeCopy(genericOctetTree *pDst, genericOctetTree *pSrc)
{
	unsigned int L1, L2, L3, L4; // level increments
	genericOctetTreeNode *pSrc1, *pSrc2, *pSrc3;
	genericOctetTreeNode *pDst1, *pDst2, *pDst3;
	unsigned int szOct = sizeof(genericOctetTreeNode);
	unsigned int szEle = pSrc->element.size;
	if(!genMagicCheck((genericInfo_t*)pSrc))
		return -1;
	genericInfoCopy(&pDst->element, &pSrc->element);
	pDst->num = pSrc->num;
	//
	// x.0.0.0
	for(L1 = 0; L1 < 256; L1++)
	{
		if(pSrc->base.pChild[L1] == NULL) {
			pDst->base.pChild[L1] = NULL;
			continue;
		}
		pDst->base.pChild[L1] = (genericOctetTreeNode*)MALLOC(szOct);
		if(pDst->base.pChild[L1] == NULL) {
			LOG_MSG(LOG_SYS,"genericOctetTreeCopy - MALLOC failed");
			return -1;
		}
		pSrc1 = pSrc->base.pChild[L1];
		pDst1 = pDst->base.pChild[L1];
		//
		// x.x.0.0
		for(L2 = 0; L2 < 256; L2++)
		{
			if(pSrc1->pChild[L2] == NULL) {
				pDst1->pChild[L2] = NULL;
				continue;
			}
			pDst1->pChild[L2] = (genericOctetTreeNode*)MALLOC(szOct);
			if(pDst1->pChild[L2] == NULL) {
				LOG_MSG(LOG_SYS,"genericOctetTreeCopy - MALLOC failed");
				return -1;
			}
			pSrc2 = pSrc1->pChild[L2];
			pDst2 = pDst1->pChild[L2];
			//
			// x.x.x.0
			for(L3 = 0; L3 < 256; L3++)
			{
				if(pSrc2->pChild[L3] == NULL) {
					pDst2->pChild[L3] = NULL;
					continue;
				}
				pDst2->pChild[L3] = (genericOctetTreeNode*)MALLOC(szOct);
				if(pDst2->pChild[L3] == NULL) {
					LOG_MSG(LOG_SYS,"genericOctetTreeCopy - MALLOC failed");
					return -1;
				}
				pSrc3 = pSrc2->pChild[L3];
				pDst3 = pDst2->pChild[L3];
				//
				// x.x.x.x
				for(L4 = 0; L4 < 256; L4++)
				{
					if(pSrc3->pChild[L4] == NULL) {
						pDst3->pChild[L4] = NULL;
						continue;
					}
					pDst3->pChild[L4] = (genericOctetTreeNode*)MALLOC(szEle);
					if(pDst3->pChild[L4] == NULL) {
						LOG_MSG(LOG_SYS,"genericOctetTreeCopy - MALLOC failed");
						return -1;
					}
					pSrc->element.fnCopy((generic_t*)pDst3->pChild[L4],
							(generic_t*)pSrc3->pChild[L4]);
				}
			}
		}
	}
	return 1;
}

generic_t *genericOctetTreeGet(genericOctetTree *pOct, unsigned int hGeneric)
{
	unsigned char L1,L2,L3,L4;
	genericOctetTreeNode *p1, *p2, *p3;
	if(!genMagicCheck((genericInfo_t*)pOct))
		return NULL;
	L1 = OCTET1(hGeneric);
	L2 = OCTET2(hGeneric);
	L3 = OCTET3(hGeneric);
	L4 = OCTET4(hGeneric);
	if(pOct->base.pChild[L1] == NULL)
		return NULL;
	p1 = pOct->base.pChild[L1];
	if(p1->pChild[L2] == NULL) {
		return NULL;
	}
	p2 = p1->pChild[L2];
	if(p2->pChild[L3] == NULL) {
		return NULL;
	}
	p3 = p2->pChild[L3];
	if(p3->pChild[L4] == NULL) {
		return NULL;
	}
	return (generic_t*)p3->pChild[L4];
}

generic_t *genericOctetTreeIncr(genericOctetTree *pOct, unsigned int *i)
{
	unsigned int L1, L2, L3, L4; // level increments
	unsigned char S1, S2, S3, S4; // starting node of each level
	genericOctetTreeNode *p1, *p2, *p3;
	generic_t *pGeneric;
	if(!genMagicCheck((genericInfo_t*)pOct))
		return NULL;
	S1 = OCTET1(*i);
	S2 = OCTET2(*i);
	S3 = OCTET3(*i);
	S4 = OCTET4(*i);
	//
	// x.0.0.0
	for(L1 = S1; L1 < 256; L1++)
	{
		if(pOct->base.pChild[L1] == NULL)
			continue;
		p1 = pOct->base.pChild[L1];
		//
		// x.x.0.0
		for(L2 = S2; L2 < 256; L2++)
		{
			if(p1->pChild[L2] == NULL)
				continue;
			p2 = p1->pChild[L2];
			//
			// x.x.x.0
			for(L3 = S3; L3 < 256; L3++)
			{
				if(p2->pChild[L3] == NULL)
					continue;
				p3 = p2->pChild[L3];
				//
				// x.x.x.x
				for(L4 = S4; L4 < 256; L4++)
				{
					if(p3->pChild[L4] == NULL)
						continue;
					pGeneric = (generic_t*)p3->pChild[L4];
					//
					// handle overflows when incrementing L4
					if(L4 == 255)
					{
						L4 = 0;
						if(L3 == 255)
						{
							L3 = 0;
							if(L2 == 255)
							{
								L2 = 0;
								if(L1 == 255) {
									L1 = L2 = L3 = L4 = 255;
								} else {
									L1++;
								}
							} else {
								L2++;
							}
						} else {
							L3++;
						}
					} else {
						L4++;
					}
					*i = OCTET(L1,L2,L3,L4);
					return pGeneric;
				}
			}
		}
	}
	return NULL;
}

unsigned int genericOctetTreeCount(genericOctetTree *pOct)
{
	return pOct->num;
}

/*!
 * @Hash ops
 */
/*
   int genericHashInit(genericHash *pH, const genericInfo_t *pInfo,
   unsigned int nInitial, incrMethod_t incrMode)
   {
   genericInfoCopy(&pH->element, pInfo);
   pH->hash = NULL;
   pH->num = 0;
   pH->max = 0;
   pH->nInitial = nInitial;
   pH->incrMethod = incrMode;
   return 1;
   }

   int genericHashAlloc(genericHash *pHash)
   {
   return 1;
   }

   int genericHashFree(genericHash *pHash)
   {
   return 1;
   }

   int genericHashAdd(genericHash *pHash, generic_t *pGeneric)
   {
   return 1;
   }

   int genericHashDel(genericHash *pHash, int hGeneric)
   {
   return 1;
   }

   int genericHashFind(genericHash *pHash, generic_t *pGeneric)
   {
   return ELEMENT_NULL;
   }

   int genericHashIns(genericHash *pHash, generic_t *pGen, int hGen)
   {
   return 1;
   }

   int genericHashCopy(genericHash *pDst, genericHash *pSrc)
   {
   return 1;
   }

   generic_t *genericHashGet(genericHash *pHash, int hGeneric)
   {
   return NULL;
   }

   generic_t *genericHashIncr(genericHash *pHash, unsigned int *i)
   {
   return NULL;
   }
 */



/*********************************
 * @integerSet stuff
 */
/*
   int integerSetInit(integerSet *pSet)
   {
   pSet->intArray = NULL;
   pSet->max = 0;
   pSet->num = 0;
   return 1;
   }

   int integerSetAlloc(integerSet *pSet)
   {
   int i;
//
// allocate inital number of integers
pSet->intArray = (int*)MALLOC(sizeof(int) * INT_INITIAL);
if(pSet->intArray == NULL) {
LOG_MSG(LOG_SYS,"integerSetAlloc - MALLOC failed");
return -1;
}
pSet->max = INITIAL_INTEGER;
pSet->num = 0;
return 1;
}

int integerSetFree(integerSet *pSet)
{
int i;
if(pSet->intArray != NULL)
FREE(pSet->intArray);
pSet->intArray = NULL;
pSet->max = 0;
pSet->num = 0;
return 1;
}

int integerSetAdd(integerSet *pSet, int integer)
{
void *intBuf;
int i = 0;
if(pSet->num >= pSet->max) {
//
intBuf = REALLOC(pSet->intArray, sizeof(int) * pSet->max * 2);
if(intBuf == NULL) {
LOG_MSG(LOG_SYS,"integerSetAdd - REALLOC failed");
return -1;
}
pSet->intArray = (int*)intBuf;
for(i = pSet->max; i < pSet->max*2; i++)
pSet->intArray[i] = INT_NULL;
i = pSet->max; // reduce search time below
pSet->max *= 2;
}
while(i < pSet->max && pSet->intArray[i] != INT_NULL)
i++;
if(i >= pSet->max) {
LOG_MSG(LOG_HIGH,"integerSetAdd - Impossible operation 1");
return -1;
}
pSet->intArray[i] = integer;
pSet->num++;
return i;
}

int integerSetDel(integerSet *pSet, int hInt)
{
int i;
if(hInt < 0 || hInt >= pSet->max)
return INT_NULL;
i = pSet->intArray[hInt];
pSet->intArray[hInt] = INT_NULL;
pSet->num--;
return i;
}

int integerSetGet(integerSet *pSet, int hInt)
{
	if(hInt < 0 || hInt >= pSet->max)
		return INT_NULL;
	return pSet->intArray[hInt];
}

int integerSetFind(integerSet *pSet, int integer)
{
	int i;
	for(i = 0; i < pSet->max; i++) {
		if(pSet->intArray[i] == integer) {
			return i;
		}
	}
	return ELEMENT_NULL;
}

int integerSetCopy(integerSet *pDst, integerSet *pSrc)
{
	int i;
	integerSetFree(pDst);
	pDst->intArray = (int*)MALLOC(sizeof(int) * pSrc->max);
	if(pDst->intArray == NULL) {
		LOG_MSG(LOG_SYS,"integerSetCopy - MALLOC failed");
		return -1;
	}
	pDst->max = pSrc->max;
	pDst->num = pSrc->num;
	for(i = 0; i < pSrc->max; i++)
		pDst->intArray[i] = pSrc->intArray[i];
	return 1;
}
*/


int genericIntCmp(generic_t *pGen1, generic_t *pGen2)
{
	return *((int*)pGen1) - *((int*)pGen2);
}

