
/*
 * Height Map Library
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#include <math.h>
#include <metric/common.h>
#include <metric/heightMap.h>
#include <metric/noise.h>


static int fillMapFlat(heightMap_t *pMap, float *pDst);
static int fillMapPerlin(heightMap_t *pMap, float *pDst);
static int fillMapHetero(heightMap_t *pMap, float *pDst);
static int fillMapMultifrac(heightMap_t *pMap, float *pDst);
static int fillMapRidged(heightMap_t *pMap, float *pDst);

/*
 * call this before doing anything with the 
 * heightMap.  don't call after it has been
 * allocated memory.
 */
int heightMapInit(heightMap_t *pMap)
{
	pMap->dx = 0;
	pMap->dy = 0;
	pMap->ix = 1.0f;
	pMap->iy = 1.0f;
	matInit2f(pMap->mPreTransform, MAT_IDENTITY);
	VECTINIT2F(pMap->vPreTranslate, 0.0f);
	pMap->postScaleZ = 0.0f;
	pMap->postTranslateZ = 0.0f;
	pMap->noiseType = NOISE_FLAT;
	pMap->prime1 = 1;
	pMap->prime2 = 3;
	pMap->fracDimen = 0.0f;
	pMap->lacunarity = 0;
	pMap->octaves = 0.0f;
	pMap->offset = 0.0f;
	pMap->gain = 0.0f;
	return 1;
}

float *heightMapAlloc(heightMap_t *pMap)
{
	float *rtrn;
	if(pMap == NULL) {
		LOG_MSG(LOG_LOW,"heightMapAlloc - NULL heightMap_t");
		return NULL;
	}
	if(pMap->dx <= 0 || pMap->dy <= 0) {
		VLOG_MSG(LOG_LOW,"heightMapAlloc - (dx,dy)(%d,%d)", pMap->dx, pMap->dy);
		return NULL;
	}
	rtrn = (float*)MALLOC(pMap->dx * pMap->dy * sizeof(float));
	if(rtrn == NULL) {
		LOG_MSG(LOG_SYS,"heightMapAlloc - MALLOC failed");
		return NULL;
	}
	return rtrn;
}

int heightMapFree(heightMap_t *pMap, float *pData)
{
	if(pData == NULL)
		return 1;
	FREE(pData);
	return 1;
}

int heightMapVerify(heightMap_t *pMap)
{
	if(pMap->ix < 0.0f) {
		VLOG_MSG(LOG_LOW,"heightMapVerify - ix < 0: %f", pMap->ix);
		return 0;
	}
	if(pMap->iy < 0.0f) {
		VLOG_MSG(LOG_LOW,"heightMapVerify - ix < 0: %f", pMap->ix);
		return 0;
	}
	if(pMap->noiseType >= NUM_NOISE_TYPE) {
		VLOG_MSG(LOG_LOW,"heightMapVerify - noiseType invalid: %d",
				pMap->noiseType);
		return 0;
	}
	return 1;
}

int heightMapByteSwap(heightMap_t *pMap)
{
	pMap->dx = BYTESWAP32(pMap->dx);
	pMap->dy = BYTESWAP32(pMap->dy);
	BYTESWAP32F(pMap->ix);
	BYTESWAP32F(pMap->iy);
	BYTESWAP32F(pMap->mPreTransform[0][0]);
	BYTESWAP32F(pMap->mPreTransform[0][1]);
	BYTESWAP32F(pMap->mPreTransform[1][0]);
	BYTESWAP32F(pMap->mPreTransform[1][1]);
	BYTESWAP32F(pMap->vPreTranslate[0]);
	BYTESWAP32F(pMap->vPreTranslate[1]);
	BYTESWAP32F(pMap->postScaleZ);
	BYTESWAP32F(pMap->postTranslateZ);
	pMap->noiseType = BYTESWAP32(pMap->noiseType);
	pMap->prime1 = BYTESWAP32(pMap->prime1);
	pMap->prime2 = BYTESWAP32(pMap->prime2);
	BYTESWAP32F(pMap->fracDimen);
	BYTESWAP32F(pMap->lacunarity);
	BYTESWAP32F(pMap->octaves);
	BYTESWAP32F(pMap->offset);
	BYTESWAP32F(pMap->gain);
	return 1;
}


int heightMapFill(heightMap_t *pMap, float *pDst)
{
	if(pMap == NULL) {
		LOG_MSG(LOG_LOW,"heightMapFill - heightMap is NULL");
		return -1;
	}
	if(pDst == NULL) {
		LOG_MSG(LOG_LOW,"heightMapFill - dst heightMap be NULL");
		return -1;
	}
	if(pMap->dx <= 0 || pMap->dy <= 0) {
		VLOG_MSG(LOG_LOW,"heightMapFill - (dx,dy)=(%d,%d)", pMap->dx, pMap->dy);
		return -1;
	}
	switch(pMap->noiseType) {
		case NOISE_FLAT:
			if(fillMapFlat(pMap, pDst) < 0)
				return -1;
			break;
		case NOISE_PERLIN:
			if(fillMapPerlin(pMap, pDst) < 0)
				return -1;
			break;
		case NOISE_HETERO:
			if(fillMapHetero(pMap, pDst) < 0)
				return -1;
			break;
		case NOISE_MULTIFRAC:
			if(fillMapMultifrac(pMap, pDst) < 0)
				return -1;
			break;
		case NOISE_RIDGED:
			if(fillMapRidged(pMap, pDst) < 0)
				return -1;
			break;
		default:
			VLOG_MSG(LOG_LOW,"heightMapFill - unknown noise type: %d",
					pMap->noiseType);
			return -1;
	}

	return 1;
}

int heightMapCopy(heightMap_t *pDst, heightMap_t *pSrc)
{
	pDst->dx = pSrc->dx;
	pDst->dy = pSrc->dy;
	pDst->ix = pSrc->ix;
	pDst->iy = pSrc->iy;
	MATCOPY2F(pDst->mPreTransform, pSrc->mPreTransform);
	VECTCOPY2F(pDst->vPreTranslate, pSrc->vPreTranslate);
	pDst->postScaleZ = pSrc->postScaleZ;
	pDst->postTranslateZ = pSrc->postTranslateZ;
	pDst->noiseType = pSrc->noiseType;
	pDst->prime1 = pSrc->prime1;
	pDst->prime2 = pSrc->prime2;
	pDst->fracDimen = pSrc->fracDimen;
	pDst->lacunarity = pSrc->lacunarity;
	pDst->octaves = pSrc->octaves;
	pDst->offset = pSrc->offset;
	pDst->gain = pSrc->gain;
	return 1;
}

/*
 * take the derivative of pSrc and
 * put results into pDst.  For 2nd derivative,
 * run pDst back through as pSrc. 
 * !! pDst and pSrc should be allocated according
 * !! to the extents in pMap */
int heightMapDerive(heightMap_t *pMap, float *pDst, float *pSrc)
{
	int dx, dy, ix, iy, idx;
	float dz, fTmp;
	if(pMap == NULL) {
		LOG_MSG(LOG_LOW,"heightMapDerive - map be NULL");
		return -1;
	}
	if(pSrc == NULL) {
		LOG_MSG(LOG_LOW,"heightMapDerive - map isn't filled yet");
		return -1;
	}
	if(pDst == NULL) {
		LOG_MSG(LOG_LOW,"heightMapDerive - dst buffer is NULL");
		return -1;
	}
	dx = pMap->dx;
	dy = pMap->dy;
	/*
	 * fill in 4 corners of the derivative map */
	/* bottom left */
	dz = pSrc[0];
	pDst[0] = 0.0f;
	pDst[0] += fabs(pSrc[1] - dz) / 2.0f;
	pDst[0] += fabs(pSrc[dx] - dz) / 2.0f;
	pDst[0] += fabs(pSrc[dx+1] - dz) / 1.414213f; /* sqrt(2) */
	idx = dx-1; /* top left */
	dz = pSrc[idx];
	pDst[idx] = 0.0f;
	pDst[idx] += fabs(pSrc[idx-1] - dz) / 2.0f;
	pDst[idx] += fabs(pSrc[idx+dx] - dz) / 2.0f;
	pDst[idx] += fabs(pSrc[idx+dx-1] - dz) / 1.414213f;
	idx = (dy-1)*dx;  /* bottom right */
	dz = pSrc[idx];
	pDst[idx] = 0.0f;
	pDst[idx] += fabs(pSrc[idx+1] - dz) / 2.0f;
	pDst[idx] += fabs(pSrc[idx-dx] - dz) / 2.0f;
	pDst[idx] += fabs(pSrc[idx-dx+1] - dz) / 1.414213f;
	idx = dy*dx - 1;  /* top right */
	dz = pSrc[idx];
	pDst[idx] = 0.0f;
	pDst[idx] += fabs(pSrc[idx-1] - dz) / 2.0f;
	pDst[idx] += fabs(pSrc[idx-dx] - dz) / 2.0f;
	pDst[idx] += fabs(pSrc[idx-dx-1] - dz) / 1.414213f;
	/*
	 * fill in all 4 sides */
	for(ix = 1; ix < dx-1; ix++) {
		idx = ix;
		dz = pSrc[idx];
		pDst[idx] = 0.0f;
		pDst[idx] += fabs(pSrc[idx-1] - dz) / 3.0f;
		pDst[idx] += fabs(pSrc[idx+1] - dz) / 3.0f;
		pDst[idx] += fabs(pSrc[idx+dx]- dz) / 3.0f;
		pDst[idx] += fabs(pSrc[idx+dx-1]-dz) / 2.828426f;
		pDst[idx] += fabs(pSrc[idx+dx+1]-dz) / 2.828426f;
		idx = (dy-1)*dx + ix;
		dz = pSrc[idx];
		pDst[idx] = 0.0f;
		pDst[idx] += fabs(pSrc[idx-1] - dz) / 3.0f;
		pDst[idx] += fabs(pSrc[idx+1] - dz) / 3.0f;
		pDst[idx] += fabs(pSrc[idx-dx]- dz) / 3.0f;
		pDst[idx] += fabs(pSrc[idx-dx-1]-dz) / 2.828426f;
		pDst[idx] += fabs(pSrc[idx-dx+1]-dz) / 2.828426f;
	}
	for(iy = 1; iy < dy-1; iy++) {
		idx = iy*dx;
		dz = pSrc[idx];
		pDst[idx] = 0.0f;
		pDst[idx] += fabs(pSrc[idx+1] - dz) / 3.0f;
		pDst[idx] += fabs(pSrc[idx-dx] - dz) / 3.0f;
		pDst[idx] += fabs(pSrc[idx+dx] - dz) / 3.0f;
		pDst[idx] += fabs(pSrc[idx-dx+1] - dz) / 2.828426f;
		pDst[idx] += fabs(pSrc[idx+dx+1] - dz) / 2.828426f;
		idx = iy*dx + (dx-1);
		dz = pSrc[idx];
		pDst[idx] = 0.0f;
		pDst[idx] += fabs(pSrc[idx+1] - dz) / 3.0f;
		pDst[idx] += fabs(pSrc[idx-dx] - dz) / 3.0f;
		pDst[idx] += fabs(pSrc[idx+dx] - dz) / 3.0f;
		pDst[idx] += fabs(pSrc[idx-dx-1] - dz) / 2.828426f;
		pDst[idx] += fabs(pSrc[idx+dx-1] - dz) / 2.828426f;
	}
	/*
	 * fill everything in the middle */
	for(iy = 1; iy < dy-1; iy++) {
		for(ix = 1; ix < dx-1; ix++) {
			idx = iy*dx + ix;
			dz = pSrc[idx];
			pDst[idx] = 0.0f;
			/*
			 * the 4 adjacent lots */
			pDst[idx] += fabs(pSrc[(iy-1)*dx+ix] - dz);
			pDst[idx] += fabs(pSrc[iy*dx+(ix-1)] - dz);
			pDst[idx] += fabs(pSrc[iy*dx+(ix+1)] - dz);
			pDst[idx] += fabs(pSrc[(iy+1)*dx+ix] - dz);
			pDst[idx] /= 4.0f;
			/*
			 * the 4 diagonal lots */
			fTmp = 0;
			fTmp += fabs(pSrc[(iy-1)*dx+(ix-1)] - dz);
			fTmp += fabs(pSrc[(iy-1)*dx+(ix+1)] - dz);
			fTmp += fabs(pSrc[(iy+1)*dx+(ix-1)] - dz);
			fTmp += fabs(pSrc[(iy+1)*dx+(ix+1)] - dz);
			fTmp /= 5.656852f; /* 4*sqrt(2) */
			pDst[idx] += fTmp;
		}
	}
	return 1;
}

int fillMapFlat(heightMap_t *pMap, float *pDst)
{
	unsigned int ix, iy;
	float h;
	int idx;
	idx = pMap->dx * pMap->dy;
	h = (float)pMap->offset;
	for(iy = 0; iy < pMap->dy; iy++) {
		for(ix = 0; ix < pMap->dx; ix++) {
			idx = iy*pMap->dx + ix;
			pDst[idx] = h;
		}
	}
	return 1;
}

int fillMapPerlin(heightMap_t *pMap, float *pDst)
{
	unsigned int ix, iy;
	float fx, fy;
	int idx;
	for(iy = 0; iy < pMap->dy; iy++)
	{
		for(ix = 0; ix < pMap->dx; ix++)
		{
			idx = iy*pMap->dx + ix;
			/*
			 * scaling and rotation of coordinates are handled
			 * by the transformation matrix below */
			fx = (ix * pMap->mPreTransform[0][0]) +
				(iy * pMap->mPreTransform[1][0]);
			fy = (ix * pMap->mPreTransform[0][1]) + 
				(iy * pMap->mPreTransform[1][1]);
			/*
			 * translation is then handled */
			fx += pMap->vPreTranslate[0];
			fy += pMap->vPreTranslate[0];
			/*
			 * use the newly transformed coordinates to find
			 * the height from a noise function */
			pDst[idx] = noisePerlin2(fx, fy);
			/*
			 * scale and translate the height */
			pDst[idx] = (pDst[idx] * pMap->postScaleZ) + pMap->postTranslateZ;
		}
	}
	return 1;
}

int fillMapHetero(heightMap_t *pMap, float *pDst)
{
	return 1;
}

int fillMapMultifrac(heightMap_t *pMap, float *pDst)
{
	float freq, result, signal, weight, remainder;
	unsigned int i, iy, ix, idx;
	float fx, fy;
	float *exponent;

	exponent = (float*)MALLOC(((int)pMap->octaves+1) * sizeof(float));
	if(exponent == NULL) {
		LOG_MSG(LOG_SYS,"fillMapMultifrac - MALLOC failed");
		return -1;
	}
	freq = 1.0f;
	for(i = 0; i <= pMap->octaves; i++) {
		exponent[i] = pow(freq, -pMap->fracDimen);
		freq *= pMap->lacunarity;
	}

	for(iy = 0; iy < pMap->dy; iy++)
	{
		for(ix = 0; ix < pMap->dx; ix++)
		{
			idx = iy*pMap->dx + ix;
			/*
			 * scaling and rotation of coordinates are handled
			 * by the transformation matrix below */
			fx = (ix * pMap->mPreTransform[0][0]) +
				(iy * pMap->mPreTransform[1][0]);
			fy = (ix * pMap->mPreTransform[0][1]) + 
				(iy * pMap->mPreTransform[1][1]);
			/*
			 * translation is then handled */
			fx += pMap->vPreTranslate[0];
			fy += pMap->vPreTranslate[0];

			pDst[idx] = (noisePerlin2(fx, fy) + pMap->offset) * exponent[0];
			/* result = (vnoise(fx, fy, 0.0f) + pMap->offset) * exponent[0];*/
			/* result = (noise2((int)fx, (int)fy) + pMap->offset) * exponent[0];*/
			weight = result;
			fx *= pMap->lacunarity;
			fy *= pMap->lacunarity;
			/* spectral construction inner loop, where fractal is 
			 * built */
			for(i = 1; i < pMap->octaves; i++) {
				/* prevent divergence */
				if(weight > 1.0f) 
					weight = 1.0f;
				/* get next higher frequency */
				signal = (noisePerlin2(fx, fy) + pMap->offset) * exponent[i];
				/* signal=(vnoise(fx,fy,0.0f)+pMap->offset)*exponent[i]; */
				/* signal = (noise2((int)fx, (int)fy)+pMap->offset)*exponent[i];*/
				/* add it in, weighted by previous freq value */
				result += weight * signal;
				/* update the (monotonically decreasing) weighted
				 * value. (this is why H must specify a high fractal
				 * dimension) */
				weight *= signal;
				/* increase frequency */
				fx *= pMap->lacunarity;
				fy *= pMap->lacunarity;
			}
			/* take care of remainder in "octaves" */
			remainder = pMap->octaves - (int)pMap->octaves;
			if(remainder)
				result += remainder * noisePerlin2(fx, fy) * exponent[i];
			/*		result += remainder*vnoise(fx,fy,0.0f)*exponent[i]; */
			/*		result += remainder * noise2((int)fx, (int)fy) * exponent[i];*/
			pDst[idx] = (result * pMap->postScaleZ) + pMap->postTranslateZ;
		}
	}
	FREE(exponent);
	return 1;
}

int fillMapRidged(heightMap_t *pMap, float *pDst)
{
	return 1;
}

int heightMap2triPatch(geomObj3ve *pObj, geoTriPatch *pTriPatch,
						float *pSrc, heightMap_t *pHeightMap)
{
	int row, col;
	int nRow, nCol;
	int iVert, iEdge, iFace; // counters 
	int edgeOffset;  // temp value to avoid re-computing
	vect3f *pVert; // to keep screen clean (Don't mess with Texas)
	vect2f *pTex;
	edge2v *pEdge;
	face3ve *pFace;
	nRow = pHeightMap->dy; // for a mental representation,
	nCol = pHeightMap->dx; // rows increase down, columns increase to the right
	//
	// use the information from the pHeightMap and pTriPatch to
	// construct the details of the resulting triPatch.
	pTriPatch->gridType = 1;
	//
	// find length between heightMap samples
	pTriPatch->dx = pHeightMap->ix * (pHeightMap->dx - 1);
	pTriPatch->dy = pHeightMap->iy * (pHeightMap->dy - 1);
	// overwrite the triPatch grid infomation to match the heightMap
	pTriPatch->nGridX = pHeightMap->dx;
	pTriPatch->nGridY = pHeightMap->dy;
	//
	// I already know what the triangle patch is going to look like,
	// so I can allocate just enough mem for it.
	pObj->nVert = nCol * nRow;
	pObj->nTex = nCol * nRow;
	pObj->nEdge = ((nCol-1)*nRow) + ((nRow-1)*nCol) + (nCol-1)*(nRow-1);
	pObj->nFace = (nRow-1)*(nCol-1) * 2;
	pObj->pVert = (vect3f*)MALLOC(sizeof(vect3f) * pObj->nVert);
	if(pObj->pVert == NULL) {
		LOG_MSG(LOG_SYS,"heightMap2triPatch - MALLOC failed");
		geomObjFree3ve(pObj);
		return -1;
	}
	pObj->pTex = (vect2f*)MALLOC(sizeof(vect2f) * pObj->nTex);
	if(pObj->pTex == NULL) {
		LOG_MSG(LOG_SYS,"heightMap2triPatch - MALLOC failed");
		geomObjFree3ve(pObj);
		return -1;
	}
	pObj->pEdge = (edge2v*)MALLOC(sizeof(edge2v) * pObj->nEdge);
	if(pObj->pEdge == NULL) {
		LOG_MSG(LOG_SYS,"heightMap2triPatch - MALLOC failed");
		geomObjFree3ve(pObj);
		return -1;
	}
	pObj->pFace = (face3ve*)MALLOC(sizeof(face3ve) * pObj->nFace);
	if(pObj->pFace == NULL) {
		LOG_MSG(LOG_SYS,"heightMap2triPatch - MALLOC failed");
		geomObjFree3ve(pObj);
		return -1;
	}
	iVert = iEdge = iFace = 0;
	pVert = pObj->pVert; // keep screen clean
	pTex = pObj->pTex;
	pEdge = pObj->pEdge;
	pFace = pObj->pFace;
	//
	// upper left corner vertex
	pVert[iVert][0] = 0.0f;
	pVert[iVert][1] = 0.0f;
	pVert[iVert][2] = pSrc[0];
	pTex[iVert][0] = 0.0f;
	pTex[iVert][1] = 0.0f;
	iVert++;
	// top edges
	for(col = 1; col < nCol; col++)
	{
		pVert[iVert][0] = col * pHeightMap->ix;
		pVert[iVert][1] = 0.0f;
		pVert[iVert][2] = pSrc[col];
		pTex[iVert][0] = (float)col / (nCol-1);
		pTex[iVert][1] = 0.0f;
		pEdge[iEdge].v[0] = col - 1;
		pEdge[iEdge].v[1] = col; 
		if(col % 2 == 0) {
			pEdge[iEdge].f[0] = col * 2 - 2;
			pEdge[iEdge].f[1] = -1;
		} else {
			pEdge[iEdge].f[0] = col * 2 - 1;
			pEdge[iEdge].f[1] = -1;
		}
		iVert++;
		iEdge++;
	}
	// everything else
	for(row = 1; row < nRow; row++)
	{
		// far left vertex
		pVert[iVert][0] = 0.0f;
		pVert[iVert][1] = row * pHeightMap->iy;
		pVert[iVert][2] = pSrc[row*nCol];
		pTex[iVert][0] = 0.0f;
		pTex[iVert][1] = (float)row / (nRow-1);
		// far left edge
		pEdge[iEdge].v[0] = (row-1)*nCol;
		pEdge[iEdge].v[1] = row*nCol;
		pEdge[iEdge].f[0] = (row-1)*(nCol-1)*2;
		pEdge[iEdge].f[1] = -1; // no face on the left
		iVert++;
		iEdge++;
		for(col = 1; col < nCol; col++)
		{
			// bottom right vertex
			pVert[iVert][0] = col * pHeightMap->ix;
			pVert[iVert][1] = row * pHeightMap->iy;
			pVert[iVert][2] = pSrc[row*nCol + col];
			pTex[iVert][0] = (float)col / (nCol-1);
			pTex[iVert][1] = (float)row / (nRow-1);
			//
			// diagonal edge; has oscillating orientation
			if((row + col) % 2 == 0) {
				pEdge[iEdge].v[0] = (row-1)*nCol + col-1;
				pEdge[iEdge].v[1] = row*nCol + col;
			} else {
				pEdge[iEdge].v[0] = (row-1)*nCol + col;
				pEdge[iEdge].v[1] = row*nCol + col-1;
			}
			pEdge[iEdge].f[0] = (row-1)*(nCol-1)*2 + (col-1)*2;
			pEdge[iEdge].f[1] = (row-1)*(nCol-1)*2 + (col-1)*2 + 1;
			// bottom edge
			pEdge[iEdge+1].v[0] = row*nCol + col-1;
			pEdge[iEdge+1].v[1] = row*nCol + col;
			if((col + row) % 2 == 0) {
				pEdge[iEdge+1].f[0] = (row-1)*(nCol-1)*2 + (col-1)*2;
			} else {
				pEdge[iEdge+1].f[0] = (row-1)*(nCol-1)*2 + (col-1)*2 + 1;
			}
			pEdge[iEdge+1].f[1] = -1;
			if(row != nRow-1) {
				if((col + row+1) % 2 == 0)
					pEdge[iEdge+1].f[1] = row*(nCol-1)*2 + (col-1)*2 + 1;
				else
					pEdge[iEdge+1].f[1] = row*(nCol-1)*2 + (col-1)*2;
			}
			// right edge
			pEdge[iEdge+2].v[0] = (row-1)*nCol + col;
			pEdge[iEdge+2].v[1] = row*nCol + col;
			pEdge[iEdge+2].f[0] = (row-1)*(nCol-1)*2 + (col-1)*2 + 1;
			pEdge[iEdge+2].f[1] = -1; 
			if(col != nCol-1) // edge of heightMap 
				pEdge[iEdge+2].f[1] = (row-1)*(nCol-1)*2 + col*2;
			// 2 faces
			pFace[iFace].mat = -1; // don't know the material yet
			pFace[iFace+1].mat = -1;
			// edgeOffset is the diagonal edge for this patch
			edgeOffset = (row-1)*(3*nCol-2) // edges in previous rows
				+ (3*(col-1))     // edges in current row 
				+ nCol;          // edges in first row
			if((row + col) % 2 == 0)
			{
				pFace[iFace].v[0] = (row-1)*nCol + col-1;	//top-left
				pFace[iFace].v[1] = row*nCol + col-1;	//bot-left
				pFace[iFace].v[2] = row*nCol + col;		//bot-right
				pFace[iFace].e[0] = edgeOffset - 1;	// left
				pFace[iFace].e[1] = edgeOffset;	// diag
				pFace[iFace].e[2] = edgeOffset + 1;	// bottom
				pFace[iFace+1].v[0] = row*nCol + col;	//bot-right
				pFace[iFace+1].v[1] = (row-1)*nCol + col;	//top-right
				pFace[iFace+1].v[2] = (row-1)*nCol + col-1;	//top-left
				pFace[iFace+1].e[0] = edgeOffset;	//diag
				pFace[iFace+1].e[1] = edgeOffset + 2;//right 
				if(row == 1)
					// very top edge
					pFace[iFace+1].e[2] = col - 1;
				else
					// previous row's bottom edge
					pFace[iFace+1].e[2] = edgeOffset-(3*nCol-2)+1;
			} else {
				pFace[iFace].v[0] = (row-1)*nCol + col;
				pFace[iFace].v[1] = (row-1)*nCol + col-1;
				pFace[iFace].v[2] = row*nCol + col-1;
				pFace[iFace].e[0] = edgeOffset - 1;	// left
				pFace[iFace].e[1] = edgeOffset;	// diag
				if(row == 1) {
					// very top edge
					pFace[iFace].e[2] = col - 1;
				} else {
					// previous row's bottom edge
					pFace[iFace].e[2] = edgeOffset-(3*nCol-2)+1;
				}
				pFace[iFace+1].v[0] = row*nCol + col-1;
				pFace[iFace+1].v[1] = row*nCol + col;
				pFace[iFace+1].v[2] = (row-1)*nCol + col;
				pFace[iFace+1].e[0] = edgeOffset;	// diag
				pFace[iFace+1].e[1] = edgeOffset + 1;// bottom
				pFace[iFace+1].e[2] = edgeOffset + 2;// right 
			}
			// texture references are the same as the vertices
			pFace[iFace].t[0] = pFace[iFace].v[0];
			pFace[iFace].t[1] = pFace[iFace].v[1];
			pFace[iFace].t[2] = pFace[iFace].v[2];
			pFace[iFace+1].t[0] = pFace[iFace+1].v[0];
			pFace[iFace+1].t[1] = pFace[iFace+1].v[1];
			pFace[iFace+1].t[2] = pFace[iFace+1].v[2];
			// wait till after processing a square 
			// to adjust the numbers
			iVert += 1;
			iEdge += 3;
			iFace += 2;
		}
	}
	if(iVert != pObj->nVert) {
		VLOG_MSG(LOG_LOW,"CreateTrianglePatch - expected %d vertices, got: %d",
				pObj->nVert, iVert);
		geomObjFree3ve(pObj);
		return -1;
	}
	if(iEdge != pObj->nEdge) {
		VLOG_MSG(LOG_LOW,"CreateTrianglePatch - expected %d edges; but got %d",
				pObj->nEdge, iEdge);
		geomObjFree3ve(pObj);
		return -1;
	}
	if(iFace != pObj->nFace) {
		VLOG_MSG(LOG_LOW,"CreateTrianglePatch - expected %d faces; but got %d",
				pObj->nFace, iFace);
		geomObjFree3ve(pObj);
		return -1;
	}
	// 
	// generate the normals for all the faces
	for(iFace = 0; iFace < pObj->nFace; iFace++) {
		FACENORM3V(pFace[iFace].n, pVert[pFace[iFace].v[2]],
				pVert[pFace[iFace].v[1]], pVert[pFace[iFace].v[0]]);
		vectNorm3f(pFace[iFace].n);
	}
	return 1;
}

float heightMapGetHeight(heightMap_t *pMap, float *pData, float x, float y)
{
	unsigned int ix, iy;  // heightMap indices
	float fHeight;
	//
	// first we need to convert the absolute coordinates x/y to
	// heightMap indices
	ix = (int)(x / pMap->ix);
	iy = (int)(y / pMap->iy);
	if(ix >= pMap->dx - 1)
		ix = pMap->dx - 2;
	if(ix < 0)
		ix = 0;
	if(iy >= pMap->dy - 1)
		iy = pMap->dy - 2;
	if(iy < 0)
		iy = 0;
	fHeight = 0.0f;
	fHeight += pData[(iy+0)*pMap->dx + (ix+0)];
	fHeight += pData[(iy+0)*pMap->dx + (ix+1)];
	fHeight += pData[(iy+1)*pMap->dx + (ix+0)];
	fHeight += pData[(iy+1)*pMap->dx + (ix+1)];
	fHeight /= 4.0f;
	return fHeight;
}

