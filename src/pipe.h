
/*
 * (going-to-be) Cross-platform Pipe Routines
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */


#ifndef METRIC_PIPE_H
#define METRIC_PIPE_H

#include <stdlib.h>

//! pipe flags
enum {
	PIPE_READ = 1, //! whether to open a pipe from child to parent
	PIPE_WRITE = 2, //! child to parent
	PIPE_ERR = 4, //! child to parent for errors
	PIPE_BLOCK =	 0x10000, 
	PIPE_NONBLOCK =	 0x20000, 
	PIPE_UNUSED = 0xFFFDFFF8
};

#define PIPE_PARENT 0x20000000
#define PIPE_CHILD 0x40000000
#define PIPE_CREATED 0x80000000

typedef struct _pipe_t {
	unsigned int flags;
	unsigned int parentRead;
	unsigned int parentWrite;
	unsigned int childRead;
	unsigned int childWrite;
	unsigned int errRead;
	unsigned int errWrite;
} pipe_t;


#ifdef __cplusplus
extern "C" {
#endif
 
int pipeInit(pipe_t *pPipe);
int pipeCopy(pipe_t *pDst, pipe_t *pSrc);
int pipeCreate(pipe_t *pPipe, unsigned int flags);
int pipeCreated(pipe_t *pPipe);
int pipeDestroy(pipe_t *pPipe);
int pipeAttach(pipe_t *pPipe, int bParent);  // set which end of pipe we are
int pipeAttached(pipe_t *pPipe);
int pipeOptionGet(pipe_t *pPipe, int opt, int *value);
int pipeOptionSet(pipe_t *pPipe, int opt, int *value);
int pipeRead(pipe_t *pPipe, void *pData, size_t nBytes);
int pipeWrite(pipe_t *pPipe, void *pData, size_t nBytes);
int pipeErrRead(pipe_t *pPipe, void *pData, size_t nBytes);
int pipeErrWrite(pipe_t *pPipe, void *pData, size_t nBytes);
int pipeFileno(pipe_t *pPipe, int *read, int *write, int *err);
int pipeDupStdio(pipe_t *pPipe); //! duplicates our side of fd's onto stdio
int pipeFromStdio(pipe_t *pPipe); //! set as child and use stdio

#ifdef __cplusplus
}
#endif

#endif

