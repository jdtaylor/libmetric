
/*
 * A Common Geometry Library
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#ifndef METRIC_GEOM_H
#define METRIC_GEOM_H

#include <metric/common.h>
#include <metric/math.h>

/*! 
 * LEGEND OF ABREVIATIONS:
 * 	
 * Variable names:
 * 	v - vertex reference
 * 	t - texture coordinate reference
 * 	e - edge reference
 * 	f - face reference
 * 	vn - vertex normal
 * 	mat - material handle
 *
 * Type names:
 * 	geomObj* - geometry of an object by primitives (vertices, edges..)
 * 	geo* - geometry of an object by description (length, radius..)
 */

/*!
 * face defined by 3 vertex references */
typedef struct {
	int mat;	/* material handle */
	int v[3];	/* 3 vertex references */
	int t[3];	/* 3 texture coordinate references */
	vect3f n;	/* 1 face normal */
	vect3f vn[3]; /* 3 vertex normals */
} face3v;

/*!
 * face defined by 3 vertices and 3 edges */
typedef struct {
	int	mat;	/* 1 material */
	int v[3];	/* 4 vertices */
	int t[3];	/* 3 texture coordinates */
	int e[3];	/* 3 edges */
	vect3f n;	/* 1 face normal */
} face3ve;	/* 2 Tsp olive oil */

/*! 
 * face defined by mammalian senses */
typedef struct {
	int eye[2];		/* 2 seeing things */
	long nose;		/* 1 smeller */
	int chomper;	/* 1 taster */
	int ear[2];		/* 2 listeners */
} faceBP;		/* 1 drop LSD */

typedef struct {
	int v[3];
	int vd[3];
} face3vd;

/*!
 * edge defined by 2 vertices */
typedef struct {
	int v[2];	/* 2 vertices */
	int f[2];	/* 1 or 2 adjacent faces */
} edge2v;	/* 1 Tsp active yeast */

/*!
 * 3d object defined by 3 vertices per face w/ edge references
 */
typedef struct {
	int nVert;
	int nTex;
	int nEdge;
	int nFace;
	vect3f	*pVert;
	vect2f	*pTex;
	edge2v	*pEdge;
	face3ve	*pFace;
} geomObj3ve;

/*!
 * set of frames for a 3d object at different times
 */
typedef struct {
	int nFrame;	     /* total number of frames of sprite */
	int *pFrameTime; /* length of frame in ms. */
	int *pObj;    /* array of references to geomObj's. 1 per frame */
} geomObjSprite;

/*
 * parameters for creating a triangle patch */
typedef struct {
	float dx;	/* units of size (meters) in direction X */
	float dy;	/* units of size (meters) in direction Y */
	int nGridX;	/* num grid lines perpendicular to X axis (includes extents)*/
	int nGridY; /* num grid lines perpendicular to Y axis (includes extents)*/
	int gridType; /* 0 for regular, 1 for alternating diagonals */
} geoTriPatch;


/* 
 * calcuate the normal to the triangle defined by its vertices
 * (x1,y1,z1),(x2,y2,z2),(x3,y3,z3).  result is stored into the
 * array[3] called n. operation here is: (P1P2 cross P1P3) */
/* It's probably a good idea to normalize "n" afterwords */
#define FACENORM3(n,x1,y1,z1,x2,y2,z2,x3,y3,z3) \
    (n)[0] = ((y2)-(y1))*((z3)-(z1))-((z2)-(z1))*((y3)-(y1));\
    (n)[1] = ((z2)-(z1))*((x3)-(x1))-((x2)-(x1))*((z3)-(z1));\
    (n)[2] = ((x2)-(x1))*((y3)-(y1))-((y2)-(y1))*((x3)-(x1))
/*
 * same as FACENORM3 except use vectors for the vertices */
#define FACENORM3V(n,v1,v2,v3) \
    FACENORM3(n,\
	    (v1)[0], (v1)[1], (v1)[2],\
	    (v2)[0], (v2)[1], (v2)[2],\
	    (v3)[0], (v3)[1], (v3)[2])

#ifdef __cplusplus
extern "C" {
#endif

int geomObjInit3ve(geomObj3ve *pObj);
int geomObjFree3ve(geomObj3ve *pObj);
int geomObjMaterial3ve(geomObj3ve *pObj, int matId);
int geomObjMinimize3ve(geomObj3ve *pObj);

#ifdef __cplusplus
}
#endif

#endif
