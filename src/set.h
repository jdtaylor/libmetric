
/*
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#ifndef METRIC_SET_H
#define METRIC_SET_H

#include <metric/common.h>

//! maximum handle to any element
#define GENERIC_MAX	((int)0xFFFFFFFC)
//! last element of a set
#define ELEMENT_LAST	((int)0xFFFFFFFD) 
//! first element of a set
#define ELEMENT_FIRST	((int)0xFFFFFFFE) 
//! null element handle
#define ELEMENT_NULL	((int)0xFFFFFFFF) 

#define integerSet_t	genericOctetTree
#define integerSet(op)	genericOctetTree##op

typedef void generic_t;

typedef int (*fGenericInit)(generic_t *pGeneric);
typedef int (*fGenericAlloc)(generic_t *pGeneric);
typedef int (*fGenericFree)(generic_t *pGeneric);
typedef int (*fGenericCmp)(generic_t *pGen1, generic_t *pGen2);
typedef int (*fGenericCopy)(generic_t *pDst, generic_t *pSrc);

typedef struct _genericInfo_t {
	unsigned int size;
	fGenericInit fnInit;
	fGenericAlloc fnAlloc;
	fGenericFree fnFree;
	fGenericCmp fnCmp;
	fGenericCopy fnCopy;
	unsigned int magic;
} genericInfo_t;

typedef enum incrMethod {
	INCR_EXPONENTIAL, //! new = current * 2;
	INCR_LINEAR,      //! new = current + initial;
} incrMethod_t;

#define ELEMENT_USED 0x00000001

typedef struct _genericArray {
	genericInfo_t element; //! description of one element
	unsigned char *array; //! array of elements
	unsigned int *flags; //! array of flags for each element 
	unsigned int num;   //! number of used elements in 'array'
	unsigned int max;  //! number of allocated elements
	unsigned int nInitial;    //! number of initial elements to alloc
	incrMethod_t incrMethod; //! method of increasing elements (see above)
} genericArray;

typedef struct _genericIncrArray {
	genericInfo_t element; //! description of one element
	unsigned char *array; //! array of elements
	unsigned int *flags; //! array of flags for each element 
	unsigned int *used; //! array of used indices into 'array'
	unsigned int num;   //! number of used elements in 'array'
	unsigned int max;    //! number of allocated elements in 'array'
	unsigned int nInitial; //! number of initial elements to alloc
	incrMethod_t incrMethod;  //! method of increasing elements (see above)
} genericIncrArray;

#define genericInsArray genericIncrArray

typedef struct _genericStaticQueue {
	genericInfo_t element; //! description of contained
	unsigned char *array; //! array implementation of a queue 
	unsigned int num;   //! number of used elements in 'queue'
	unsigned int max;  //! number of allocated elements in 'queue'
	unsigned int first; //! first element in the queue. (next one dequeued)
	unsigned int last;   //! last element in the queue. (last queued)
	unsigned int nInitial; //! number of initial elements to alloc
	incrMethod_t incrMethod; //! method of increasing elements (see above)
} genericStaticQueue;

typedef struct _genericQueueNode {
	generic_t *pData;
	struct _genericQueueNode *pNext;
	unsigned int flags;
} genericQueueNode;

typedef struct _genericDynamicQueue {
	genericInfo_t element;
	genericQueueNode base;
	unsigned int num;
} genericDynamicQueue;

typedef struct _genericOctetTreeNode {
	struct _genericOctetTreeNode *pChild[256];
} genericOctetTreeNode;

typedef struct _genericOctetTree {
	genericInfo_t element;
	genericOctetTreeNode base;
	unsigned int num;
} genericOctetTree;

typedef struct _genericHash {
	genericInfo_t element;
	unsigned char *hash; //! array implementation of a hash table
	unsigned int num;   //! number of used elements in 'hash'
	unsigned int max;  //! number of allocated elements in 'queue'
	unsigned int nInitial;    //! number of initial elements to alloc
	incrMethod_t incrMethod; //! method of increasing elements (see above)
} genericHash;	//! mmm. hash brownies

#ifdef __cplusplus
extern "C" {
#endif

int genericIntCmp(generic_t *pGen1, generic_t *pGen2);
extern genericInfo_t genericInt; //! declared beginning of set.c

/*
 * @Array ops
 */
int genericArrayInit(genericArray *pArray, const genericInfo_t *pInfo,
		unsigned int nInitial, incrMethod_t incrMode);
int genericArrayAlloc(genericArray *pArray);
int genericArrayFree(genericArray *pArray);
int genericArrayAdd(genericArray *pArray, generic_t *pGeneric);
int genericArrayDel(genericArray *pArray, unsigned int hGeneric);
int genericArrayFind(genericArray *pArray, generic_t *pGeneric);
int genericArrayIns(genericArray *pArray, generic_t *pGen,
		unsigned int hGeneric);
int genericArrayCopy(genericArray *pDst, genericArray *pSrc);
//int genericArrayCmp(genericArray *pGen1, genericArray *pGen2); //TODO
generic_t *genericArrayGet(genericArray *pArray, unsigned int hGeneric);
generic_t *genericArrayIncr(genericArray *pArray, unsigned int *i);
unsigned int genericArrayCount(genericArray*);

/*!
 * @IncrArray ops
 */
int genericIncrArrayInit(genericIncrArray *pQ, const genericInfo_t *pInfo,
		unsigned int nInitial, incrMethod_t incrMode);
int genericIncrArrayAlloc(genericIncrArray *pIncrArray);
int genericIncrArrayFree(genericIncrArray *pIncrArray);
int genericIncrArrayAdd(genericIncrArray *pIncrArray, generic_t *pGeneric);
int genericIncrArrayDel(genericIncrArray *pIncrArray, unsigned int hGeneric);
int genericIncrArrayFind(genericIncrArray *pIncrArray, generic_t *pGeneric);
int genericIncrArrayIns(genericIncrArray *pArray, generic_t *pGen,
		unsigned int hGeneric);
int genericIncrArrayCopy(genericIncrArray *pDst, genericIncrArray *pSrc);
generic_t *genericIncrArrayGet(genericIncrArray *pIncrArray,
		unsigned int hGeneric);
generic_t *genericIncrArrayIncr(genericIncrArray *pIncrArray, unsigned int *i);
unsigned int genericIncrArrayCount(genericIncrArray*);

/*
 */
#define INCRC genericIncrArray*
#define genericInsArrayInit(a,b,c,d)  genericIncrArrayInit((INCRC)(a),b,c,d)
#define genericInsArrayAlloc(a) genericIncrArrayAlloc((INCRC)(a))
#define genericInsArrayFree(a) genericIncrArrayFree((INCRC)(a))
#define genericInsArrayAdd(a,b) genericIncrArrayAdd((INCRC)(a), b)
#define genericInsArrayDel(a,b) genericIncrArrayDel((INCRC)(a), b)
#define genericInsArrayFind(a,b) genericIncrArrayFind((INCRC)(a), b)
int genericInsArrayIns(genericInsArray *pArray, generic_t *pGen,
		unsigned int hGeneric);
#define genericInsArrayCopy(a,b) genericIncrArrayCopy((INCRC)(a), (INCRC)(b))
#define genericInsArrayGet(a,b) genericIncrArrayGet((INCRC)(a), b)
#define genericInsArrayIncr(a,b) genericIncrArrayIncr((INCRC)(a), b)
#define genericInsArrayCount(a) genericIncrArrayCount((INCRC)(a))

/*
 * @StaticQueue ops
 */
/*
int genericStaticQueueInit(genericStaticQueue *pQ, const genericInfo_t *pInfo,
unsigned int nInitial, incrMethod_t incrMode);
int genericStaticQueueAlloc(genericStaticQueue *pQ);
int genericStaticQueueFree(genericStaticQueue *pQ);
int genericStaticQueueAdd(genericStaticQueue *pQ, generic_t *pGeneric);
int genericStaticQueueDel(genericStaticQueue *pQ, unsigned int hGeneric);
int genericStaticQueueFind(genericStaticQueue *pQ, generic_t *pGeneric);
int genericStaticQueueIns(genericStaticQueue *pQ, generic_t *pGen,
unsigned int hGeneric);
int genericStaticQueueCopy(genericStaticQueue *pDst, genericStaticQueue *pSrc);
//int genericStaticQueueCmp(genericStaticQueue *pQ1, genericStaticQueue *pQ2);
generic_t *genericStaticQueueGet(genericStaticQueue *pQ, unsigned int hGeneric);
generic_t *genericStaticQueueIncr(genericStaticQueue *pQ, unsigned int *i);
 */
/*!
 * @OctetTree ops
 */
int genericOctetTreeInit(genericOctetTree *pOct, const genericInfo_t *pInfo,
		unsigned int nInitial, incrMethod_t incrMode);
int genericOctetTreeAlloc(genericOctetTree *pOct);
int genericOctetTreeFree(genericOctetTree *pOct);
int genericOctetTreeAdd(genericOctetTree *pOct, generic_t *pGeneric);
int genericOctetTreeDel(genericOctetTree *pOct, unsigned int hGeneric);
int genericOctetTreeFind(genericOctetTree *pOct, generic_t *pGeneric);
int genericOctetTreeIns(genericOctetTree *pOct, generic_t *pGen,
		unsigned int hGeneric);
int genericOctetTreeCopy(genericOctetTree *pDst, genericOctetTree *pSrc);
generic_t *genericOctetTreeGet(genericOctetTree *pOct, unsigned int hGeneric);
generic_t *genericOctetTreeIncr(genericOctetTree *pOct, unsigned int *i);
unsigned int genericOctetTreeCount(genericOctetTree*);

/*!
 * @Hash ops
 */
/*
int genericHashInit(genericHash *pHash, const genericInfo_t *pInfo,
unsigned int nInitial, incrMethod_t incrMode);
int genericHashAlloc(genericHash *pHash);
int genericHashFree(genericHash *pHash);
int genericHashAdd(genericHash *pHash, generic_t *pGeneric);
int genericHashDel(genericHash *pHash, int hGeneric);
int genericHashFind(genericHash *pHash, generic_t *pGeneric);
int genericHashInsert(genericHash *pHash, generic_t *pGen, int hGen);
int genericHashCopy(genericHash *pDst, genericHash *pSrc);
generic_t *genericHashGet(genericHash *pHash, int hGeneric);
generic_t *genericHashIncr(genericHash *pHash, unsigned int *i);
 */

#ifdef __cplusplus
}
#endif

#endif

