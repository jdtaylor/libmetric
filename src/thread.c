
/*
 * Library for handling process threads.
 *
 * Copyright (c) 2001-2007,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 *         Rajiv Bakulesh Shah <brainix@gmail.com>
 */

#include <metric/common.h>
#include <metric/thread.h>


int thread_create(thread_t *thread, thread_entry fnEntry, void *arg)
{
#ifndef WIN32
	int i = pthread_create(thread, NULL, fnEntry, arg);
	if(i != 0) {
		VLOG_MSG(LOG_LOW,"thread_create - pthread_create failed with: %d", i);
		return -1;
	}
#else
	*thread = CreateThread(NULL,0, (LPTHREAD_START_ROUTINE)fnEntry, arg,0,NULL);
	if(*thread == NULL) {
		VLOG_MSG(LOG_LOW,"thread_create - CreateThread failed with: %d", i);
		return -1;
	}
#endif
	return 1;
}

int thread_exit()
{
#ifndef WIN32
	pthread_exit(NULL);
#else
	ExitThread(0);
#endif
	return -1; /* shouldn't reach */
}

int thread_wait(thread_t *thread)
{
#ifndef WIN32
	return pthread_join(*thread, NULL) ? -1 : 1;
#else
	return WaitForSingleObject(*thread, INFINITE) == WAIT_FAILED ? -1 : 1;
#endif
}

int mutex_init(mutex_t *mutex)
{
#ifndef WIN32
	return pthread_mutex_init(mutex, NULL) ? -1 : 1;
#else
	*mutex = CreateMutex(NULL, FALSE, NULL);
	return 1;
#endif
}

int mutex_try_lock(mutex_t *mutex)
{
#ifndef WIN32
	switch(pthread_mutex_trylock(mutex))
	{
		case EBUSY : return 0;
		case 0     : return 1;
		default    : return -1;
	}
#else
	return WaitForSingleObject(*mutex, 0) == WAIT_TIMEOUT ? 0 : 1;
#endif
}

int mutex_lock(mutex_t *mutex)
{
#ifndef WIN32
	return pthread_mutex_lock(mutex) ? -1 : 1;
#else
	return WaitForSingleObject(*mutex, INFINITE) == WAIT_FAILED ? -1 : 1;
#endif
}

int mutex_unlock(mutex_t *mutex)
{
#ifndef WIN32
	return pthread_mutex_unlock(mutex) ? -1 : 1;
#else
	return ReleaseMutex(*mutex) ? 1 : -1;
#endif
}

int mutex_destroy(mutex_t *mutex)
{
#ifndef WIN32
	return pthread_mutex_destroy(mutex) ? -1 : 1;
#else
	return CloseHandle(*mutex) ? 1 : -1;
#endif
}

int cond_init(cond_t *cond, void *attr)
{
#ifndef WIN32
	return pthread_cond_init(cond, (pthread_condattr_t*)attr) ? -1 : 1;
#else
	InitializeCriticalSection(&cond->lock);
	cond->count = 0;
	if((cond->sema = CreateSemaphore(NULL, 0, 0x7FFFFFFF, NULL)) == NULL)
		return -1;
	if((cond->done = CreateEvent(NULL, FALSE, FALSE, NULL)) == NULL)
		return -1;
	cond->bcast = FALSE;
	return 1;
#endif
}

int cond_wait(cond_t *cond, mutex_t *mutex)
{
#ifndef WIN32
	return pthread_cond_wait(cond, mutex) ? -1 : 1;
#else
	EnterCriticalSection(&cond->lock);
	cond->count++;
	LeaveCriticalSection(&cond->lock);

	if(SignalObjectAndWait(*mutex, cond->sema, INFINITE, FALSE) == WAIT_FAILED)
		return -1;

	EnterCriticalSection(&cond->lock);
	BOOL waiter = --cond->count || !cond->bcast;
	LeaveCriticalSection(&cond->lock);

	if(!waiter && SignalObjectAndWait(cond->done, *mutex, INFINITE, FALSE) == WAIT_FAILED)
		return -1;
	if(waiter && WaitForSingleObject(*mutex, INFINITE) == WAIT_FAILED)
		return -1;
	return 1;
#endif
}

int cond_signal(cond_t *cond)
{
#ifndef WIN32
	return pthread_cond_signal(cond) ? -1 : 1;
#else
	EnterCriticalSection(&cond->lock);
	BOOL waiter = cond->count;
	LeaveCriticalSection(&cond->lock);

	if(waiter && !ReleaseSemaphore(cond->sema, 1, 0))
		return -1;
	return 1;
#endif
}

int cond_broadcast(cond_t *cond)
{
#ifndef WIN32
	return pthread_cond_broadcast(cond) ? -1 : 1;
#else
	BOOL waiter = FALSE;

	EnterCriticalSection(&cond->lock);
	if(cond->count)
	{
		if(!ReleaseSemaphore(cond->sema, cond->count, 0))
			return -1;
		cond->bcast = TRUE;
		waiter = TRUE;
	}
	LeaveCriticalSection(&cond->lock);

	if(waiter)
	{
		if(WaitForSingleObject(cond->done, INFINITE) == WAIT_FAILED)
			return -1;
		cond->bcast = FALSE;
	}
	return 1;
#endif
}

int cond_destroy(cond_t *cond)
{
#ifndef WIN32
	return pthread_cond_destroy(cond) ? -1 : 1;
#else
	if(!CloseHandle(cond->done))
		return -1;
	if(!CloseHandle(cond->sema))
		return -1;
	DeleteCriticalSection(&cond->lock);
	return 1;
#endif
}


