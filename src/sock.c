
/*
 * Cross-platform Network Socket Routines
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include "config.h"
#ifndef WIN32
  #include <unistd.h>
  #include <sys/mman.h>
#endif
#ifndef HAVE_METRIC_SDLNET
  /* includes for BSD sockets */
  #include <unistd.h>
  #include <sys/socket.h>
  #include <sys/select.h>
  #include <sys/time.h>
  #include <netinet/in.h>
  #include <netdb.h>
  #include <fcntl.h>
#else
  /* includes for SDL_Net */
  #include "SDL/SDL_net.h"
#endif
#include "metric/common.h"
#include "metric/sock.h"
#include "metric/mtime.h"

static int g_bInitialized = FALSE;

/***********************************************
 * @socket definitions
 */

#ifdef HAVE_METRIC_SDLNET
int socketSystemInit()
{
	timerSystemInit(); // commonTime.h
	SDLNet_Init();
	g_bInitialized = TRUE;
	return 1;
}

int socketSystemFree()
{
	g_bInitialized = FALSE;
	SDLNet_Quit();
	//timerSystemFree(); timer system not modular.
	return 1;
}

int socketInit(socket_t *pSock)
{
	if(!g_bInitialized) {
		socketSystemInit();
	}
	*pSock = NULL;
	return 1;
}

int socketConnect(socket_t *pSock, socketAddr_t *pAddr)
{
	*pSock = SDLNet_TCP_Open(pAddr);
	if(*pSock == NULL) {
		return -1;
	}
	return 1;
}

int socketListen(socket_t *pSock, socketAddr_t *pAddr)
{
	*pSock = SDLNet_TCP_Open(pAddr);
	if(*pSock == NULL) {
		return -1;
	}
	return 1;
}

int socketOptionGet(socket_t *pSock, int opt, int *value)
{
	return 1;
}

int socketOptionSet(socket_t *pSock, int opt, int value)
{
	return 1;
}

int socketClose(socket_t *pSock)
{
	if(*pSock != NULL)
		SDLNet_TCP_Close(*pSock);
	*pSock = NULL;
	return 1;
}

int socketValid(socket_t *pSock)
{
	if(*pSock != NULL)
		return TRUE;
	return FALSE;
}

int socketRead(socket_t *pSock, void *pData, size_t nBytes)
{
	if(*pSock == NULL) {
		LOG_MSG(LOG_LOW,"socketRead - socket NULL");
		return -1;
	}
	return SDLNet_TCP_Recv(*pSock, pData, nBytes);
}

int socketWrite(socket_t *pSock, void *pData, size_t nBytes)
{
	if(*pSock == NULL) {
		LOG_MSG(LOG_LOW,"socketWrite - socket NULL");
		return -1;
	}
	SDLNet_TCP_Send(*pSock, pData, nBytes);
	return 1;
}

int socketAccept(socket_t *pSock, socket_t *pNewSock, socketAddr_t *pAddr)
{
	*pNewSock = SDLNet_TCP_Accept(*pSock);
	if(*pNewSock == NULL)
		return -1;
	*pAddr = *SDLNet_TCP_GetPeerAddress(*pNewSock);
	return 1;
}

int socketSetInit(socketSet_t *pSockSet)
{
	*pSockSet = NULL;
	return 1;
}

int socketCopy(socket_t *pDst, socket_t *pSrc)
{
	*pDst = *pSrc;
	return 1;
}

int socketSetAlloc(socketSet_t *pSockSet, unsigned int maxSockets)
{
	*pSockSet = SDLNet_AllocSocketSet(maxSockets);
	return 1;
}

int socketSetFree(socketSet_t *pSockSet)
{
	// TODO: undefined reference when win32 linking.  dunno why
	//SDL_FreeSocketSet(*pSockSet);
	return 1;
}

int socketSetAdd(socketSet_t *pSockSet, socket_t *pSock)
{
	if(*pSock == NULL) {
		return -1;
	}
	SDLNet_TCP_AddSocket(*pSockSet, *pSock);
	return 1;
}

int socketSetDel(socketSet_t *pSockSet, socket_t *pSock)
{
	if(*pSock == NULL)
		return -1;
	SDLNet_TCP_DelSocket(*pSockSet, *pSock);
	return 1;
}

int socketSetCheck(socketSet_t *pSockSet, long msTimeout, socketSet_t *pDst)
{
	*pDst = *pSockSet;
	return SDLNet_CheckSockets(*pSockSet, (int)msTimeout);
}

int socketSetReady(socketSet_t *pReadySockSet, socket_t *pSock)
{
	if(*pSock == NULL)
		return FALSE;
	return SDLNet_SocketReady(*pSock);
}

int socketAddrInit(socketAddr_t *pSockAddr)
{
	pSockAddr->host = 0; // 32 bit ipv4 address
	pSockAddr->port = 0; // 16 bit protocol address
	return 1;
}

int socketAddrCopy(socketAddr_t *pDst, socketAddr_t *pSrc)
{
	pDst->host = pSrc->host;
	pDst->port = pSrc->port;
	return 1;
}

int socketAddrNameSet(socketAddr_t *pSockAddr, char *hostname, short port)
{
	if(SDLNet_ResolveHost(pSockAddr, hostname, port) < 0) {
		VLOG_MSG(LOG_USER,"Couldn't resolve hostname: %s", hostname);
		return 0;
	}
	return 1;
}

int socketAddrNameGet(socketAddr_t *pSockAddr, char *hostname, short *port)
{
	char *staticBuf;
	staticBuf = SDLNet_ResolveIP(pSockAddr);
	if(staticBuf == NULL) {
		hostname[0] = '\0';
		*port = 0;
		return 0;
	}
	strncpy(hostname, staticBuf, MAX_HOST_NAME);
	*port = pSockAddr->port;
	return 1;
}

#else
int socketSystemInit()
{
	timerSystemInit(); // commonTime.h
	g_bInitialized = TRUE;
	return 1;
}

int socketSystemFree()
{
	g_bInitialized = FALSE;
	return 1;
}

int socketInit(socket_t *pSock)
{
	if(!g_bInitialized) {
		socketSystemInit();
	}
	*pSock = 0;
	return 1;
}

int socketCopy(socket_t *pDst, socket_t *pSrc)
{
	*pDst = *pSrc;
	return 1;
}

int socketConnect(socket_t *pSock, socketAddr_t *pAddr)
{
	int fd;
	struct sockaddr_in sa;
	socklen_t sa_len;
	// remote host, start network connection
	fd = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
	if(fd < 0) {
		LOG_MSG(LOG_SYS,"socketConnect - socket failed");
		return -1;
	}
	sa_len = sizeof(sa);
	memset(&sa, 0, sa_len);
	sa.sin_family = AF_INET;
	// connect to remote host
	sa.sin_port = htons(pAddr->port);
	sa.sin_addr.s_addr = htonl(pAddr->ip);
	VLOG_MSG(LOG_USER,"Attempting connect to host: %d", pAddr->ip);
	if(connect(fd, (struct sockaddr*)&sa, sa_len) < 0) {
		close(fd);
		return -1;
	}
	VLOG_MSG(LOG_USER,"Connection to %d successfull.", pAddr->ip);
	*pSock = (unsigned int)fd;
	return 1;
}

int socketListen(socket_t *pSock, socketAddr_t *pAddr)
{
	int fd;
	struct sockaddr_in sa;
	socklen_t sa_len;
	fd = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
	if(fd < 0) {
		LOG_MSG(LOG_SYS,"socketListen - socket failed");
		return -1;
	}
	sa_len = sizeof(sa);
	memset(&sa, 0, sa_len);
	sa.sin_family = AF_INET;
	sa.sin_port = htons(pAddr->port);
	sa.sin_addr.s_addr = htonl(pAddr->ip);
	if(bind(fd, (struct sockaddr*)&sa, sa_len) < 0) {
		close(fd);
		return -1;
	}
	// TODO: WTF do I do with the '5' here?
	if(listen(fd, 5) < 0) {
		close(fd);
		return -1;
	}
	VLOG_MSG(LOG_USER,"Listening on %d.%d.%d.%d : %d",
			(pAddr->ip & 0xFF000000) >> 24,
			(pAddr->ip & 0xFF0000) >> 16,
			(pAddr->ip & 0xFF00) >> 8,
			(pAddr->ip & 0xFF),
			pAddr->port);
	*pSock = (unsigned int)fd;
	return 1;
}

int socketOptionGet(socket_t *pSock, int opt, int *value)
{
	int flags;
	socklen_t optlen;
	switch(opt) {
		case SOCK_OPT_BLOCKING:
			flags = fcntl(*pSock, F_GETFL, 0);
			if(flags < 0) {
				VLOG_MSG(LOG_SYS,"socketOptionGet - fcntl returned: %d", flags);
				return -1;
			}
			return flags & O_NONBLOCK ? 0 : 1;
		case SOCK_OPT_READY_BYTES:
			optlen = sizeof(int);
			if(getsockopt(*pSock, SOL_SOCKET, SO_RCVLOWAT, value, &optlen) < 0)
			{
				VLOG_MSG(LOG_SYS,"socketOptionGet - getsockopt failed %d", opt);
				return -1;
			}
			return 1;
		default:
			VLOG_MSG(LOG_LOW,"socketOptionGet - unknown opt: %d", opt);
			return -1;
	}
	return 1;
}

int socketOptionSet(socket_t *pSock, int opt, int value)
{
	int flags;
	int optlen;
	switch(opt)
	{
		case SOCK_OPT_BLOCKING:
			flags = fcntl(*pSock, F_GETFL, 0);
			if(flags < 0) {
				VLOG_MSG(LOG_SYS,"socketOptionSet - fcntl GETFL: %d", flags);
				return -1;
			}
			if(value)
				flags &= ~O_NONBLOCK;
			else
				flags |= O_NONBLOCK;
			flags = fcntl(*pSock, F_SETFL, flags);
			if(flags < 0) {
				VLOG_MSG(LOG_SYS,"socketOptionSet - fcntl SETFL: %d", flags);
				return -1;
			}
			return 1;
		case SOCK_OPT_READY_BYTES:
			optlen = sizeof(int);
			if(setsockopt(*pSock, SOL_SOCKET, SO_RCVLOWAT, &value, optlen) < 0)
			{
				VLOG_MSG(LOG_SYS,"socketOptionSet - setsockopt failed %d", opt);
				return -1;
			}
			return 1;
		default:
			VLOG_MSG(LOG_LOW,"socketOptionSet - unknown opt: %d", opt);
			return -1;
	}
	return 1;
}

int socketClose(socket_t *pSock)
{
	if(*pSock != 0) {
		close(*pSock);
		*pSock = 0;
	}
	return 1;
}

int socketValid(socket_t *pSock)
{
	// TODO?
	return TRUE;
}

int socketRead(socket_t *pSock, void *pData, size_t nBytes)
{
	ssize_t nread;
	unsigned int fd = *pSock;
	const char *ptr = (const char*)pData;
	size_t nleft = nBytes;

	while(nleft > 0)
	{
		if((nread = read(fd, (void*)ptr, nleft)) < 0)
		{
			if(errno == EINTR) // interrupt by a signal, try again
				nread = 0; /* and call read() again */
			else if(errno == EWOULDBLOCK || errno == EAGAIN) {
				return (nBytes - nleft); // non blocking and no data ready
			} else {
				VLOG_MSG(LOG_SYS,"socketRead - read failed errno: %d", errno);
				return -1;
			}
		} else if(nread == 0)
			return -1;  /* EOF (socket closed connection) */
		nleft -= nread;
		ptr += nread;
	}
	return nBytes - nleft;
}

int socketWrite(socket_t *pSock, void *pData, size_t nBytes)
{
	ssize_t nwritten; // is this a word?
	unsigned int fd = *pSock;
	const char *ptr = (const char*)pData;
	size_t nleft = nBytes;

	while(nleft > 0)
	{
		if((nwritten = write(fd, ptr, nleft)) <= 0)
		{
			if(errno == EINTR || errno == EAGAIN)
				nwritten = 0; /* and call write() again */
			else if(errno == EWOULDBLOCK) {
				return (nBytes - nleft); // non blocking and no buffer ready
			} else {
				VLOG_MSG(LOG_SYS,"socketWrite - write returned: %d", nwritten);
				return -1;  /* error */
			}
		}
		nleft -= nwritten;
		ptr += nwritten;
	}
	return nBytes - nleft;
}

int socketAccept(socket_t *pSock, socket_t *pNewSock, socketAddr_t *pAddr)
{
	int clientFD;
	struct sockaddr_in sa;
	socklen_t sa_len = sizeof(sa);
	//
	// get information about the remote host
	clientFD = accept(*pSock, (struct sockaddr*)&sa, &sa_len);
	if(clientFD < 0) {
		if(errno == EWOULDBLOCK)
			return 0; // non blocking behavior
		LOG_MSG(LOG_SYS,"socketAccept - accept failed");
		return -1;
	}
	// network->host byte order for tcp port
	pAddr->port = ntohs(sa.sin_port);
	// network->host for ip address
	pAddr->ip = ntohl(sa.sin_addr.s_addr);
	*pNewSock = (unsigned int)clientFD;
	return 1;
}

int socketSetInit(socketSet_t *pSockSet)
{
	FD_ZERO(&pSockSet->set);
	pSockSet->largestFD = 0;
	return 1;
}

int socketSetAlloc(socketSet_t *pSockSet, unsigned int maxSockets)
{
	FD_ZERO(&pSockSet->set);
	pSockSet->largestFD = 0;
	return 1;
}

int socketSetFree(socketSet_t *pSockSet)
{
	FD_ZERO(&pSockSet->set);
	pSockSet->largestFD = 0;
	return 1;
}

int socketSetAdd(socketSet_t *pSockSet, socket_t *pSock)
{
	if(*pSock > pSockSet->largestFD)
		pSockSet->largestFD = *pSock;
	FD_SET(*pSock, &pSockSet->set);
	return 1;
}

int socketSetDel(socketSet_t *pSockSet, socket_t *pSock)
{
	// FUCK! we need a way to reconstruct largestFD here. :(
	FD_CLR(*pSock, &pSockSet->set);
	return 1;
}

int socketSetCheck(socketSet_t *pSockSet, long msTimeout, socketSet_t *pDst)
{
	int i;
	struct timeval waitTime;
	struct timeval *pWaitTime;
	if(msTimeout < 0) {
		pWaitTime = NULL; // wait indefinately 
	} else {
		waitTime.tv_sec = 0; // seconds
		waitTime.tv_usec = msTimeout; // milliseconds
		pWaitTime = &waitTime;
	}
	// select modifies the socketSet, so make a copy first
	// TODO: proper way to copy a fd_set?
	pDst->set = pSockSet->set;
	// poll for any incoming data
	i = select(pSockSet->largestFD+1, &pDst->set, NULL, NULL, pWaitTime);
	if(i < 0) {
		// TODO: I think I should be deleting failed sockets?
		if(errno == EINTR) //TODO: call select again since no timeout
			return 0; // interrupted before timeout but no sockets ready
		VLOG_MSG(LOG_SYS,"socketSetCheck - select failed: %d", errno);
		return i;
	}
	return i; // at least 1 socket is ready to transfer
}


int socketSetReady(socketSet_t *pReadySockSet, socket_t *pSock)
{
	if(FD_ISSET(*pSock, &pReadySockSet->set))
		return TRUE;
	return FALSE;
}

int socketAddrInit(socketAddr_t *pSockAddr)
{
	pSockAddr->ip = 0;
	pSockAddr->port = 0;
	return 1;
}

int socketAddrCopy(socketAddr_t *pDst, socketAddr_t *pSrc)
{
	pDst->ip = pSrc->ip;
	pDst->port = pSrc->port;
	return 1;
}

int socketAddrNameSet(socketAddr_t *pSockAddr, char *hostname, short port)
{
	struct hostent *hostlist;
	long ip;
	if(hostname == NULL || hostname[0] == '\0') {
		pSockAddr->ip = 0;
		pSockAddr->port = port;
		return 1;
	}
	hostlist = gethostbyname(hostname);
	if(hostlist == NULL) {
		VLOG_MSG(LOG_USER,"Couln't resolve hostname: %s", hostname);
		return 0;
	}
	if(hostlist->h_addrtype != AF_INET) {
		VLOG_MSG(LOG_LOW,"Host (%s) doesn't support IPv4 addresses", hostname);
		return 0;
	}
	memcpy(&ip, hostlist->h_addr_list[0], hostlist->h_length); 
	pSockAddr->ip = ntohl(ip);
	pSockAddr->port = port;
	/* inet_pton(AF_INET, "192.168.3.1", &w->ip); */
	return 1;
}

int socketAddrNameGet(socketAddr_t *pSockAddr, char *hostname, short *port)
{
	snprintf(hostname, MAX_HOST_NAME, "%d.%d.%d.%d",
			(int)(pSockAddr->ip & 0xFF000000) >> 24,
			(int)(pSockAddr->ip & 0xFF0000) >> 16,
			(int)(pSockAddr->ip & 0xFF00) >> 8,
			(int)(pSockAddr->ip & 0xFF));
	*port = pSockAddr->port;
	return 1;
}

#endif



