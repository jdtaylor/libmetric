
/*
 * Utility for PAK files
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#include <stdio.h>
#include <getopt.h>
#define GPAK
#include <metric/pak.h>

int main(int argc, char *argv[])
{
	char c, *dir, *pakFile;

	if(argc < 3) {
		printf("usage:\n");
		printf("create:  %s -c <pakFile> <srcDir>\n", argv[0]);
		printf("extract: %s -x <pakFile>\n", argv[0]);
		printf("list:    %s -l <pakFile>\n", argv[0]);
		return 0;
	}

	while((c = getopt(argc, argv, "c:x:l:")) != EOF)
	{
		switch(c)
		{
		case 'c': /* create pak file */
			pakFile = optarg;
			dir = argv[optind];
			pakCreateFromDir(pakFile, dir, "test");
			break;
		case 'x': /* extract pak file */
			pakFile = optarg;
			break;
		case 'l': /* list pak contents */
			pakFile = optarg;
			pakPrintContents(pakFile);
			break;
		}
	}
	return 1;
}
