
/*
 * Cross-platform File Access Routines
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#ifndef METRIC_FS_H
#define METRIC_FS_H


#ifdef WIN32
  #include <windows.h>
#endif

/* cross platform file facilities
 */
#ifdef WIN32
    /* morph to handle win32 file descriptors */
    /* did I mention that I want eternal pain for MS? */
    typedef HANDLE file_des;
#   define FILE_SEEK_SET FILE_BEGIN
#   define FILE_SEEK_CURRENT FILE_CURRENT
#   define FILE_SEEK_END FILE_END
#else
    /* POSIX */
    typedef int file_des;
#   define FILE_SEEK_SET SEEK_SET
#   define FILE_SEEK_CURRENT SEEK_CUR
#   define FILE_SEEK_END SEEK_END
#endif

#ifdef __cplusplus
extern "C" {
#endif

int fileWrite(file_des fd, void *buf, int n);
int fileRead(file_des fd, void *buf, int n);
int fileSeek(file_des fd, int n, int whence);
void *fileMap(file_des fd, int offset, int len);
int fileUnMap(void *addr, int len);
int fileClose(file_des fd);

#ifdef __cplusplus
}
#endif

#endif

