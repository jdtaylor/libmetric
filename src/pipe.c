
/*
 * Pipe Routines
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <metric/common.h>
#include <metric/pipe.h>


int pipeInit(pipe_t *pPipe)
{
	pPipe->flags = 0; // unsets PIPE_CREATED
	return 1;
}

int pipeCopy(pipe_t *pDst, pipe_t *pSrc)
{
	pDst->flags = pSrc->flags;
	pDst->parentRead = pSrc->parentRead;
	pDst->parentWrite = pSrc->parentWrite;
	pDst->childRead = pSrc->childRead;
	pDst->childWrite = pSrc->childWrite;
	pDst->errRead = pSrc->errRead;
	pDst->errWrite = pSrc->errWrite;
	return 1;
}

int pipeCreate(pipe_t *pPipe, unsigned int flags)
{
	int fd[2];
	if(flags & PIPE_CREATED) {
		LOG_MSG(LOG_LOW,"pipeCreate - pipe already created");
		return -1;
	}
	if(flags & PIPE_UNUSED) {
		VLOG_MSG(LOG_LOW,"pipeCreate - invalid set of flags: %d", flags);
		return -1;
	}
	if((flags & (PIPE_READ | PIPE_WRITE | PIPE_ERR)) == 0) {
		LOG_MSG(LOG_LOW,"pipeCreate - no operations to perform.");
		return -1;
	}
	pPipe->flags = 0; // unsets PIPE_CREATED
	if(flags & PIPE_READ) {
		if(pipe(fd)) {
			LOG_MSG(LOG_SYS,"pipeCreate - pipe for reads failed");
			return -1;
		}
		pPipe->flags |= PIPE_READ;
		pPipe->parentRead = fd[0];
		pPipe->childWrite = fd[1];
	}
	if(flags & PIPE_WRITE) {
		if(pipe(fd)) {
			LOG_MSG(LOG_SYS,"pipeCreate - pipe for writes failed");
			pipeDestroy(pPipe);
			return -1;
		}
		pPipe->flags |= PIPE_WRITE;
		pPipe->parentWrite = fd[1];
		pPipe->childRead = fd[0];
	}
	if(flags & PIPE_ERR) {
		if(pipe(fd)) {
			LOG_MSG(LOG_SYS,"pipeCreate - pipe for errors failed");
			pipeDestroy(pPipe);
			return -1;
		}
		pPipe->flags |= PIPE_ERR;
		pPipe->errWrite = fd[1];
		pPipe->errRead = fd[0];
	}
	pPipe->flags |= PIPE_CREATED;
	return 1;
}

int pipeCreated(pipe_t *pPipe)
{
	return pPipe->flags & PIPE_CREATED ? 1 : 0;
}

int pipeDestroy(pipe_t *pPipe)
{
	if(!pipeCreated(pPipe)) {
		LOG_MSG(LOG_LOW,"pipeDestroy - pipe not created yet");
		return -1;
	}
	if(pipeAttached(pPipe)) {
		//
		// file descriptors of both parent and child need to be closed
		if(pPipe->flags & PIPE_READ) {
			close(pPipe->parentRead);
			close(pPipe->childWrite);
		}
		if(pPipe->flags & PIPE_WRITE) {
			close(pPipe->parentWrite);
			close(pPipe->parentRead);
		}
		if(pPipe->flags & PIPE_ERR) {
			close(pPipe->errRead);
			close(pPipe->errWrite);
		}
	}
	if(pPipe->flags & PIPE_PARENT) {
		//
		// pipe is attached to parent side.  Child side is already closed
		if(pPipe->flags & PIPE_READ)
			close(pPipe->parentRead);
		if(pPipe->flags & PIPE_WRITE)
			close(pPipe->parentWrite);
		if(pPipe->flags & PIPE_ERR)
			close(pPipe->errRead);
	}
	if(pPipe->flags & PIPE_CHILD) {
		//
		// pipe is attached to child side.  Parent side is already closed
		if(pPipe->flags & PIPE_READ)
			close(pPipe->childRead);
		if(pPipe->flags & PIPE_WRITE)
			close(pPipe->childWrite);
		if(pPipe->flags & PIPE_ERR)
			close(pPipe->childRead);
	}
	pPipe->flags &= ~PIPE_CREATED; // unset PIPE_CREATED
	return 1;
}

int pipeAttach(pipe_t *pPipe, int bParent)
{
	if(!pipeCreated(pPipe)) {
		LOG_MSG(LOG_LOW,"pipeAttach - pipe not created yet");
		return -1;
	}
	if(pipeAttached(pPipe)) {
		LOG_MSG(LOG_LOW,"pipeAttach - pipe already attached");
		return -1;
	}
	if(bParent) {
		//
		// we are parent, so close child's file descriptors
		if(pPipe->flags & PIPE_READ)
			close(pPipe->childWrite);
		if(pPipe->flags & PIPE_WRITE)
			close(pPipe->childRead);
		if(pPipe->flags & PIPE_ERR)
			close(pPipe->errWrite);
		pPipe->flags |= PIPE_PARENT;
	} else {
		//
		// we are child, so close parent's file descriptors
		if(pPipe->flags & PIPE_READ)
			close(pPipe->parentRead);
		if(pPipe->flags & PIPE_WRITE)
			close(pPipe->parentWrite);
		if(pPipe->flags & PIPE_ERR)
			close(pPipe->errRead);
		pPipe->flags |= PIPE_CHILD;
	}
	return 1;
}

int pipeAttached(pipe_t *pPipe)
{
	if((pPipe->flags & (PIPE_PARENT | PIPE_CHILD)) == 0)
		return FALSE; // not attached yet
	if((pPipe->flags & PIPE_PARENT) && (pPipe->flags & PIPE_CHILD))
		return FALSE; // cannot be both parent and child
	return TRUE;
}

int pipeOptionGet(pipe_t *pPipe, int opt, int *value)
{
	int tmp;
	int flags;

	tmp = 0;
	if(pPipe->flags & PIPE_READ)
		tmp++;
	if(pPipe->flags & PIPE_WRITE)
		tmp++;
	if(pPipe->flags & PIPE_ERR)
		tmp++;
	if(tmp >= 2) {
		logMsg(LOG_WARN,"pipeOptionGet - only one option target supported!");
		// uses whichever we find first
	}
	if(pPipe->flags & PIPE_PARENT)
	{
		if(opt & PIPE_READ)
		{
			flags = fcntl(pPipe->parentRead, F_GETFL, 0);
			if(flags < 0)
				goto fcntl_err;
			if(opt & PIPE_BLOCK)
				return flags & O_NONBLOCK ? 1 : 0;
			if(opt & PIPE_NONBLOCK)
				return flags & O_NONBLOCK ? 0 : 1;
		}
		if(opt & PIPE_WRITE)
		{
			flags = fcntl(pPipe->parentWrite, F_GETFL, 0);
			if(flags < 0)
				goto fcntl_err;
			if(opt & PIPE_BLOCK)
				return flags & O_NONBLOCK ? 1 : 0;
			if(opt & PIPE_NONBLOCK)
				return flags & O_NONBLOCK ? 0 : 1;
		}
		if(opt & PIPE_ERR)
		{
			flags = fcntl(pPipe->errRead, F_GETFL, 0);
			if(flags < 0)
				goto fcntl_err;
			if(opt & PIPE_BLOCK)
				return flags & O_NONBLOCK ? 1 : 0;
			if(opt & PIPE_NONBLOCK)
				return flags & O_NONBLOCK ? 0 : 1;
		}
	}
	if(pPipe->flags & PIPE_CHILD)
	{
		if(opt & PIPE_READ)
		{
			flags = fcntl(pPipe->childRead, F_GETFL, 0);
			if(flags < 0)
				goto fcntl_err;
			if(opt & PIPE_BLOCK)
				return flags & O_NONBLOCK ? 1 : 0;
			if(opt & PIPE_NONBLOCK)
				return flags & O_NONBLOCK ? 0 : 1;
		}
		if(opt & PIPE_WRITE)
		{
			flags = fcntl(pPipe->childWrite, F_GETFL, 0);
			if(flags < 0)
				goto fcntl_err;
			if(opt & PIPE_BLOCK)
				return flags & O_NONBLOCK ? 1 : 0;
			if(opt & PIPE_NONBLOCK)
				return flags & O_NONBLOCK ? 0 : 1;
		}
		if(opt & PIPE_ERR)
		{
			flags = fcntl(pPipe->errWrite, F_GETFL, 0);
			if(flags < 0)
				goto fcntl_err;
			if(opt & PIPE_BLOCK)
				return flags & O_NONBLOCK ? 1 : 0;
			if(opt & PIPE_NONBLOCK)
				return flags & O_NONBLOCK ? 0 : 1;
		}
	}
	VLOG_MSG(LOG_SYS,"pipeOptionGet - unknown options: %d", opt);
	return -1;
fcntl_err:
	VLOG_MSG(LOG_SYS,"pipeOptionGet - fcntl returned: %d", flags);
	return -1;
}

int pipeOptionSet(pipe_t *pPipe, int opt, int *value)
{
	int flags;

	if(pPipe->flags & PIPE_PARENT)
	{
		if(opt & PIPE_READ)
		{
			flags = fcntl(pPipe->parentRead, F_GETFL, 0);
			if(flags < 0)
				goto fcntl_err;
			if(opt & PIPE_BLOCK)
				flags &= ~O_NONBLOCK;
			if(opt & PIPE_NONBLOCK)
				flags |= O_NONBLOCK;
			flags = fcntl(pPipe->parentRead, F_SETFL, flags);
			if(flags < 0)
				goto fcntl_err;
		}
		if(opt & PIPE_WRITE)
		{
			flags = fcntl(pPipe->parentWrite, F_GETFL, 0);
			if(flags < 0)
				goto fcntl_err;
			if(opt & PIPE_BLOCK)
				flags &= ~O_NONBLOCK;
			if(opt & PIPE_NONBLOCK)
				flags |= O_NONBLOCK;
			flags = fcntl(pPipe->parentWrite, F_SETFL, flags);
			if(flags < 0)
				goto fcntl_err;
		}
		if(opt & PIPE_ERR)
		{
			flags = fcntl(pPipe->errRead, F_GETFL, 0);
			if(flags < 0)
				goto fcntl_err;
			if(opt & PIPE_BLOCK)
				flags &= ~O_NONBLOCK;
			if(opt & PIPE_NONBLOCK)
				flags |= O_NONBLOCK;
			flags = fcntl(pPipe->errRead, F_SETFL, flags);
			if(flags < 0)
				goto fcntl_err;
		}
	}
	if(pPipe->flags & PIPE_CHILD)
	{
		if(opt & PIPE_READ)
		{
			flags = fcntl(pPipe->childRead, F_GETFL, 0);
			if(flags < 0)
				goto fcntl_err;
			if(opt & PIPE_BLOCK)
				flags &= ~O_NONBLOCK;
			if(opt & PIPE_NONBLOCK)
				flags |= O_NONBLOCK;
			flags = fcntl(pPipe->childRead, F_SETFL, flags);
			if(flags < 0)
				goto fcntl_err;
		}
		if(opt & PIPE_WRITE)
		{
			flags = fcntl(pPipe->childWrite, F_GETFL, 0);
			if(flags < 0)
				goto fcntl_err;
			if(opt & PIPE_BLOCK)
				flags &= ~O_NONBLOCK;
			if(opt & PIPE_NONBLOCK)
				flags |= O_NONBLOCK;
			flags = fcntl(pPipe->childWrite, F_SETFL, flags);
			if(flags < 0)
				goto fcntl_err;
		}
		if(opt & PIPE_ERR)
		{
			flags = fcntl(pPipe->errWrite, F_GETFL, 0);
			if(flags < 0)
				goto fcntl_err;
			if(opt & PIPE_BLOCK)
				flags &= ~O_NONBLOCK;
			if(opt & PIPE_NONBLOCK)
				flags |= O_NONBLOCK;
			flags = fcntl(pPipe->errWrite, F_SETFL, flags);
			if(flags < 0)
				goto fcntl_err;
		}
	}
	return 1;
fcntl_err:
	VLOG_MSG(LOG_SYS,"pipeOptionSet - fcntl returned: %d", flags);
	return -1;
}

int pipeRead(pipe_t *pPipe, void *pData, size_t nBytes)
{
	ssize_t nread;
	unsigned int fd;
	const char *ptr = (const char*)pData;
	size_t nleft = nBytes;

	if(!pipeCreated(pPipe)) {
		LOG_MSG(LOG_LOW,"pipeRead - pipe not created yet");
		return -1;
	}
	if(!pipeAttached(pPipe)) {
		LOG_MSG(LOG_LOW,"pipeRead - pipe not attached yet");
		return -1;
	}
	if(pPipe->flags & PIPE_PARENT)
		fd = pPipe->parentRead;
	else
		fd = pPipe->childRead;
	while(nleft > 0) {
		if((nread = read(fd, (void*)ptr, nleft)) < 0) {
			if(errno == EINTR) // interrupt by a signal, try again
				nread = 0; /* and call read() again */
			else if(errno == EWOULDBLOCK || errno == EAGAIN) {
				return (nBytes - nleft); // non blocking and no data ready
			} else {
				VLOG_MSG(LOG_SYS,"pipeRead - read failed errno: %d", errno);
				return -1;
			}
		} else if(nread == 0)
			return -1;  /* EOF (pipe closed connection) */
		nleft -= nread;
		ptr += nread;
	}
	return (nBytes - nleft);
}

int pipeSmoke(pipe_t *pPipe)
{
	return 420; //! nature is illegal?
	//  /me lives in the United authoritarian States of America
}

int pipeWrite(pipe_t *pPipe, void *pData, size_t nBytes)
{
	ssize_t nwritten; // is this a word?
	unsigned int fd;
	const char *ptr = (const char*)pData;
	size_t nleft = nBytes;

	if(!pipeCreated(pPipe)) {
		LOG_MSG(LOG_LOW,"pipeWrite - pipe not created yet");
		return -1;
	}
	if(!pipeAttached(pPipe)) {
		LOG_MSG(LOG_LOW,"pipeWrite - pipe not attached yet");
		return -1;
	}
	if(pPipe->flags & PIPE_PARENT)
		fd = pPipe->parentWrite;
	else
		fd = pPipe->childWrite;
	while(nleft > 0) {
		if((nwritten = write(fd, (void*)ptr, nleft)) <= 0) {
			if(errno == EINTR || errno == EAGAIN)
				nwritten = 0; /* and call write() again */
			else if(errno == EWOULDBLOCK) {
				return (nBytes - nleft); // non blocking and no buffer ready
			} else {
				VLOG_MSG(LOG_SYS,"pipeWrite - write returned: %d", nwritten);
				return -1;  /* error */
			}
		}
		nleft -= nwritten;
		ptr += nwritten;
	}
	return (nBytes - nleft);
}

int pipeFileno(pipe_t *pPipe, int *read, int *write, int *err)
{
	if(!pipeAttached(pPipe)) {
		LOG_MSG(LOG_LOW,"pipeFileno - pipe is required to be attached first.");
		return -1;
	}
	if(pPipe->flags & PIPE_PARENT) {
		//
		// pipe is attached to parent
		if(read != NULL) {
			if(pPipe->flags & PIPE_READ)
				*read = pPipe->parentRead;
			else
				*read = -1;
		}
		if(write != NULL) {
			if(pPipe->flags & PIPE_WRITE)
				*write = pPipe->parentWrite;
			else
				*write = -1;
		}
	} else {
		//
		// pipe is attached to child
		if(read != NULL) {
			if(pPipe->flags & PIPE_READ)
				*read = pPipe->childRead;
			else
				*read = -1;
		}
		if(write != NULL) {
			if(pPipe->flags & PIPE_WRITE)
				*write = pPipe->childWrite;
			else
				*write = -1;
		}
	}
	if(err != NULL) {
		if(pPipe->flags & PIPE_ERR)
			*err = pPipe->errRead;
		else
			*err = -1;
	}
	return 1;
}

int pipeDupStdio(pipe_t *pPipe)
{
	if(!pipeAttached(pPipe)) {
		LOG_MSG(LOG_LOW,"pipeDupStdio - pipe needs to be attached first.");
		return -1;
	}
	if(pPipe->flags & PIPE_PARENT) {
		//
		// pipe is attached to parent
		if(pPipe->flags & PIPE_READ) {
			//
			// don't close(), if parentRead is already STDIN
			if(pPipe->parentRead != STDIN_FILENO) {
				if(dup2(pPipe->parentRead, STDIN_FILENO) != STDIN_FILENO) {
					LOG_MSG(LOG_SYS,"pipDupStdio - parentRead dup");
					return -1;
				}
				close(pPipe->parentRead);
				pPipe->parentRead = STDIN_FILENO;
			}
		}
		if(pPipe->flags & PIPE_WRITE) {
			//
			// don't close(), if parentWrite is already STDOUT
			if(pPipe->parentWrite != STDOUT_FILENO) {
				if(dup2(pPipe->parentWrite, STDOUT_FILENO) != STDOUT_FILENO) {
					LOG_MSG(LOG_SYS,"pipDupStdio - parentWrite dup");
					return -1;
				}
				close(pPipe->parentWrite);
				pPipe->parentWrite = STDOUT_FILENO;
			}
		}
		if(pPipe->flags & PIPE_ERR) {
			//
			// we are the parent, so we dup the reading end of the err pipe
			if(pPipe->errRead != STDERR_FILENO) {
				if(dup2(pPipe->errRead, STDERR_FILENO) != STDERR_FILENO) {
					LOG_MSG(LOG_SYS,"pipDupStdio - errRead dup");
					return -1;
				}
				close(pPipe->errRead);
				pPipe->errRead = STDERR_FILENO;
			}
		}
	} else {
		//
		// pipe is attached to child
		if(pPipe->flags & PIPE_READ) {
			//
			// don't close(), if childRead is already STDIN
			if(pPipe->childRead != STDIN_FILENO) {
				if(dup2(pPipe->childRead, STDIN_FILENO) != STDIN_FILENO) {
					LOG_MSG(LOG_SYS,"pipDupStdio - childRead dup");
					return -1;
				}
				close(pPipe->childRead);
				pPipe->childRead = STDIN_FILENO;
			}
		}
		if(pPipe->flags & PIPE_WRITE) {
			//
			// don't close(), if childWrite is already STDOUT
			if(pPipe->childWrite != STDOUT_FILENO) {
				if(dup2(pPipe->childWrite, STDOUT_FILENO) != STDOUT_FILENO) {
					LOG_MSG(LOG_SYS,"pipDupStdio - childWrite dup");
					return -1;
				}
				close(pPipe->childWrite);
				pPipe->childWrite = STDOUT_FILENO;
			}
		}
		if(pPipe->flags & PIPE_ERR) {
			//
			// we are the child, so we dup the writing end of the err pipe
			if(pPipe->errWrite != STDERR_FILENO) {
				if(dup2(pPipe->errWrite, STDERR_FILENO) != STDERR_FILENO) {
					LOG_MSG(LOG_SYS,"pipDupStdio - errWrite dup");
					return -1;
				}
				close(pPipe->errWrite);
				pPipe->errWrite = STDERR_FILENO;
			}
		}
	}
	return 1;
}

int pipeFromStdio(pipe_t *pPipe)
{
	pPipe->flags &= ~PIPE_PARENT;
	pPipe->flags |= PIPE_CHILD | PIPE_READ | PIPE_WRITE | PIPE_ERR;
	pPipe->childRead = STDIN_FILENO;
	pPipe->childWrite = STDOUT_FILENO;
	pPipe->errWrite = STDERR_FILENO;
	pPipe->flags |= PIPE_CREATED;
	return 1;
}


