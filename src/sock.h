
/*
 * Cross-platform Network Socket Routines
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */


#ifndef METRIC_SOCK_H
#define METRIC_SOCK_H

#include "metric/common.h"

#ifdef WIN32
  #include <windows.h>
#endif

#ifdef HAVE_METRIC_SDLNET
  #include "SDL/SDL_net.h"
#else
  #include "sys/select.h"
#endif


#define MAX_HOST_NAME 128

 
#ifdef HAVE_METRIC_SDLNET
typedef TCPsocket socket_t;
typedef IPaddress socketAddr_t;
typedef SDLNet_SocketSet socketSet_t;
#else
typedef unsigned int socket_t;
typedef struct {
	unsigned long ip;
	unsigned short port;
} socketAddr_t;
typedef struct {
	unsigned int largestFD;
	fd_set set;
} socketSet_t;
#endif

enum {
	SOCK_OPT_BLOCKING, //! whether the socket will block on read/write
	SOCK_OPT_READY_BYTES, //! minimum bytes waiting on socket to be 'ready'
};


#ifdef __cplusplus
extern "C" {
#endif


int socketSystemInit();
int socketSystemFree();

int socketInit(socket_t *pSock);
int socketCopy(socket_t *pDst, socket_t *pSrc);
int socketConnect(socket_t *pSock, socketAddr_t *pAddr);
int socketListen(socket_t *pSock, socketAddr_t *pAddr);
int socketOptionGet(socket_t *pSock, int opt, int *value);
int socketOptionSet(socket_t *pSock, int opt, int value);
int socketClose(socket_t *pSock);
int socketValid(socket_t *pSock);
int socketRead(socket_t *pSock, void *pData, size_t nBytes);
int socketWrite(socket_t *pSock, void *pData, size_t nBytes);
int socketAccept(socket_t *pSock, socket_t *pNewSock, socketAddr_t *pAddr);

int socketSetInit(socketSet_t *pSockSet);
int socketSetAlloc(socketSet_t *pSockSet, unsigned int maxSockets);
int socketSetFree(socketSet_t *pSockSet);
int socketSetAdd(socketSet_t *pSockSet, socket_t *pSock);
int socketSetDel(socketSet_t *pSockSet, socket_t *pSock);
/*!
 * socketSetCheck()
 *
 * returns:
 * 	< 0: error
 * 	0: no sockets ready (timeout expired)
 * 	> 0: at least one socket is ready
 */
int socketSetCheck(socketSet_t *pSockSet, long msTimeout, socketSet_t *pDst);
int socketSetReady(socketSet_t *pReadySockSet, socket_t *pSock);

int socketAddrInit(socketAddr_t *pSockAddr);
int socketAddrCopy(socketAddr_t *pDst, socketAddr_t *pSrc);
int socketAddrNameSet(socketAddr_t *pSockAddr, char *hostname, short port);
//! hostname HAS TO be MAX_HOST_NAME bytes in length before calling.
int socketAddrNameGet(socketAddr_t *pSockAddr, char *hostname, short *port);

#ifdef __cplusplus
}
#endif

#endif

