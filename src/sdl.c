
/*
 * A Common SDL-specific Library
 *
 * Copyright (c) 2001  James D. Taylor  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * JAMES DOUGLAS TAYLOR BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#include <metric/sdl.h>

int SDLEventsEqual(const SDL_Event *e1, const SDL_Event *e2)
{
	if(e1->type != e2->type)
		return 0;
	switch(e1->type)
	{
	case SDL_KEYDOWN:
	case SDL_KEYUP:
		if(e1->key.keysym.sym == e2->key.keysym.sym)
			return 1;
		break;
	case SDL_MOUSEMOTION:
		if((e1->motion.xrel * e2->motion.xrel) > 0)
			if((e1->motion.yrel * e2->motion.yrel) > 0)
				return 1;
		break;
	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
		if(e1->button.button == e2->button.button)
			return 1;
		break;
	case SDL_JOYAXISMOTION:
	case SDL_JOYBALLMOTION:
	case SDL_JOYHATMOTION:
		break;
	case SDL_JOYBUTTONDOWN:
	case SDL_JOYBUTTONUP:
		break;
	case SDL_USEREVENT:
		break;
	}
	return 0;
}

int SDLEventCopy(SDL_Event *dst, const SDL_Event *src)
{
	dst->type = src->type;
	switch(src->type)
	{
	case SDL_KEYDOWN:
	case SDL_KEYUP:
		dst->key.keysym.scancode = src->key.keysym.scancode;
		dst->key.keysym.sym = src->key.keysym.sym;
		dst->key.keysym.mod = src->key.keysym.mod;
		dst->key.keysym.unicode = src->key.keysym.mod;
		break;
	case SDL_MOUSEMOTION:
		dst->motion.state = src->motion.state;
		dst->motion.x = src->motion.x;
		dst->motion.y = src->motion.y;
		dst->motion.xrel = src->motion.xrel;
		dst->motion.yrel = src->motion.yrel;
		break;
	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
		dst->button.button = src->button.button;
		dst->button.state = src->button.state;
		dst->button.x = src->button.x;
		dst->button.y = src->button.y;
		break;
	case SDL_JOYAXISMOTION:
	case SDL_JOYBALLMOTION:
	case SDL_JOYHATMOTION:
		break;
	case SDL_JOYBUTTONDOWN:
	case SDL_JOYBUTTONUP:
		break;
	case SDL_USEREVENT:
		break;
	}
	return 1;
}

