
/*
 * Library for handling unicode character encodings
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#include <metric/common.h>
#include <metric/char.h>

typedef int (*uni_verify)(void *stream, mUint32 nElem);
typedef int (*uni_encoder)(void *pDst, mUint32 *pSrc, mUint32 nDstElem);
typedef int (*uni_decoder)(mUint32 *pDst, void *pSrc, mUint32 nDstElem);
typedef int (*uni_byteSwap)(void *stream, mUint32 nElem);
typedef int (*uni_compare)(void *stream1, void *stream2, mUint32 nElem);
typedef int (*uni_copy)(void *pDst, const void *pSrc, mUint32 nDstElem);
typedef unsigned int (*uni_length)(void *stream);

static int ucs4_verify(void *stream, mUint32 nElem);
static int ucs4_byteSwap(void *stream, mUint32 nElem);
static int ucs4_encode(void *pDst, mUint32 *pSrc, mUint32 nDstElem);
static int ucs4_decode(mUint32 *pDst, void *pSrc, mUint32 nDstElem);
static int ucs4_compare(void *stream1, void *stream2, mUint32 nElem);
static int ucs4_copy(void *pDst, const void *pSrc, mUint32 nDstElem);
static unsigned int ucs4_length(void *stream);
static int utf8_verify(void *stream, mUint32 nElem);
static int utf8_byteSwap(void *stream, mUint32 nElem);
static int utf8_encode(void *pDst, mUint32 *pSrc, mUint32 nDstElem);
static int utf8_decode(mUint32 *pDst, void *pSrc, mUint32 nDstElem);
static int utf8_compare(void *stream1, void *stream2, mUint32 nElem);
static int utf8_copy(void *pDst, const void *pSrc, mUint32 nDstElem);
static unsigned int utf8_length(void *stream);
static int utf16_verify(void *stream, mUint32 nElem);
static int utf16_byteSwap(void *stream, mUint32 nElem);
static int utf16_encode(void *pDst, mUint32 *pSrc, mUint32 nDstElem);
static int utf16_decode(mUint32 *pDst, void *pSrc, mUint32 nDstElem);
static int utf16_compare(void *stream1, void *stream2, mUint32 nElem);
static int utf16_copy(void *pDst, const void *pSrc, mUint32 nDstElem);
static unsigned int utf16_length(void *stream);

static uni_verify g_verify[MAX_UNICODE] =
{ ucs4_verify, utf8_verify, utf16_verify };
static uni_byteSwap g_byteSwap[MAX_UNICODE] =
{ ucs4_byteSwap, utf8_byteSwap, utf16_byteSwap };
static uni_encoder g_encoder[MAX_UNICODE] =
{ ucs4_encode, utf8_encode, utf16_encode };
static uni_decoder g_decoder[MAX_UNICODE] =
{ ucs4_decode, utf8_decode, utf16_decode };
static uni_compare g_compare[MAX_UNICODE] =
{ ucs4_compare, utf8_compare, utf16_compare };
static uni_copy g_copy[MAX_UNICODE] =
{ ucs4_copy, utf8_copy, utf16_copy };
static uni_length g_length[MAX_UNICODE] =
{ ucs4_length, utf8_length, utf16_length };

int unicode_verify(void *stream, mUint32 type, mUint32 nElem)
{
	if(type >= MAX_UNICODE) {
		VLOG_MSG(LOG_LOW,"unicode_verify - invalid encoding type: %d", type);
		return -1;
	}
	return g_verify[type](stream, nElem);
}

int unicode_byteSwap(void *stream, mUint32 type, mUint32 nElem)
{
	if(type >= MAX_UNICODE) {
		VLOG_MSG(LOG_LOW,"unicode_verify - invalid encoding type: %d", type);
		return -1;
	}
	return g_byteSwap[type](stream, nElem);
}

int unicode_encode(void *dst, mUint32 *src, mUint32 dstType, mUint32 nDstElem)
{
	if(dstType >= MAX_UNICODE) {
		VLOG_MSG(LOG_LOW,"unicode_encode - invalid encoding type: %d", dstType);
		return -1;
	}
	return g_encoder[dstType](dst, src, nDstElem);
}

int unicode_decode(mUint32 *dst, void *src, mUint32 srcType, mUint32 nDstElem)
{
	if(srcType >= MAX_UNICODE) {
		VLOG_MSG(LOG_LOW,"unicode_decode - invalid encoding type: %d", srcType);
		return -1;
	}
	return g_decoder[srcType](dst, src, nDstElem);
}

int unicode_cmp(void *stream1, void *stream2, mUint32 type, mUint32 nElem)
{
	if(type >= MAX_UNICODE) {
		VLOG_MSG(LOG_LOW,"unicode_cmp - invalid encoding type: %d", type);
		return -1;
	}
	return g_compare[type](stream1, stream2, nElem);
}

int unicode_cpy(void *pDst, const void *pSrc, mUint32 type, mUint32 nDstElem)
{
	if(type >= MAX_UNICODE) {
		VLOG_MSG(LOG_LOW,"unicode_cpy - invalid encoding type: %d", type);
		return -1;
	}
	return g_copy[type](pDst, pSrc, nDstElem);
}

unsigned int unicode_len(void *stream, mUint32 type)
{
	if(type >= MAX_UNICODE) {
		VLOG_MSG(LOG_LOW,"unicode_len - invalid encoding type: %d", type);
		return 0;
	}
	return g_length[type](stream);
}

int unicode_cat(mUint32 *pDst, mUint32 *pSrc, mUint32 nDstElem)
{
	mUint32 i, j;

	i = 0;
	while(i < nDstElem && pDst[i])
		i++;
	if(i >= nDstElem) {
		pDst[nDstElem-1] = 0;
		return 0;
	}
	j = 0;
	while(i < nDstElem && pSrc[j])
		pDst[i++] = pSrc[j++];
	if(i >= nDstElem) {
		pDst[nDstElem-1] = 0;
		return 0;
	}
	pDst[i] = 0;
	return 1;
}

int unicode_tok(mUint32 *pDst, mUint32 **pSrc, mUint32 token, mUint32 nDstElem)
{
	mUint32 c;
	unsigned int i, j;

	pDst[0] = 0;
	if(*pSrc == NULL)
		return 0;
	if(!(**pSrc)) {
		*pSrc = NULL;
		return 0; // no string returned
	}
	//
	// skip any tokens from the beginning of "pSrc"
	i = 0;
	while((*pSrc)[i]  &&  (*pSrc)[i] == token)
		i++;
	if(!(*pSrc)[i]) {
		// "pSrc" is all token characters
		*pSrc = NULL;
		return 0; // no string returned
	}
	//
	// search for first token in "pSrc"
	j = 0;
	c = (*pSrc)[i++];
	while(c  &&  j < nDstElem  &&  c != token) {
		pDst[j++] = c;
		c = (*pSrc)[i++];
	}
	if(j < nDstElem)
		pDst[j] = 0;
	else
		pDst[nDstElem-1] = 0;
	if(!c) {
		*pSrc = NULL;
		return -1; // last sub-string
	}
	// search ran into a token before end of "pSrc" 
	// OR dst buffer has run out.  either way, we need to
	// update the pSrc pointer to skip what we have already copied.
	*pSrc = (*pSrc) + i;
	if(!(**pSrc)) {
		*pSrc = NULL;
		return -1; // last sub-string
	}
	return 1;
}

int ucs4_verify(void *stream, mUint32 nElem)
{
	mUint32 i = 0;
	mUint32 *p32 = (mUint32*)stream;

	while(i < nElem && p32[i]) {
		if(p32[i] >= 0x110000)
			return 0;
		if(!(p32[i] & 0xFFFF0000)) {
			if(p32[i] >= 0xD800 && p32[i] <= 0xDFFF)
				return 0;
			if(p32[i] >= 0xFFFE && p32[i] <= 0xFFFF)
				return 0;
		}
		i++;
	}
	p32[nElem-1] = 0;
	return 1;
}

int ucs4_byteSwap(void *stream, mUint32 nElem)
{
	mUint32 i;
	mUint32 *p32 = (mUint32*)stream;

	i = 0;
	while(i < nElem && p32[i]) {
		p32[i] = BYTESWAP32(p32[i]);
		i++;
	}
	return 1;
}

int ucs4_encode(void *pDst, mUint32 *pSrc, mUint32 nDstElem)
{
	return ucs4_copy(pDst, pSrc, nDstElem);
}

int ucs4_decode(mUint32 *pDst, void *pSrc, mUint32 nDstElem)
{
	return ucs4_copy(pDst, pSrc, nDstElem);
}

int ucs4_compare(void *stream1, void *stream2, mUint32 nElem)
{
	mUint32 i = 0;
	mUint32 *p1 = (mUint32*)stream1;
	mUint32 *p2 = (mUint32*)stream2;

	while(i < nElem && p1[i] && p2[i]) {
		if(p1[i] != p2[i])
			return p1[i] - p2[i];
		i++;
	}
	if(i >= nElem)
		return 0; // equal
	return p1[i] - p2[i];
}

int ucs4_copy(void *pDst, const void *pSrc, mUint32 nDstElem)
{
	unsigned int i;
	mUint32 *pSrc32 = (mUint32*)pSrc;
	mUint32 *pDst32 = (mUint32*)pDst;

	if(!nDstElem) {
		// cannot be zero because we always have to append a '\0' to the end
		LOG_MSG(LOG_LOW,"ucs4_copy - nDstElem cannot be zero");
		return -1;
	}
	i = 0;
	while(i < nDstElem-1 && pSrc32[i]) {
		pDst32[i] = pSrc32[i];
		i++;
	}
	pDst32[i] = 0;
	return 1;
}

unsigned int ucs4_length(void *stream)
{
	unsigned int i = 0;
	mUint32 *p32 = (mUint32*)stream;

	while(p32[i])
		i++;
	return i;
}

int utf8_verify(void *stream, mUint32 nElem)
{
	mUint32 i = 0; // which byte in stream we are currently on
	mUint32 seqLeft = 0;  // how many bytes are left in current char sequence
	mUint8 *p8 = (mUint8*)stream; // byte stream
	mUint8 byte;

	while(i < nElem && p8[i])
	{
		byte = p8[i];
		//
		// some sort of binary-like search for this byte's meaning
		//
		if(byte <= 0xBF)
		{
			if(byte <= 0x7F) {  // 0x01 - 0x7F (ASCII)
				//
				// valid single byte character 
				if(seqLeft) {
					// expecting a continue byte, damnit
					LOG_MSG(LOG_LOW,"utf8_verify - continuing byte not found");
					return 0; // expecting a 0x80-0xBF continuing byte
				}
			} else { // 0x80 - 0xBF
				//
				// continuing byte of multibyte char sequence
				if(!seqLeft) {
					LOG_MSG(LOG_LOW,"utf8_verify - continue byte not expected");
					return 0;
				}
				seqLeft--;
			}
		}
		else // 0xC0 - 0xFF
		{
			if(byte <= 0xFD) // 0xC0 - 0xFD
			{
				//
				// begining byte of multibyte char sequence
				if(seqLeft) {
					LOG_MSG(LOG_LOW,"utf8_verify - continuing byte not found");
					return 0; // expecting a 0x80-0xBF continuing byte
				}
				if(byte <= 0xEF) {
					if(byte <= 0xDF)
						seqLeft = 1; // 0xC0 - 0xDF  (2 byte char)
					else
						seqLeft = 2; // 0xE0 - 0xEF  (3 byte char)
				} else {
					if(byte <= 0xF7) // 0xF0 - 0xF7  (4 byte char)
						seqLeft = 3;
					else if(byte <= 0xFB)
						seqLeft = 4; // 0xF8 - 0xFB  (5 byte char)
					else
						seqLeft = 5; // 0xFC || 0xFD (6 byte char)
				}
			} else { // 0xFE - 0xFF:
				//
				// not used except for endian marking at begining
				if(seqLeft) {
					LOG_MSG(LOG_LOW,"utf8_verify - continuing byte not found");
					return 0; // expecting a 0x80-0xBF continuing byte
				}
				break;
			}
		}
		i++;
	}
	return 1;
}

int utf8_byteSwap(void *stream, mUint32 nElem)
{
	// no byteswap necessary.
	return 1;
}

int utf8_encode(void *pDst, mUint32 *pSrc, mUint32 nDstElem)
{
	mUint32 i8 = 0; // increment of UTF-8 destination stream
	mUint32 i32 = 0; // increment of UCS-4 source stream
	mUint8 *p8 = (mUint8*)pDst;
	mUint32 wchar;
	while(pSrc[i32])
	{
		//
		// search for different classes of encoding (1-byte, 4-byte, etc)
		//
		wchar = pSrc[i32];
		//
		// validate source before encoding
		if((wchar >= 0xD800 && wchar <= 0xDFFF) ||
				(wchar >= 0xFFFE && wchar <= 0xFFFF))
		{
			VLOG_MSG(LOG_LOW,"utf8_encode - invalid unicode char: %x", wchar);
			if(i8 >= nDstElem - 1)
				i8 = nDstElem -1;
			p8[i8] = 0;
			return -1;
		}
		if(wchar >= 0x110000) {
			VLOG_MSG(LOG_LOW,"utf8_encode - invalid unicode char: %x", wchar);
			if(i8 >= nDstElem - 1)
				i8 = nDstElem -1;
			p8[i8] = 0;
			return -1;
		}
		//
		// encode
		if(wchar <= 0x7FF)
		{
			if(wchar <= 0x7F) { // 0x00 - 0x7F
				//
				// single byte character
				if(i8 >= nDstElem - 1)
					break;
				p8[i8++] = (mUint8)wchar;
			} else {
				//
				// two byte character
				if(i8 >= nDstElem - 2)
					break;
				p8[i8++] = wchar / 0x40 + 0xC0;
				p8[i8++] = wchar % 0x40 + 0x80;
			}
		}
		else
		{
			//
			// greater than a 2 byte character
			//
			if(wchar <= 0x1FFFFF) {
				if(wchar <= 0xFFFF) {
					// 
					// three byte character
					if(i8 >= nDstElem - 3)
						break;
					p8[i8++] = wchar / 0x1000 + 0xE0;
					p8[i8++] = (wchar / 0x40) % 0x40 + 0x80;
					p8[i8++] = wchar % 0x40 + 0x80;
				} else {
					// 
					// four byte character
					if(i8 >= nDstElem - 4)
						break;
					p8[i8++] = wchar / 0x40000 + 0xF0;
					p8[i8++] = (wchar / 0x1000) % 0x40 + 0x80;
					p8[i8++] = (wchar / 0x40) % 0x40 + 0x80;
					p8[i8++] = wchar % 0x40 + 0x80;
				}
			} else {
				if(wchar <= 0x3FFFFFF) {
					// 
					// five byte character
					if(i8 >= nDstElem - 5)
						break;
					p8[i8++] = wchar / 0x1000000 + 0xF8;
					p8[i8++] = (wchar / 0x40000) % 0x40 + 0x80;
					p8[i8++] = (wchar / 0x1000) % 0x40 + 0x80;
					p8[i8++] = (wchar / 0x40) % 0x40 + 0x80;
					p8[i8++] = wchar % 0x40 + 0x80;
				} else {
					// 
					// six byte character
					if(i8 >= nDstElem - 6)
						break;
					p8[i8++] = wchar / 0x40000000 + 0xFC;
					p8[i8++] = (wchar / 0x1000000) % 0x40 + 0x80;
					p8[i8++] = (wchar / 0x40000) % 0x40 + 0x80;
					p8[i8++] = (wchar / 0x1000) % 0x40 + 0x80;
					p8[i8++] = (wchar / 0x40) % 0x40 + 0x80;
					p8[i8++] = wchar % 0x40 + 0x80;
				}
			}
		}
		i32++;
	}
	if(i8 >= nDstElem - 1)
		i8 = nDstElem -1;
	p8[i8] = 0;
	return 1;
}

int utf8_decode(mUint32 *pDst, void *pSrc, mUint32 nDstElem)
{
	mUint32 i8 = 0; // increment of UTF-8 source stream
	mUint32 i32 = 0; // increment of UCS-4 destination stream
	mUint8 *p8 = (mUint8*)pSrc;
	while(i32 < nDstElem-1 && p8[i8])
	{
		//
		// search for code class
		//
		if(p8[i8] <= 0x7F) { // 0x00 - 0x7F
			pDst[i32] = p8[i8++];
		} else if(p8[i8] <= 0xDF) { // 0xC0 - 0xDF
			pDst[i32] =  (p8[i8++] - 0xC0) * 0x40;
			if(!p8[i8])
				break;
			pDst[i32] += (p8[i8++] - 0x80);
		} else if(p8[i8] <= 0xEF) { // 0xE0 - 0xEF
			pDst[i32] =  (p8[i8++] - 0xE0) * 0x1000;
			if(!p8[i8])
				break;
			pDst[i32] += (p8[i8++] - 0x80) * 0x40;
			if(!p8[i8])
				break;
			pDst[i32] += (p8[i8++] - 0x80);
		} else if(p8[i8] <= 0xF7) { // 0xF0 - 0xF7
			pDst[i32] =  (p8[i8++] - 0xF0) * 0x40000;
			if(!p8[i8])
				break;
			pDst[i32] += (p8[i8++] - 0x80) * 0x1000;
			if(!p8[i8])
				break;
			pDst[i32] += (p8[i8++] - 0x80) * 0x40;
			if(!p8[i8])
				break;
			pDst[i32] += (p8[i8++] - 0x80);
		} else if(p8[i8] <= 0xFB) { // 0xF8 - 0xFB
			pDst[i32] =  (p8[i8++] - 0xF8) * 0x1000000;
			if(!p8[i8])
				break;
			pDst[i32] += (p8[i8++] - 0x80) * 0x40000;
			if(!p8[i8])
				break;
			pDst[i32] += (p8[i8++] - 0x80) * 0x1000;
			if(!p8[i8])
				break;
			pDst[i32] += (p8[i8++] - 0x80) * 0x40;
			if(!p8[i8])
				break;
			pDst[i32] += (p8[i8++] - 0x80);
		} else if(p8[i8] <= 0xFD) { // 0xFC - 0xFD
			pDst[i32] =  (p8[i8++] - 0xFC) * 0x40000000;
			if(!p8[i8])
				break;
			pDst[i32] += (p8[i8++] - 0x80) * 0x1000000;
			if(!p8[i8])
				break;
			pDst[i32] += (p8[i8++] - 0x80) * 0x40000;
			if(!p8[i8])
				break;
			pDst[i32] += (p8[i8++] - 0x80) * 0x1000;
			if(!p8[i8])
				break;
			pDst[i32] += (p8[i8++] - 0xC0) * 0x40;
			if(!p8[i8])
				break;
			pDst[i32] += (p8[i8++] - 0x80);
		} else {
			VLOG_MSG(LOG_LOW,"utf8_decode - invalid char code: %x", p8[i8]);
			break;
		}
		i32++;
	}
	pDst[i32] = 0;
	return 1;
}

int utf8_compare(void *stream1, void *stream2, mUint32 nElem)
{
	mUint32 i = 0;
	mUint8 *p1 = (mUint8*)stream1;
	mUint8 *p2 = (mUint8*)stream2;

	while(i < nElem && p1[i] && p2[i]) {
		if(p1[i] != p2[i])
			return p1[i] - p2[i];
		i++;
	}
	if(i >= nElem)
		return 0; // equal
	return p1[i] - p2[i];
}

int utf8_copy(void *pDst, const void *pSrc, mUint32 nDstElem)
{
	mUint32 i = 0;
	mUint8 *pDst8 = (mUint8*)pDst;
	mUint8 *pSrc8 = (mUint8*)pSrc;

	while(i < nDstElem-1 && pSrc8[i]) {
		pDst8[i] = pSrc8[i];
		i++;
	}
	pDst8[i] = 0;
	return 1;
}

unsigned int utf8_length(void *stream)
{
	mUint32 i8 = 0;
	mUint32 nChar = 0;
	mUint8 *p8 = (mUint8*)stream;

	while(p8[i8])
	{
		//
		// search for code class
		//
		if(p8[i8] <= 0x7F) { // 0x00 - 0x7F
			i8++;
		} else if(p8[i8] <= 0xDF) { // 0xC0 - 0xDF
			i8++;
			if(!p8[i8++])
				break;
		} else if(p8[i8] <= 0xEF) { // 0xE0 - 0xEF
			i8++;
			if(!p8[i8++])
				break;
			if(!p8[i8++])
				break;
		} else if(p8[i8] <= 0xF7) { // 0xF0 - 0xF7
			i8++;
			if(!p8[i8++])
				break;
			if(!p8[i8++])
				break;
			if(!p8[i8++])
				break;
		} else if(p8[i8] <= 0xFB) { // 0xF8 - 0xFB
			i8++;
			if(!p8[i8++])
				break;
			if(!p8[i8++])
				break;
			if(!p8[i8++])
				break;
			if(!p8[i8++])
				break;
		} else if(p8[i8] <= 0xFD) { // 0xFC - 0xFD
			i8++;
			if(!p8[i8++])
				break;
			if(!p8[i8++])
				break;
			if(!p8[i8++])
				break;
			if(!p8[i8++])
				break;
			if(!p8[i8++])
				break;
		} else {
			VLOG_MSG(LOG_LOW,"utf8_length - invalid char code: %d", p8[i8]);
			break;
		}
		nChar++;
	}
	return nChar;
}

int utf16_verify(void *stream, mUint32 nElem)
{
	mUint32 i;
	mUint16 *p16 = (mUint16*)stream;

	i = 0;
	while(i < nElem && p16[i])
	{
		if(p16[i] >= 0xD800 && p16[i] <= 0xDBFF) {
			//
			// 4 byte character
			//
			if(i >= nElem-1)
				return 0; // no room left for 0x0
			i++;
			if(p16[i] < 0xDC00 || p16[i] > 0xDFFF)
				return 0;
			i++;
			continue;
		}
		if(p16[i] >= 0xDC00 && p16[i] <= 0xDFFF) 
			return 0;  // out of sequence 4 byte marker
		i++;
	}
	return 1;
}

//
// TODO: a complete unicode lib would append a 0xFEFF to each stream
// so that the receiving end will know when to swap bytes.
//
int utf16_byteSwap(void *stream, mUint32 nElem)
{
	mUint32 i;
	mUint16 *p16 = (mUint16*)stream;

	i = 0;
	while(i < nElem && p16[i]) {
		p16[i] = BYTESWAP16(p16[i]);
		i++;
	}
	return 1;
}

int utf16_encode(void *pDst, mUint32 *pSrc, mUint32 nDstElem)
{
	mUint16 *p16 = (mUint16*)pDst;
	mUint32 temp;
	unsigned int iDst = 0;
	unsigned int iSrc = 0; 

	while(pSrc[iSrc])
	{
		//
		// from ISO-10646-UTF-16
		//
		//  NOTE 1 - Code positions from 0000 D800 to 0000 DFFF are
		//  reserved for the UTF-16 form and do not occur in UCS-4.
		//  The values 0000 FFFE and 0000 FFFF also do not occur (see
		//  clause 8). The mapping of these code positions in UTF-16
		//  is undefined.
		//
		if(pSrc[iSrc] & 0xFFFF0000)
		{
			//
			// character requires two 16 bit encodings in utf16
			if(iDst >= nDstElem-2)
				break; // not enough room for us and a 0x0
			if(pSrc[iSrc] >= 0x110000) {
				VLOG_MSG(LOG_LOW,"utf16_encode - invalid char: %d", pSrc[iSrc]);
				return -1;
			}
			temp = pSrc[iSrc] - 0x10000;
			p16[iDst++] = ((temp / 0x400) + 0xd800);
			p16[iDst++] = ((temp % 0x400) + 0xdc00);
		}
		else
		{
			//
			// character can map completely with lower 16 bits of ucs4
			if(iDst >= nDstElem-1)
				break;
			if((pSrc[iSrc] >= 0xD800 && pSrc[iSrc] <= 0xDFFF) ||
					(pSrc[iSrc] >= 0xFFFE && pSrc[iSrc] <= 0xFFFF))
			{
				VLOG_MSG(LOG_LOW,"utf16_encode - invalid char: %d", pSrc[iSrc]);
				return -1;
			}
			p16[iDst++] = (mUint16)pSrc[iSrc];
		}
		iSrc++;
	}
	if(iDst >= nDstElem-1)
		iDst = nDstElem - 1;
	p16[iDst] = 0;
	return 1;
}

int utf16_decode(mUint32 *pDst, void *pSrc, mUint32 nDstElem)
{
	mUint16 *p16 = (mUint16*)pSrc;
	unsigned int iDst = 0;
	unsigned int iSrc = 0;

	while(iDst < nDstElem-1 && p16[iSrc])
	{
		if(p16[iSrc] >= 0xD800 && p16[iSrc] <= 0xDBFF)
		{
			pDst[iDst] = ((p16[iSrc] - 0xD800) * 0x400);
			iSrc++;
			if(p16[iSrc] < 0xDC00 || p16[iSrc] > 0xDFFF) {
				VLOG_MSG(LOG_LOW,"utf16_decode - invalid code: %d", p16[iSrc]);
				return -1;
			}
			pDst[iDst] += (p16[iSrc] - 0xDC00) + 0x10000;
			iSrc++;
		} else {
			pDst[iDst] = (mUint32)p16[iSrc];
			iSrc++;
		}
		iDst++;
	}
	pDst[iDst] = 0;
	return 1;
}


int utf16_compare(void *stream1, void *stream2, mUint32 nElem)
{
	mUint32 i = 0;
	mUint16 *p1 = (mUint16*)stream1;
	mUint16 *p2 = (mUint16*)stream2;

	while(i < nElem && p1[i] && p2[i]) {
		if(p1[i] != p2[i])
			return p1[i] - p2[i];
		i++;
	}
	if(i >= nElem)
		return 0; // equal
	return p1[i] - p2[i];
}

int utf16_copy(void *pDst, const void *pSrc, mUint32 nDstElem)
{
	mUint32 i = 0;
	mUint16 *pSrc16 = (mUint16*)pSrc;
	mUint16 *pDst16 = (mUint16*)pDst;

	while(i < nDstElem-1 && pSrc16[i]) {
		pDst16[i] = pSrc16[i];
		i++;
	}
	pDst16[i] = 0;
	return 1;
}

unsigned int utf16_length(void *stream)
{
	mUint32 i = 0;
	mUint32 nChar = 0;
	mUint16 *p16 = (mUint16*)stream;

	while(p16[i])
	{
		if(p16[i] >= 0xD800 && p16[i] <= 0xDBFF) {
			//
			// 2-byte char
			i++;
			if(!p16[i] || p16[i] < 0xDC00 || p16[i] > 0xDFFF)
				break; // no continuation char
		}
		i++;
		nChar++;
	}
	return nChar;
}


