
/*
 * Height Map Library
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#ifndef METRIC_HEIGHT_MAP_H
#define METRIC_HEIGHT_MAP_H


#include <metric/geom.h>

enum {
	NOISE_FLAT=0,  /* height map is a plane */
	NOISE_PERLIN, /* perlin noise generator */
	NOISE_HETERO, /* heterogeneous */
	NOISE_MULTIFRAC,  /* multifractal */
	NOISE_RIDGED,  /* ridged multifractal */
	NUM_NOISE_TYPE
};

/*!
 * herein lies the structure for defining how to
 * generate a height map.
 */
typedef struct
{
	mUint32 dx; /* number of samples in the X axis */
	mUint32 dy; /* number of samples in the Y axis */
	/*
	 * ix and iy do not effect the generation of the heightMap.
	 * They are, however, needed for converting physical locations
	 * into a x/y heightMap position.
	 */
	mFloat32 ix; /* (increment) - physical length between samples (dx) */
	mFloat32 iy; /* (increment) - physical length between samples (dy) */
	/*!
	 * 2x2 transformation matrix which will
	 * be applied to (x,y) b4 input to noise  */
	mat2f mPreTransform; 
	/*
	 * translate point after transformation */
	vect2f vPreTranslate;
	/*
	 * scales the resulting height after returning from noise */
	mFloat32 postScaleZ;
	/*
	 * translates resutling height */
	mFloat32 postTranslateZ;
	/*!
	 * procedure to use to generate heightMap.  Assign
	 * one of the enumerations above */
	mUint32 noiseType;
	/*
	 * rest are variables to modify how the fractal noise works
	 */
	mUint32 prime1; /* prime numbers */
	mUint32 prime2;
	mFloat32 fracDimen; /* fractal dimension of roughest area .25 */
	mFloat32 lacunarity; /* gap between successive frequencies */
	mFloat32 octaves; /* number of frequencies in the fBm */
	mFloat32 offset; /* raises the heightMap from "sea level" .7 */
	mFloat32 gain;  /* ridged weight */
} heightMap_t;

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * always Init a heightMap_t b4 using 
 */
int heightMapInit(heightMap_t *pMap);
float *heightMapAlloc(heightMap_t *pMap);
int heightMapFree(heightMap_t *pMap, float *pData);
int heightMapVerify(heightMap_t *pMap);
int heightMapByteSwap(heightMap_t *pMap);

//! Generate heightMap data from heightMap info structure.
/*!
 * create the heightMap pDst according to the info in
 * pMap;  allocate enough mem for pDst b4 calling 
 * \param pMap heightMap info used for filling
 * \param pDst destination heightMap data
 */
int heightMapFill(heightMap_t *pMap, float *pDst);

//! copy heightMap info structure
/*!
 * bit for bit 
 * \param pDst destination heightMap info struct
 * \param pSrc source heightMap info struct
 */
int heightMapCopy(heightMap_t *pDst, heightMap_t *pSrc);

//! Compute the derivative of a heightMap.
/*!
 * Put the derivative of pSrc into pDst.
 * Allocate memory for pDst before calling. 
 * \param pMap heightMap info structure (for extents).
 * \param pDst destination heightMap data (derived data)
 * \param pSrc source heightMap data
 */
int heightMapDerive(heightMap_t *pMap, float *pDst, float *pSrc);

//! Create a triPatch geometry object from a heightMap.
/*!
 * Object data is put into 'pObj' and object parameters
 * are put into 'pTriPatch'.
 * \param pObj destination geometry object to put the tripatch
 * \param pTriPatch destination triPatch info structure
 * \param pSrc source heightMap data array
 * \param pHeightMap source heightMap info structure
 * \return TRUE on success
 */
int heightMap2triPatch(geomObj3ve *pObj, geoTriPatch *pTriPatch,
		       float *pSrc, heightMap_t *pHeightMap);

//! Get the height of the physical x/y location
/*!
 * \param pMap source heightMap info structure
 * \param pData source heightMap data array
 * \param x location in heightMap
 * \param y location in heightMap
 * \return The height of the physical x/y location
 */
float heightMapGetHeight(heightMap_t *pMap, float *pData, float x, float y);

#ifdef __cplusplus
}
#endif

#endif

