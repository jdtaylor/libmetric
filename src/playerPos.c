
/*
 * Library for handling player positioning.
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#include "playerPos.h"

#define MIN_PLAYER_POS_BUF 5
#define INCR_PLAYER_POS_BUF 10
#define MAX_PLAYER_POS_QUEUE 100

/*!
 * queue of player positions implemented with
 * a circular array of cargoPlayerPos'.
 * This is used as a buffer and to interpolate positions 
 */
typedef struct {
	int capacity;
	int front;
	int rear;
	int size;
	playerPos_t *posArray;
} playerPosQueue;

static playerPosQueue *g_pQueue[MAX_PLAYER_POS_QUEUE];
static int g_bInitialized = FALSE;

int playerPosInit(playerPos_t *pPos)
{
	VECTINIT3F(pPos->vLocation, 0.0f);
	VECTINIT3F(pPos->vMovingDir, 0.0f);
	/* make the player face positive Y axis */
	pPos->vFacingDir[0] = 0.0f;
	pPos->vFacingDir[1] = 1.0f;
	pPos->vFacingDir[2] = 0.0f;
	pPos->time = 0;
	return 1;
}

int playerPosCopy(playerPos_t *dst, playerPos_t *src)
{
	VECTCOPY3F(dst->vLocation, src->vLocation);
	VECTCOPY3F(dst->vFacingDir, src->vFacingDir);
	VECTCOPY3F(dst->vMovingDir, src->vMovingDir);
	dst->time = src->time;
	return 1;
}

int playerPosByteSwap(playerPos_t *pPos)
{
	BYTESWAP32F(pPos->vLocation[0]);
	BYTESWAP32F(pPos->vLocation[1]);
	BYTESWAP32F(pPos->vLocation[2]);
	BYTESWAP32F(pPos->vMovingDir[0]);
	BYTESWAP32F(pPos->vMovingDir[1]);
	BYTESWAP32F(pPos->vMovingDir[2]);
	BYTESWAP32F(pPos->vFacingDir[0]);
	BYTESWAP32F(pPos->vFacingDir[1]);
	BYTESWAP32F(pPos->vFacingDir[2]);
	pPos->time = BYTESWAP32(pPos->time); //TODO! 64 bit isn't it?
	return 1;
}

int playerPosQueueCreate()
{
	int i;
	if(!g_bInitialized) {
		for(i = 0; i < MAX_PLAYER_POS_QUEUE; i++)
			g_pQueue[i] = NULL;
		g_bInitialized = TRUE;
	}
	i = 0;
	while(i < MAX_PLAYER_POS_QUEUE && g_pQueue[i] != NULL)
		i++;
	if(i >= MAX_PLAYER_POS_QUEUE) {
		VLOG_MSG(LOG_LOW,"playerPosQueueCreate - queue overflow at: %d", i);
		return -1;
	}
	g_pQueue[i] = (playerPosQueue*)MALLOC(sizeof(playerPosQueue));
	if(g_pQueue[i] == NULL) {
		LOG_MSG(LOG_SYS,"playerPosQueueCreate - MALLOC failed");
		return -1;
	}
	g_pQueue[i]->posArray = 
		(playerPos_t*)MALLOC(sizeof(playerPos_t) * MIN_PLAYER_POS_BUF);
	if(g_pQueue[i]->posArray == NULL) {
		LOG_MSG(LOG_SYS,"playerPosQueueCreate - MALLOC failed");
		FREE(g_pQueue[i]);
		g_pQueue[i] = NULL;
		return -1;
	}
	g_pQueue[i]->capacity = MIN_PLAYER_POS_BUF;
	g_pQueue[i]->front = 0;
	g_pQueue[i]->rear = 0;
	g_pQueue[i]->size = 0; 
	return i;
}

int playerPosQueueDelete(int queueId)
{
	if(queueId < 0 || queueId >= MAX_PLAYER_POS_QUEUE) {
		VLOG_MSG(LOG_LOW,"playerPosQueueDelete - invalid id: %d", queueId);
		return -1;
	}
	if(g_pQueue[queueId] == NULL) {
		VLOG_MSG(LOG_LOW,"playerPosQueueDelete - queue %d is NULL", queueId);
		return -1;
	}
	FREE(g_pQueue[queueId]->posArray);
	FREE(g_pQueue[queueId]);
	g_pQueue[queueId] = NULL;
	return 1;
}

int playerPosEnqueue(int queueId, playerPos_t *pPos)
{
	int i, oldCapacity;
	playerPos_t *pTempPos;
	playerPosQueue *pQueue;
	if(queueId < 0 || queueId >= MAX_PLAYER_POS_QUEUE) {
		VLOG_MSG(LOG_LOW,"playerPosEnqueue - invalid id: %d", queueId);
		return -1;
	}
	pQueue = g_pQueue[queueId];
	if(pQueue == NULL) {
		VLOG_MSG(LOG_LOW,"playerPosEnqueue - queue %d is NULL", queueId);
		return -1;
	}
	if(pQueue->size == pQueue->capacity-1) {
		pQueue->size--;
		pQueue->size++;
	}
	if(pQueue->size >= pQueue->capacity) {
		oldCapacity = pQueue->capacity;
		VLOG_MSG(LOG_USER,"Re-sizing playerPos queue to: %d", oldCapacity * 2);
		pTempPos = (playerPos_t*)REALLOC(pQueue->posArray, 
				oldCapacity*2 * sizeof(playerPos_t));
		if(pTempPos == NULL) {
			VLOG_MSG(LOG_LOW,"playerPosEnqueue - full queue: %d", oldCapacity);
			return -1;
		}
		pQueue->posArray = pTempPos;
		pQueue->capacity *= 2;
		i = 0;
		while(i < pQueue->front) {
			playerPosCopy(&pTempPos[oldCapacity+i], &pTempPos[i]);
			i++;
		}
		pQueue->rear = pQueue->front + pQueue->size;
	}
	//
	// save player pos info onto back of position queue
	playerPosCopy(&pQueue->posArray[pQueue->rear], pPos);
	pQueue->rear = (pQueue->rear + 1) % pQueue->capacity;
	pQueue->size++;
	return 1;
}

int playerPosDequeue(int queueId, playerPos_t *pPos)
{
	playerPosQueue *pQueue;
	int front;
	if(queueId < 0 || queueId >= MAX_PLAYER_POS_QUEUE) {
		VLOG_MSG(LOG_LOW,"playerPosDequeue - invalid id: %d", queueId);
		return -1;
	}
	pQueue = g_pQueue[queueId];
	if(pQueue == NULL) {
		VLOG_MSG(LOG_LOW,"playerPosDequeue - queue %d is NULL", queueId);
		return -1;
	}
	front = pQueue->front;
	playerPosCopy(pPos, &pQueue->posArray[front]);
	pQueue->front = (front + 1) % pQueue->capacity;
	pQueue->size--;
	return 1;
}

int playerPosGetCurrent(int queueId, playerPos_t *pDst, mUint64 curTime)
{
	playerPosQueue *pQueue;
	playerPos_t posBuf;
	int front, next;
	float fT;
	if(queueId < 0 || queueId >= MAX_PLAYER_POS_QUEUE) {
		VLOG_MSG(LOG_LOW,"playerPosGetCurrent - invalid id: %d", queueId);
		return -1;
	}
	pQueue = g_pQueue[queueId];
	if(pQueue == NULL) {
		VLOG_MSG(LOG_LOW,"playerPosGetCurrent - queue %d is NULL", queueId);
		return -1;
	}
	if(pQueue->size <= 0) {
		VLOG_MSG(LOG_LOW,"playerPosGetCurrent - queue %d is empty", queueId);
		return -1;
	}
	front = pQueue->front;
	/*
	 * if we only have one position we don't need to interpolate */
	if(pQueue->size == 1) {
		/* no movement */
		playerPosCopy(pDst, &pQueue->posArray[front]);
		return 1;
	}
	next = (front + 1) % pQueue->capacity;
	/*
	 * dequeue any invalid or uneeded player positions.
	 * In the process, check if we have run out of positions */
	while(pQueue->posArray[next].time <= pQueue->posArray[front].time ||
			curTime >= pQueue->posArray[next].time)
	{
		playerPosDequeue(queueId, &posBuf);
		if(pQueue->size <= 0) {
			VLOG_MSG(LOG_LOW,"playerPosGetCurrent - queue %d empty", queueId);
			return -1;
		}
		front = pQueue->front;
		if(pQueue->size == 1) {
			/* no movement */
			playerPosCopy(pDst, &pQueue->posArray[front]);
			return 1;
		}
		next = (front + 1) % pQueue->capacity;
	}
	/*
	 * determine the current normalized time with respect to the
	 * next two player position times 
	 */
	fT = (float)(curTime - pQueue->posArray[front].time) / 
		(float)(pQueue->posArray[next].time - pQueue->posArray[front].time);
	/*
	 * do linear interpolation on vectors, return results in "pDst"
	 */
	LERP3FV(pDst->vLocation, pQueue->posArray[front].vLocation,
			pQueue->posArray[next].vLocation, fT);
	LERP3FV(pDst->vFacingDir, pQueue->posArray[front].vFacingDir,
			pQueue->posArray[next].vFacingDir, fT);
	pDst->time = curTime;
	return 1;
}


playerPos_t *playerPosPeekFront(int queueId)
{
	playerPosQueue *pQueue;
	if(queueId < 0 || queueId >= MAX_PLAYER_POS_QUEUE) {
		VLOG_MSG(LOG_LOW,"playerPosPeekFront - invalid id: %d", queueId);
		return NULL;
	}
	pQueue = g_pQueue[queueId];
	if(pQueue == NULL) {
		VLOG_MSG(LOG_LOW,"playerPosPeekFront - queue %d is NULL", queueId);
		return NULL;
	}
	if(pQueue->size <= 0) {
		VLOG_MSG(LOG_LOW,"playerPosPeekFront - queue %d is empty", queueId);
		return NULL;
	}
	return &(pQueue->posArray[pQueue->front]);
}

playerPos_t *playerPosPeekRear(int queueId)
{
	playerPosQueue *pQueue;
	int rear;
	if(queueId < 0 || queueId >= MAX_PLAYER_POS_QUEUE) {
		VLOG_MSG(LOG_LOW,"playerPosPeekRear - invalid id: %d", queueId);
		return NULL;
	}
	pQueue = g_pQueue[queueId];
	if(pQueue == NULL) {
		VLOG_MSG(LOG_LOW,"playerPosPeekRear - queue %d is NULL", queueId);
		return NULL;
	}
	if(pQueue->size <= 0) {
		VLOG_MSG(LOG_LOW,"playerPosPeekRear - queue %d is empty", queueId);
		return NULL;
	}
	rear = pQueue->rear - 1;
	if(rear < 0)
		rear = pQueue->capacity - 1;
	return &(pQueue->posArray[rear]);
}

int playerPosMove(playerPos_t *pPos, float speed)
{
	vect3f vIncr;
	vect3f vZAxis;
	vect3f vRight;
	vect3f vUp;
	if(pPos == NULL)
		return -1;
	// 
	// normalize the direction it is facing
	if(vectNorm3f(pPos->vFacingDir) <= 0)
		return -1;
	vZAxis[0] = 0.0f;
	vZAxis[1] = 0.0f;
	vZAxis[2] = -1.0f;
	VECTCROSS3F(vRight, pPos->vFacingDir, vZAxis);
	if(vectNorm3f(vRight) <= 0)
		return -1;
	VECTCROSS3F(vUp, pPos->vFacingDir, vRight);
	if(vectNorm3f(vUp) <= 0)
		return -1;
	//
	// the matrix [vRight, vForward, vUp] *should* be our transformation
	// matrix 
	vIncr[0] = VECTDOTP3F(pPos->vMovingDir, vRight);   /* X */
	vIncr[1] = VECTDOTP3F(pPos->vMovingDir, pPos->vFacingDir); /* Y */
	vIncr[2] = VECTDOTP3F(pPos->vMovingDir, vUp);      /* Z */
	//
	// normalize the increment vector (make it a unit vector)
	if(vectNorm3f(vIncr) <= 0)
		return -1;
	VECTMULT3F(vIncr, speed);  // scale by the players speed
	//
	// increment to new location
	pPos->vLocation[0] += vIncr[0];
	pPos->vLocation[1] += vIncr[1];
	pPos->vLocation[2] += vIncr[2];
	return 1;
}

int playerPosRotate(playerPos_t *pPos, vect3f vRot)
{
	vect3f vUp;
	vect3f vRight;
	vUp[0] = 0.0f;
	vUp[1] = 0.0f;
	vUp[2] = -1.0f;
	VECTCROSS3F(vRight, pPos->vFacingDir, vUp);
	vectRotate3f(pPos->vFacingDir, vRot[2], 0.0f, 0.0f, -1.0f);
	vectRotate3f(pPos->vFacingDir, vRot[0], vRight[0], vRight[1], vRight[2]);
	/*
	   dir = &(serverCache.cargos.playerPos[id]->pos.vFacingDir);
	   vectNorm3f(*dir);
	   vRad[0] = acos((*dir)[0]);
	   vRad[2] = acos((*dir)[2]);
	   vRad[0] += z;
	   vRad[2] += x;
	   if(vRad[2] < 0.0f)
	   vRad[2] += M_PI;
	   if(vRad[0] < 0.0f)
	   vRad[0] += M_PI;
	   if(vRad[2] >= M_PI)
	   vRad[2] -= M_PI;
	   if(vRad[0] >= M_PI)
	   vRad[0] -= M_PI;
	   (*dir)[0] = cos(vRad[0]);
	   (*dir)[1] = sin(vRad[0]);
	   (*dir)[2] = cos(vRad[2]);
	 */
	return 1;
}


