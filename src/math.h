
/*
 * A Common Math Library
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */


#ifndef METRIC_MATH_H
#define METRIC_MATH_H

#include <metric/type.h>


typedef mInt32		vect2i[2];
typedef mFloat32	vect2f[2];
typedef mInt32		vect3i[3];
typedef mFloat32	vect3f[3];
typedef mFloat32	vect4f[4];
typedef mFloat32	mat2f[2][2];
typedef mFloat32	mat3f[3][3];
typedef mFloat32	mat4f[4][4];
typedef struct {
	mInt32 x, y;
	mUint32 w, h;
} rect2i;

/* matrix types */
enum {
	MAT_ZERO = 0, /* zero matrix */
	MAT_IDENTITY,   /* identity matrix */
};


#ifdef __cplusplus
extern "C" {
#endif


#define VECTINIT2I(v,i) (v)[0]=i; (v)[1]=i
#define VECTINIT2F(v,f) (v)[0]=f; (v)[1]=f
#define VECTINIT3I(v,i) (v)[0]=i; (v)[1]=i; (v)[2]=i
#define VECTINIT3F(v,f) (v)[0]=f; (v)[1]=f; (v)[2]=f
#define VECTCOPY2I(dst,src) memcpy(dst, src, sizeof(vect2i))
#define VECTCOPY2F(dst,src) memcpy(dst, src, sizeof(vect2f))
#define VECTCOPY3I(dst,src) memcpy(dst, src, sizeof(vect3i))
#define VECTCOPY3F(dst,src) memcpy(dst, src, sizeof(vect3f))
#define VECTCOPY4F(dst,src) memcpy(dst, src, sizeof(vect4f))
#define MATCOPY2F(dst,src) memcpy(dst, src, sizeof(mat2f))
#define MATCOPY3F(dst,src) memcpy(dst, src, sizeof(mat3f))
#define MATCOPY4F(dst,src) memcpy(dst, src, sizeof(mat4f))
#define VECTADD3F(d,a,b) d[0]=a[0]+b[0]; d[1]=a[1]+b[1]; d[2]=a[2]+b[2]
#define VECTSUB3F(d,a,b) d[0]=a[0]-b[0]; d[1]=a[1]-b[1]; d[2]=a[2]-b[2]
/* vector scalar multiply */
#define VECTMULT3F(v,f) (v)[0] *= f; (v)[1] *= f; (v)[2] *= f
/* vector dot multiply */
#define VECTDOT3F(d,a,b) d[0]=a[0]*b[0]; d[1]=a[1]*b[1]; d[2]=a[2]*b[2]
/* vector cross multiply */
#define VECTCROSS3F(d,a,b) (d)[0] = (a)[1]*(b)[2] - (a)[2]*(b)[1]; \
			   (d)[1] = (a)[2]*(b)[0] - (a)[0]*(b)[2]; \
			   (d)[2] = (a)[0]*(b)[1] - (a)[1]*(b)[0]
/* vector dot product */
#define VECTDOTP3F(a,b) (a)[0]*(b)[0] + (a)[1]*(b)[1] + (a)[2]*(b)[2] 
int vectNorm3f(vect3f v); /* make vector "v" a unit vector */
void vectDump4f(vect4f v); /* print vector to stdout */
/*
 * initialize a matrix of the type "type" enumerated above.
 * use the data parameters depending on what type of matrix */
void matInit2f(mat2f mat, int type); 
void matInit3f(mat3f mat, int type); 
void matInit4f(mat4f mat, int type);
void matDump2f(mat2f mat); /* print matrix to stdout */
void matDump3f(mat3f mat); /* print matrix to stdout */
void matDump4f(mat4f mat); /* print matrix to stdout */
void matMult3f(mat3f dest, mat3f m1, mat3f m2); /* multiply matrices */
void matMult4f(mat4f dest, mat4f m1, mat4f m2); /* multiply matrices */
/*
 * Rotate the matrix "m" by degrees "rA"
 * around the vector (fX, fY, fZ)  */
void matRotate3f(mat3f m, float rA, float fX, float fY, float fZ);
void vectRotate3f(vect3f v, float rA, float fX, float fY, float fZ);
/*
 * transform vector "v". (multiply it by matrix "m") */
void vectTransform3f(vect3f dest, vect3f v, mat3f m);
void vectTransform4f(vect4f dest, vect4f v, mat4f m);

#define RECTCOPY2I(dst,src) memcpy(dst,src,sizeof(rect2i))
#define RECTINIT2I(r,ii) memset(r,ii,sizeof(rect2i))

#define FLOOR(x)	((int)(x) - ((x) < 0 && (x) != (int)(x)))
#define CEIL(x)		((int)(x) + ((x) > 0 && (x) != (int)(x)))
#define CLAMP(x,a,b)	((x) <= (a) ? (a) : ((x) >= (b) ? (b) : (x)))
#ifndef DEG2RAD
#  define DEG2RAD 0.017453293
#endif
#ifndef RAD2DEG
#  define RAD2DEG 57.29577951
#endif

#ifdef WIN32
#  define cosf(f) (float)cos((double)(f))
#endif

/*! linear interpolation */
#define LERP(x0,x1,t)	((x0) + (t)*((x1)-(x0)))
#define LERP3FV(vDst,v0,v1,t) \
    (vDst)[0] = LERP((v0)[0], (v1)[0], t);\
    (vDst)[1] = LERP((v0)[1], (v1)[1], t);\
    (vDst)[2] = LERP((v0)[2], (v1)[2], t)
/*! cosine interpolation between "v0" and "v1" at time "t" */
int cerp3fv(vect3f vDst, vect3f v0, vect3f v1, float t);

#ifdef __cplusplus
}
#endif

#endif

