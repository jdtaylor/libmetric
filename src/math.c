
/*
 * A Common Math Library
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */


#include <stdio.h>
#include <math.h>
#include <string.h>
#include <metric/common.h>
#include <metric/math.h>


int vectNorm3f(vect3f v)
{
	float n = sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
	if(n == 0)
		return -1;
	v[0] /= n;
	v[1] /= n;
	v[2] /= n;
	return 1;
}

/*! cosine interpolation between v0 and v1 */
int cerp3fv(vect3f vDst, vect3f v0, vect3f v1, float t)
{
	return 1;
}

void matDump2f(mat2f mat)
{
	int i, j;
	for(i = 0; i < 2; i++) {
		for(j = 0; j < 2; j++) {
			printf("%f ", mat[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void matDump3f(mat3f mat)
{
	int i, j;
	for(i = 0; i < 3; i++) {
		for(j = 0; j < 3; j++) {
			printf("%f ", mat[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void matDump4f(mat4f mat)
{
	int i, j;
	for(i = 0; i < 4; i++) {
		for(j = 0; j < 4; j++) {
			printf("%f ", mat[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void vectDump4f(vect4f v)
{
	printf("(%f, %f, %f, %f)\n", v[0],v[1],v[2],v[3]);
}

/* fills matrix with template according to "type" */
void matInit2f(mat2f mat, int type)
{
	switch(type)
	{
	case MAT_ZERO:
		memset(mat, 0, sizeof(mat2f));
		break;
	case MAT_IDENTITY:
		memset(mat, 0, sizeof(mat2f));
		mat[0][0] = 1.0f;
		mat[1][1] = 1.0f;
		break;
	default:
		VLOG_MSG(LOG_LOW,"matInit2f - unknown matrix type: %d", type);
		break;
	}
}

/* fills matrix with template according to "type" */
void matInit3f(mat3f mat, int type)
{
	switch(type)
	{
	case MAT_ZERO:
		memset(mat, 0, sizeof(mat3f));
		break;
	case MAT_IDENTITY:
		memset(mat, 0, sizeof(mat3f));
		mat[0][0] = 1.0f;
		mat[1][1] = 1.0f;
		mat[2][2] = 1.0f;
		break;
	default:
		VLOG_MSG(LOG_LOW,"matInit3f - unknown matrix type: %d", type);
		break;
	}
}

void matInit4f(mat4f mat, int type)
{
	switch(type)
	{
	case MAT_ZERO:
		memset(mat, 0, sizeof(mat4f));
		break;
	case MAT_IDENTITY:
		memset(mat, 0, sizeof(mat4f));
		mat[0][0] = 1.0f;
		mat[1][1] = 1.0f;
		mat[2][2] = 1.0f;
		mat[3][3] = 1.0f;
		break;
	default:
		VLOG_MSG(LOG_LOW,"matInit4f - unknown matrix type: %d", type);
		break;
	}
}

void matMult3f(mat3f dest, mat3f m1, mat3f m2)
{
	int i;
	float m1a, m1b, m1c;
	for(i = 0; i < 3; i++) {
		m1a = m1[i][0];
		m1b = m1[i][1];
		m1c = m1[i][2];
		dest[i][0] = m1a*m2[0][0] + m1b*m2[1][0] + m1c*m2[2][0];
		dest[i][1] = m1a*m2[0][1] + m1b*m2[1][1] + m1c*m2[2][1];
		dest[i][2] = m1a*m2[0][2] + m1b*m2[1][2] + m1c*m2[2][2];
	}
}

/* multiplies two 4f matrices, puts result in "dest" */
void matMult4f(mat4f dest, mat4f m1, mat4f m2)
{
	int i;
	float m1a, m1b, m1c, m1d;
	for(i = 0; i < 4; i++) {
		m1a = m1[i][0];
		m1b = m1[i][1];
		m1c = m1[i][2];
		m1d = m1[i][3];
		dest[i][0] = m1a*m2[0][0] + m1b*m2[1][0] + m1c*m2[2][0] + m1d*m2[3][0];
		dest[i][1] = m1a*m2[0][1] + m1b*m2[1][1] + m1c*m2[2][1] + m1d*m2[3][1];
		dest[i][2] = m1a*m2[0][2] + m1b*m2[1][2] + m1c*m2[2][2] + m1d*m2[3][2];
		dest[i][3] = m1a*m2[0][3] + m1b*m2[1][3] + m1c*m2[2][3] + m1d*m2[3][3];
	}
}

/*
 * Adapted from Brian Paul's Mesa project matrix rotation,
 * Copyright (C) 1999-2001  Brian Paul   All Rights Reserved.
 * (licensed under MIT license at the time of November 2001)
 *
 * and Diana Gruber's "The Mathematics of the 3D Rotation Matrix"
 */
void matRotate3f(mat3f m, float rA, float fX, float fY, float fZ)
{
	float c,s,t,n;
	float xx,yy,zz,xy,yz,zx,xs,ys,zs;
	mat3f mR; /* temp rotation matrix */
	xx = fX*fX; yy = fY*fY; zz = fZ*fZ;
	n = sqrt(xx + yy + zz);
	if(n <= 1.0e-4) {
		LOG_MSG(LOG_LOW,"matInitRotate3f - invalid vector");
		return;
	}
	c = cos(rA * DEG2RAD);
	s = sin(rA * DEG2RAD);
	t = 1.0f - c;
	xy = fX*fY; yz = fY*fZ; zx = fX*fZ;
	xs = fX*s; ys = fY*s; zs = fZ*s;
	/* normalize the rotation axis vector */
	fX /= n;
	fY /= n;
	fZ /= n;
	/* construct the rotation matrix */
	mR[0][0] = t * xx + c;
	mR[0][1] = t * xy - zs;
	mR[0][2] = t * zx + ys;
	mR[1][0] = t * xy + zs;
	mR[1][1] = t * yy + c;
	mR[1][2] = t * yz - xs;
	mR[2][0] = t * zx - ys;
	mR[2][1] = t * yz + xs;
	mR[2][2] = t * zz + c;
	/* multiply incoming matrix by the temporary rotation matrix */
	matMult3f(m, m, mR);
}

/*
 * Adapted from Brian Paul's Mesa project matrix rotation,
 * Copyright (C) 1999-2001  Brian Paul   All Rights Reserved.
 * (licensed under MIT license at the time of November 2001)
 *
 * and Diana Gruber's "The Mathematics of the 3D Rotation Matrix"
 */
void vectRotate3f(vect3f v, float rA, float fX, float fY, float fZ)
{
	float c,s,t,n;
	float xx,yy,zz,xy,yz,zx,xs,ys,zs;
	mat3f mR; /* temp rotation matrix */
	xx = fX*fX; yy = fY*fY; zz = fZ*fZ;
	n = sqrt(xx + yy + zz);
	if(n <= 1.0e-4) {
		LOG_MSG(LOG_LOW,"matInitRotate3f - invalid vector");
		return;
	}
	c = cos(rA * DEG2RAD);
	s = sin(rA * DEG2RAD);
	t = 1.0f - c;
	xy = fX*fY; yz = fY*fZ; zx = fX*fZ;
	xs = fX*s; ys = fY*s; zs = fZ*s;
	/* normalize the rotation axis vector */
	fX /= n;
	fY /= n;
	fZ /= n;
	/* construct the rotation matrix */
	mR[0][0] = t * xx + c;
	mR[0][1] = t * xy - zs;
	mR[0][2] = t * zx + ys;
	mR[1][0] = t * xy + zs;
	mR[1][1] = t * yy + c;
	mR[1][2] = t * yz - xs;
	mR[2][0] = t * zx - ys;
	mR[2][1] = t * yz + xs;
	mR[2][2] = t * zz + c;
	/* multiply incoming vector by the temporary rotation matrix */
	vectTransform3f(v, v, mR);
}

void vectTransform3f(vect3f dest, vect3f v, mat3f m)
{
	vect3f vTmp; /* temp Vector */
	/*
	 * by copying, we allow the same vector to be passed
	 * in as both "dest" and "v" */
	VECTCOPY3F(vTmp, v);
	dest[0] = vTmp[0]*m[0][0] + vTmp[1]*m[0][1] + vTmp[2]*m[0][2];
	dest[1] = vTmp[0]*m[1][0] + vTmp[1]*m[1][1] + vTmp[2]*m[1][2];
	dest[2] = vTmp[0]*m[2][0] + vTmp[1]*m[2][1] + vTmp[2]*m[2][2];
}

/* multiplies one 4f vector by a 4f matrix.. aka. transformation */
void vectTransform4f(vect4f dest, vect4f v, mat4f m)
{
	vect4f vT;
	VECTCOPY4F(vT, v);
	dest[0] = vT[0]*m[0][0] + vT[1]*m[0][1] + vT[2]*m[0][2] + vT[3]*m[0][3];
	dest[1] = vT[0]*m[1][0] + vT[1]*m[1][1] + vT[2]*m[1][2] + vT[3]*m[1][3];
	dest[2] = vT[0]*m[2][0] + vT[1]*m[2][1] + vT[2]*m[2][2] + vT[3]*m[2][3];
	dest[3] = vT[0]*m[3][0] + vT[1]*m[3][1] + vT[2]*m[3][2] + vT[3]*m[3][3];
}

