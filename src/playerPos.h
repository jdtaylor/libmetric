
/*
 * Library for handling player positioning.
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#ifndef METRIC_PLAYER_POS_H
#define METRIC_PLAYER_POS_H

#include <metric/common.h>
#include <metric/math.h>


/*
 * absolute == relative to the ground
 * relative == relative to the player
 */
typedef struct {
	vect3f vLocation;  /* player's absolute location */
	vect3f vFacingDir; /* absolute direction the player is facing */
	vect3f vMovingDir; /* relative direction the player is moving */
	mUint64 time; /* no see */
} playerPos_t;


#ifdef __cplusplus
extern "C" {
#endif

int playerPosInit(playerPos_t *pPos);
int playerPosCopy(playerPos_t *dst, playerPos_t *src);
int playerPosByteSwap(playerPos_t *pPos);
int playerPosQueueCreate();
int playerPosQueueDelete(int queueId);
int playerPosEnqueue(int queueId, playerPos_t *pPos);
int playerPosDequeue(int queueId, playerPos_t *pPos);
int playerPosGetCurrent(int queueId, playerPos_t *pDst, mUint64 curTime);
playerPos_t *playerPosPeekFront(int queueId);
playerPos_t *playerPosPeekRear(int queueId);
int playerPosMove(playerPos_t *pPos, float speed);
int playerPosRotate(playerPos_t *pPos, vect3f vRot);

#ifdef __cplusplus
}
#endif

#endif

