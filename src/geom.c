
/*
 * A Common Geometry Library
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#include <math.h>
#include <metric/geom.h>

// number of polygons allocated initially when optimizing
#define POLY_INITIAL_COUNT 100
#define TRIPATCH_THRESHOLD 0.1f 

/*
 * "face" with arbitrary number of edges */
typedef struct {
	int mat;		/* 1 material */
	int nEdge;		/* 1 E (number of edges) */
	int maxEdge;
	int *pEdge;		/* E edges */
	int *edgeDirty;
	vect3f n;		/* 1 aproximate normal to the face */
} _poly_t;

static int _polyInit(_poly_t *pPoly);
static int _polyFree(_poly_t *pPoly);
static int _polyFromFace3ve(_poly_t*, geomObj3ve*, int, int);
static int _polyMergeFace3ve(_poly_t*, geomObj3ve*, int, int);
static int _polyMergePoly(_poly_t*, _poly_t*, geomObj3ve*, int, int, int);
static int _polyDump(_poly_t *pPoly); // for debugging
static int _geomObjDump(geomObj3ve *pObj); // for debugging

int geomObjInit3ve(geomObj3ve *pObj)
{
	pObj->nVert = 0;
	pObj->nTex = 0;
	pObj->nEdge = 0;
	pObj->nFace = 0;
	pObj->pVert = NULL;
	pObj->pTex = NULL;
	pObj->pEdge = NULL;
	pObj->pFace = NULL;
	return 1;
}

int geomObjFree3ve(geomObj3ve *pObj)
{
	if(pObj->pVert != NULL)
		FREE(pObj->pVert);
	if(pObj->pTex != NULL)
		FREE(pObj->pTex);
	if(pObj->pEdge != NULL)
		FREE(pObj->pEdge);
	if(pObj->pFace != NULL)
		FREE(pObj->pFace);
	pObj->nVert = 0;
	pObj->nTex = 0;
	pObj->nEdge = 0;
	pObj->nFace = 0;
	pObj->pVert = NULL;
	pObj->pTex = NULL;
	pObj->pEdge = NULL;
	pObj->pFace = NULL;
	return 1;
}

int geomObjMaterial3ve(geomObj3ve *pObj, int matId)
{
	int iFace;
	/*
	 * set every face material handle to "matId"
	 */
	for(iFace = 0; iFace < pObj->nFace; iFace++)
		pObj->pFace[iFace].mat = matId;
	return 1;
}


int geomObjMinimize3ve(geomObj3ve *pObj)
{
	int *faceDirty;
	int *edgeDirty;
	edge2v *pEdge; // keep screen clean
	face3ve *pFace;
	int nFace;
	float dotP; // scalar dot product
	int iEdge;
	int face0, face1; // 2 faces of an edge
	_poly_t *pPoly; // array of "maxPoly" polygons
	int nPoly; // number of polygons being used in "pPoly"
	int maxPoly; // number of currently allocated polygons
	faceDirty = (int*)CALLOC(pObj->nFace, sizeof(int));
	if(faceDirty == NULL) {
		LOG_MSG(LOG_SYS,"geomObjMinimize - CALLOC failed on faceDirty");
		return -1;
	}
	edgeDirty = (int*)CALLOC(pObj->nEdge, sizeof(int));
	if(edgeDirty == NULL) {
		LOG_MSG(LOG_SYS,"geomObjMinimize - CALLOC failed on edgeDirty");
		FREE(faceDirty);
		return -1;
	}
	maxPoly = POLY_INITIAL_COUNT;
	nPoly = 0;
	pPoly = (_poly_t*)MALLOC(sizeof(_poly_t) * maxPoly);
	if(pPoly == NULL) {
		LOG_MSG(LOG_SYS,"geomObjMinimize - MALLOC failed on pPoly");
		FREE(faceDirty);
		FREE(edgeDirty);
		return -1;
	}
	nFace = pObj->nFace;
	pEdge = pObj->pEdge;
	for(iEdge = 0; iEdge < pObj->nEdge; iEdge++)
	{
		if(edgeDirty[iEdge])
			continue;
		face0 = pEdge[iEdge].f[0];
		face1 = pEdge[iEdge].f[1];
		// make sure this edge has both faces present
		if(face0 < 0 || face1 < 0)
			continue;
		if(face0 >= nFace)
		{
			if(face1 >= nFace)
			{
				// face 0 is a poly, face 1 is a poly
				// ---
				// this edge is almost useless.  Need to test if
				// I need to merge the too polygons together.
				dotP = VECTDOTP3F(pPoly[face0-nFace].n,
						pPoly[face1-nFace].n);
				if(2.0f - (1.0f+dotP) < TRIPATCH_THRESHOLD) {
					// both polygons are similar in slope
					// Merge poly "face1" into poly "face0"
					_polyMergePoly(&pPoly[face0-nFace], &pPoly[face1-nFace],
							pObj, face0, face1, iEdge);
				}
				edgeDirty[iEdge] = TRUE;
			} else {
				if(faceDirty[face1])
					continue;
				// face 0 is a poly, face 1 is a face3ve
				// ---
				// face 0 has already been merged with a polygon, so
				// try to merge face 1 with face 0's polygon
				dotP = VECTDOTP3F(pPoly[face0-nFace].n, pFace[face1].n);
				if(2.0f - (1.0f+dotP) < TRIPATCH_THRESHOLD) {
					// face1 is similar in slope to it's adjacent polygon.
					// Merge it with the polygon
					_polyMergeFace3ve(&pPoly[face0-nFace], pObj, face0, iEdge);
				}
				edgeDirty[iEdge] = TRUE;
			}
		} else {
			if(faceDirty[face0])
				continue;
			if(face1 >= nFace) {
				// face 0 is a face3ve, face 1 is a poly
				// ---
				// face 1 is part of a polygon already, so I have it
				// try to merge face 0 with it.
				dotP = VECTDOTP3F(pFace[face0].n, pPoly[face1-nFace].n);
				if(2.0f - (1.0f+dotP) < TRIPATCH_THRESHOLD) {
					// face0 is similar in slope to it's adjacent polygon.
					// Merge it with the polygon
					_polyMergeFace3ve(&pPoly[face1-nFace], pObj, face1, iEdge);
				}
				edgeDirty[iEdge] = TRUE;
			} else {
				if(faceDirty[face1])
					continue;
				// face 0 is a face3ve, face 1 is a face3ve.
				// ---
				// Since both faces havn't been touched yet, I'm going
				// to make a new polygon and try swallow both faces into it
				// and set the faces to dirty if they were merged.
				dotP = VECTDOTP3F(pFace[face0].n, pFace[face1].n);
				// dotP should *hopefully* always be less than or equal to 1
				if(2.0f - (1.0f+dotP) < TRIPATCH_THRESHOLD) {
					// the two triangles have similar normals
					//
					if(nPoly >= maxPoly) {
						// need to allocate more polygon structs
					}
					_polyInit(&pPoly[nPoly]);
					_polyFromFace3ve(&pPoly[nPoly], pObj, nPoly+nFace, face0);
					_polyMergeFace3ve(&pPoly[nPoly], pObj, nPoly+nFace, iEdge);
					nPoly++;
				}
				edgeDirty[iEdge] = TRUE; // edge isn't important anymore
			}
		}
	}
	FREE(faceDirty);
	FREE(edgeDirty);
	//
	// we should have many polygons now.  Each should contain many
	// edge references in no particular order.  Since a polygon is
	// only defined by its outer edges, we need to sort all the edges
	// and remove (dirty) the duplicates.  Eh! It so happens that 
	// I have a function that does just that:
	//    for(i = 0; i < nPoly; i++)
	//	_polyCleanup(&pPoly[i]);
	FREE(pPoly);
	return 1;
}

int _polyInit(_poly_t *pPoly)
{
	pPoly->mat = -1;
	pPoly->nEdge = 0;
	pPoly->maxEdge = 0;
	pPoly->pEdge = NULL;
	pPoly->edgeDirty = NULL;
	VECTINIT3F(pPoly->n, 0.0f);
	return 1;
}

int _polyFree(_poly_t *pPoly)
{
	pPoly->mat = -1;
	pPoly->nEdge = 0;
	pPoly->maxEdge = 0;
	VECTINIT3F(pPoly->n, 0.0f);
	if(pPoly->pEdge != NULL) {
		FREE(pPoly->pEdge);
		pPoly->pEdge = NULL;
	}
	if(pPoly->edgeDirty != NULL) {
		FREE(pPoly->edgeDirty);
		pPoly->edgeDirty = NULL;
	}
	return 1;
}

int _polyFromFace3ve(_poly_t *pPoly, geomObj3ve *pObj, int polyId, int faceId)
{
	edge2v *pEdge;
	if(faceId < 0 || faceId >= pObj->nFace) {
		VLOG_MSG(LOG_LOW,"_polyFromFace3ve - geomObj's face %d invalid",
				faceId);
		return -1;
	}
	// this call will die, if the poly hasn't been initialized yet.
	_polyFree(pPoly);
	pPoly->maxEdge = 50;
	pPoly->nEdge = 0;
	pPoly->pEdge = (int*)MALLOC(sizeof(int) * pPoly->maxEdge);
	if(pPoly->pEdge == NULL) {
		LOG_MSG(LOG_SYS,"polyMergeFace3ve - MALLOC failed");
		_polyFree(pPoly);
		return -1;
	}
	pPoly->edgeDirty = (int*)CALLOC(pPoly->maxEdge, sizeof(int));
	if(pPoly->edgeDirty == NULL) {
		LOG_MSG(LOG_SYS,"polyMergeFace3ve - MALLOC failed");
		_polyFree(pPoly);
		return -1;
	}
	//
	// modify adjacent edges to reference the polygon instead
	// of the old face
	pEdge = &pObj->pEdge[pObj->pFace[faceId].e[0]];
	if(pEdge->f[0] == faceId) {
		if(pEdge->f[0] != -1)
			pEdge->f[0] = polyId;
	} else {
		if(pEdge->f[1] != -1)
			pEdge->f[1] = polyId;
	}
	pEdge = &pObj->pEdge[pObj->pFace[faceId].e[1]];
	if(pEdge->f[0] == faceId) {
		if(pEdge->f[0] != -1)
			pEdge->f[0] = polyId;
	} else {
		if(pEdge->f[1] != -1)
			pEdge->f[1] = polyId;
	}
	pEdge = &pObj->pEdge[pObj->pFace[faceId].e[2]];
	if(pEdge->f[0] == faceId) {
		if(pEdge->f[0] != -1)
			pEdge->f[0] = polyId;
	} else {
		if(pEdge->f[1] != -1)
			pEdge->f[1] = polyId;
	}
	// copy triangle edge information to the polygon
	pPoly->nEdge = 3;
	pPoly->pEdge[0] = pObj->pFace[faceId].e[0];
	pPoly->pEdge[1] = pObj->pFace[faceId].e[1];
	pPoly->pEdge[2] = pObj->pFace[faceId].e[2];
	VECTCOPY3F(pPoly->n, pObj->pFace[faceId].n);
	pPoly->mat = pObj->pFace[faceId].mat;
	return 1;
}

int _polyMergeFace3ve(_poly_t *pPoly, geomObj3ve *pObj, int poly, int edge)
{
	void *pBuf;
	edge2v *pEdge; // pointer to the edge that we are merging across
	face3ve *pFace; // pointer to the face we are merging
	int face; // id of the face we are merging
	int edge1, edge2; // two edges of "face" that aren't "edge"
	//
	// I will be adding 3 new edges to the polygon.
	// Check if we have enough space in the polygon for them.
	//
	if(pPoly->nEdge + 3 >= pPoly->maxEdge) {
		// increase memory for edges
		pBuf = REALLOC(pPoly->pEdge, sizeof(int) * pPoly->maxEdge * 2);
		if(pBuf == NULL) {
			LOG_MSG(LOG_SYS,"_polyMergeFace3ve - REALLOC failed");
			return -1;
		}
		pPoly->pEdge = (int*)pBuf;
		pPoly->maxEdge *= 2;
	}
	pEdge = &pObj->pEdge[edge];
	if(pEdge->f[0] == poly)
		face = pEdge->f[1];
	else
		face = pEdge->f[0];
	pFace = &pObj->pFace[face];
	//
	// copy triangle info to the polygon
	//
	// determine which two edges aren't in the polygon
	if(pFace->e[0] == edge) {
		edge1 = pFace->e[1];
		edge2 = pFace->e[2];
	}
	else
	{
		if(pFace->e[1] == edge) {
			edge1 = pFace->e[0];
			edge2 = pFace->e[2];
		}
		else
		{
			if(pFace->e[2] == edge) {
				edge1 = pFace->e[0];
				edge2 = pFace->e[1];
			} else {
				LOG_MSG(LOG_LOW,"_polyMergeFace3ve - edge mismatch!");
				return -1;
			}
		}
	}
	//
	// change the first edge's face reference to the polygon
	pEdge = &pObj->pEdge[edge1];
	if(pEdge->f[0] == face) {
		pEdge->f[0] = poly;
	} else {
		if(pEdge->f[1] == face) {
			pEdge->f[1] = poly;
		} else {
			LOG_MSG(LOG_LOW,"_polyMergeFace3ve - face mismatch!");
			return -1;
		}
	}
	//
	// change the second edge's face reference to the polygon
	pEdge = &pObj->pEdge[edge2];
	if(pEdge->f[0] == face) {
		pEdge->f[0] = poly;
	} else {
		if(pEdge->f[1] == face) {
			pEdge->f[1] = poly;
		} else {
			LOG_MSG(LOG_LOW,"_polyMergeFace3ve - face mismatch!");
			return -1;
		}
	}
	//
	// add the three edges of the face to the polygon
	// Duplicates are OK.  When the polygon is complete I will
	// take them out because they aren't needed.
	pPoly->pEdge[pPoly->nEdge] = edge; 
	pPoly->pEdge[pPoly->nEdge+1] = edge1;
	pPoly->pEdge[pPoly->nEdge+2] = edge2;
	//
	// average the normals of the polygon with that of the new face
	// TODO: this should probably be weighted more towards the polygon
	VECTADD3F(pPoly->n, pPoly->n, pFace->n);
	vectNorm3f(pPoly->n);
	pPoly->nEdge += 3;
	return 1;
}

int _polyMergePoly(_poly_t *poly1, _poly_t *poly2, geomObj3ve *pObj,
		int polyId1, int polyId2, int edge)
{
	int i;
	void *pBuf;
	int memIncr;
	int curEdge;
	edge2v *pEdge;
	pEdge = &pObj->pEdge[edge];
	//
	// polygon references are somewhere above the face references
	if(pEdge->f[0] < pObj->nFace || pEdge->f[1] < pObj->nFace) {
		LOG_MSG(LOG_LOW,"_polyMergePoly - edge isn't between 2 polygons");
		return -1;
	}
	//
	// adjust memory in polygon1 to fit the size of polygon2's data
	//
	if(poly1->nEdge + poly2->nEdge >= poly1->maxEdge) {
		// increase memory for edges
		memIncr = (poly1->nEdge + poly2->nEdge) * 2;
		pBuf = REALLOC(poly1->pEdge, sizeof(int) * memIncr);
		if(pBuf == NULL) {
			LOG_MSG(LOG_SYS,"_polyMergePoly - REALLOC failed");
			return -1;
		}
		poly1->pEdge = (int*)pBuf;
		poly1->maxEdge = memIncr;
	}
	//
	// touch every edge in poly2 and migrate it (the edge) to poly1.
	// Also modify any adjacent face references.
	for(i = 0; i < poly2->nEdge; i++)
	{
		curEdge = poly2->pEdge[i];
		pEdge = &pObj->pEdge[curEdge];
		if(pEdge->f[0] == polyId2) {
			pEdge->f[0] = polyId1;
		} else {
			if(pEdge->f[1] == polyId2) {
				pEdge->f[1] = polyId1;
			} else {
				LOG_MSG(LOG_LOW,"_polyMergePoly - poly2 has edge inconsistency");
				return -1;
			}
		}
		// add the edge to polygon1
		poly1->pEdge[poly1->nEdge + i] = poly2->pEdge[i];
	}
	//
	// average the normals of both polygons
	// TODO: this should probably be weighted more towards the polygon
	VECTADD3F(poly1->n, poly1->n, poly2->n);
	vectNorm3f(poly1->n);
	poly1->nEdge += poly2->nEdge;
	return 1;
}

int _polyDump(_poly_t *pPoly)
{
	int i;
	LOG_MSG(LOG_USER,"_poly_t Dump {");
	VLOG_MSG(LOG_USER,"\tmat: %d,  nEdge: %d", pPoly->mat, pPoly->nEdge);
	VLOG_MSG(LOG_USER,"\tmaxEdge: %d", pPoly->maxEdge);
	VLOG_MSG(LOG_USER,"\tnorm: (%.4f, %.4f, %.4f)",
			pPoly->n[0], pPoly->n[1], pPoly->n[2]);
	//
	// dump all edge references
	for(i = 0; i < pPoly->nEdge; i++) {
		VLOG_MSG(LOG_USER,"\tpEdge[%d] = %d", i, pPoly->pEdge[i]);
	}
	LOG_MSG(LOG_USER,"}");
	return 1;
}

int _geomObjDump(geomObj3ve *pObj)
{
	int i;
	LOG_MSG(LOG_USER,"geomObj3ve Dump {");
	VLOG_MSG(LOG_USER,"\tnVert: %d,  nTex: %d, nEdge: %d, nFace: %d",
			pObj->nVert, pObj->nTex, pObj->nEdge, pObj->nFace);
	//
	// dump all vertex coordinates
	for(i = 0; i < pObj->nVert; i++) {
		VLOG_MSG(LOG_USER,"\tpVert[%d] = (%.4f,%.4f,%.4f)", i,
				pObj->pVert[i][0], pObj->pVert[i][1], pObj->pVert[i][2]);
	}
	//
	// dump all texture coordinates
	for(i = 0; i < pObj->nTex; i++) {
		VLOG_MSG(LOG_USER,"\tpTex[%d] = (%.4f,%.4f,%.4f)", i,
				pObj->pTex[i][0], pObj->pTex[i][1], pObj->pTex[i][2]);
	}
	//
	// dump all edge references
	for(i = 0; i < pObj->nEdge; i++) {
		VLOG_MSG(LOG_USER,"\tpEdge[%d]: v=(%d,%d) f=(%d,%d)", i,
				pObj->pEdge[i].v[0], pObj->pEdge[i].v[1],
				pObj->pEdge[i].f[0], pObj->pEdge[i].f[1]);
	}
	//
	// dump all face references
	for(i = 0; i < pObj->nFace; i++) {
		VLOG_MSG(LOG_USER,"\tpFace[%d]: v=(%d,%d,%d) t=(%d,%d) e=(%d,%d,%d)", i,
				pObj->pFace[i].v[0], pObj->pFace[i].v[1], pObj->pFace[i].v[2],
				pObj->pFace[i].t[0], pObj->pFace[i].t[1],
				pObj->pFace[i].e[0], pObj->pFace[i].e[1], pObj->pFace[i].e[2]);
	}
	LOG_MSG(LOG_USER,"}");
	return 1;
}

static int the_US_Constitution_is()
{
	_polyDump(NULL);
	_geomObjDump(NULL);
	return 0;
}

