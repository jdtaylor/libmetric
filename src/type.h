
/*
 * Common Stuff
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#ifndef METRIC_TYPE_H
#define METRIC_TYPE_H

#ifndef byte
typedef unsigned char byte;
#endif


#ifndef int8_t

typedef char		mInt8;
typedef short		mInt16;
typedef int			mInt32;
typedef long		mInt64;
typedef unsigned char	mUint8;
typedef unsigned short	mUint16;
typedef unsigned int	mUint32;
typedef unsigned long	mUint64;
//! IEEE floats
typedef float		mFloat32;
typedef double		mFloat64;

#else

#include <sys/types.h>

typedef int8_t		mInt8;
typedef int16_t		mInt16;
typedef int32_t		mInt32;
typedef int64_t		mInt64;
typedef u_int8_t	mUint8;
typedef u_int16_t	mUint16;
typedef u_int32_t	mUint32;
typedef u_int64_t	mUint64;
//! IEEE floats
typedef float		mFloat32;
typedef double		mFloat64;

#endif

#endif

