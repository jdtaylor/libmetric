
/*
 * Library for PAK files
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#ifndef METRIC_PAK_H
#define METRIC_PAK_H

#include <sys/types.h>
#include <metric/common.h>
#include <metric/fs.h>

#define MAX_PAKNAME_LEN 32

/* gpak is a utility for creating/extracting pak files
 */
#ifdef GPAK
  #ifdef WIN32
    #error Currently, gpak is not suported in win32.
  #endif
#endif

typedef enum {
	NODE_BMP, NODE_PNG, NODE_JPG,
	NODE_3DS, NODE_ASE, NODE_WAV,
	NODE_MOD, NODE_FONT, NODE_GENERIC
} nodeType;


#ifdef __cplusplus
extern "C" {
#endif

#ifndef WIN32
/* these functions are only for gpak (POSIX only)*/
int pakCreateFromDir(char *filename,  /* filename of output pak file */
		     char *dir,  /* base dir name of source files/dirs */
		     char *pakName); /* generic name for the pak file */
int pakPrintContents(char *pakfile);
#endif

int pakOpen(char *pak_name);
int pakClose(int pakId);
 /* resName is the relative filename */
void *pakGetResource(int pakId, const char *resName,
		      int *len, nodeType *type);  /* return parameters */

#ifdef __cplusplus
}
#endif

#endif
