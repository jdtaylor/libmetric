
/*
 * Library for pseudo-random noise generation
 *
 * Copyright (c) 2001-2002,  James D. Taylor.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * contact James D. Taylor:  <james.d.taylor@gmail.com>
 */

#include <stdlib.h>
#include <math.h>
#include <metric/common.h>
#include <metric/noise.h>
#include <metric/math.h>

/*********************************************************
 * CREDIT:
 ****************************************************
 *
 * Perlin Noise Implementation 
 * Derived from Hugo Elias' tutorial:
 * http://freespace.virgin.net/hugo.elias/models/m_perlin.htm
 *
 * -----------------------------------------------------------
 *
 * vnoise/spline/vlattice ripped from the book:
 * "Texture & Modeling - A Procedural Approach";
 * specific code by Darwyn Peachey.
 *
 * thanks!
 *
 **********/


/* Coefficients of basis matrix. */
#define CR00     -0.5
#define CR01      1.5
#define CR02     -1.5
#define CR03      0.5
#define CR10      1.0
#define CR11     -2.5
#define CR12      2.0
#define CR13     -0.5
#define CR20     -0.5
#define CR21      0.0
#define CR22      0.5
#define CR23      0.0
#define CR30      0.0
#define CR31      1.0
#define CR32      0.0
#define CR33      0.0

#define TABSIZE		256
#define TABMASK		(TABSIZE-1)
#define PERM(x)		perm[(x)&TABMASK]
#define INDEX(ix,iy,iz)	PERM((ix)+PERM((iy)+PERM(iz)))
#define RANDMASK	0x7fffffff

/*
 * random() and srandom() don't port to WIN32
 */
#define RANDNBR		((rand() & RANDMASK)/(double) RANDMASK)
//#define RANDNBR	((random() & RANDMASK)/(double) RANDMASK)

static float interpolateCos(float a, float b, float x);
static float interpolateLinear(float a, float b, float x);
static float interpolateCubic(float v0, float v1,
			      float v2, float v3, float x);
static float spline(float x, int nknots, float *knot);
static void valueTabInit(int seed);
static float vlattice(int ix, int iy, int iz);
static float valueTab[TABSIZE];
static unsigned char perm[TABSIZE];

#define PERSISTENCE 0.1f
#define NUM_OCTAVES 4

float noise1(int x)
{
    x = (x << 13) ^ x;
    return (1.0f - ((x*(x*x*15731+789221)+1376312589) & 0x7fffffff) / 
	    1073741824.0f);
}

float noise2(int x, int y)
{
    int n = x + y * 57;
    n = (n << 13) ^ n;
    return (1.0f - (float)((n*(n*n*15731+789221)+1376312589) & 0x7fffffff) / 
	    1073741824.0f);
}

float interpolateLinear(float a, float b, float x)
{
    return (a*(1.0f-x) + (b*x));
}

float interpolateCos(float a, float b, float x)
{
    float ft = x * 3.1415927f;
    ft = (1.0f-cosf(ft)) * 0.5f;

    return a * (1.0f-ft) + (b*ft);
}

float interpolateCubic(float v0, float v1, float v2, float v3, float x)
{
    float P = (v3 - v2) - (v0 - v1);
    float Q = (v0 - v1) - P;
    float R = v2 - v0;
    float S = v1;

    return P*x*x*x + Q*x*x + R*x + S;
}

float noiseSmooth1(int x)
{
    return noise1(x)/2 + noise1(x-1)/4 + noise1(x+1)/4;
}

float noiseInterp1(float x)
{
    int intX = (int)x;
    float frac = x - intX;

    float v1 = noiseSmooth1(intX);
    float v2 = noiseSmooth1(intX + 1);

    return interpolateCos(v1, v2, frac);
}

float noisePerlin1(float x)
{
    float total = 0;
    float p = PERSISTENCE;
    float freq, ampl;
    int n = NUM_OCTAVES;
    int i;

    for(i = 0; i < n; i++)
    {
	freq = (float)pow(2, i);
	ampl = (float)pow(p, i);
	total += noiseInterp1(x * freq) * ampl;
    }
    return total;
}

float noiseSmooth2(int x, int y)
{
    float corners = (noise2(x-1, y-1) +
					noise2(x+1, y+1) +
					noise2(x-1, y+1) +
					noise2(x+1, y-1)) / 16;
    float sides = (noise2(x-1, y  ) +
					noise2(x+1, y  ) +
					noise2(x  , y-1) +
					noise2(x  , y+1)) / 8;
    float center = noise2(x, y) / 4;

    return corners + sides + center;
}

float noiseInterp2(float x, float y)
{
    int iX = (int)x;
    int iY = (int)y;
    float fX = x - iX;
    float fY = y - iY;

    float v1 = noiseSmooth2(iX, iY);
    float v2 = noiseSmooth2(iX+1, iY);
    float v3 = noiseSmooth2(iX, iY+1);
    float v4 = noiseSmooth2(iX+1, iY+1);
    float i1 = interpolateCos(v1, v2, fX);
    float i2 = interpolateCos(v3, v4, fX);

    return interpolateCos(i1, i2, fY);
}

float noisePerlin2(float x, float y)
{
    float total = 0;
    float p = PERSISTENCE;
    float freq, ampl;
    int n = NUM_OCTAVES;
    int i;

	for(i = 0; i < n; i++)
	{
		freq = (float)pow(2, i);
		ampl = (float)pow(p, i);
		total += noiseInterp2(x*freq, y*freq) * ampl;
	}

	return total;
}

//! Spline in the ICE machine!
float spline(float x, int nknots, float *knot)
{
	int span;
	int nspans = nknots - 3;
	float c0, c1, c2, c3;  /* coefficients of the cubic.*/

	if(nspans < 1) {  /* illegal */
		LOG_MSG(LOG_LOW,"spline - Spline has too few knots.");
		return 0;
	}

	/* Find the appropriate 4-point span of the spline. */
	x = CLAMP(x, 0, 1) * nspans;
	span = (int) x;
	if (span >= nknots - 3)
		span = nknots - 3;
	x -= span;
	knot += span;

	/* Evaluate the span cubic at x using Horner's rule. */
	c3 = CR00*knot[0] + CR01*knot[1]
		+ CR02*knot[2] + CR03*knot[3];
	c2 = CR10*knot[0] + CR11*knot[1]
		+ CR12*knot[2] + CR13*knot[3];
	c1 = CR20*knot[0] + CR21*knot[1]
		+ CR22*knot[2] + CR23*knot[3];
	c0 = CR30*knot[0] + CR31*knot[1]
		+ CR32*knot[2] + CR33*knot[3];

	return ((c3*x + c2)*x + c1)*x + c0;
}


float vnoise(float x, float y, float z)
{
	int ix, iy, iz;
	int i, j, k;
	float fx, fy, fz;
	float xknots[4], yknots[4], zknots[4];
	static int initialized = 0;

	if(!initialized) {
		valueTabInit(665);
		initialized = 1;
	}

	ix = FLOOR(x);
	fx = x - ix;

	iy = FLOOR(y);
	fy = y - iy;

	iz = FLOOR(z);
	fz = z - iz;

	for (k = -1; k <= 2; k++) {
		for (j = -1; j <= 2; j++) {
			for (i = -1; i <= 2; i++)
				xknots[i+1] = vlattice(ix+i,iy+j,iz+k);
			yknots[j+1] = spline(fx, 4, xknots);
		}
		zknots[k+1] = spline(fy, 4, yknots);
	}
	return spline(fz, 4, zknots);
}

void valueTabInit(int seed)
{
	float *table = valueTab;
	int i;

	// srandom() doesn't port to WIN32
	//srandom(seed);
	srand(seed);
	for(i = 0; i < TABSIZE; i++)
		*table++ = 1. - 2.*RANDNBR;
}

float vlattice(int ix, int iy, int iz)
{
	return valueTab[INDEX(ix,iy,iz)];
}

static int the_metric_system_is()
{
	interpolateLinear(0.0f, 0.0f, 0.0f);
	interpolateCubic(0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
	return 1;
}


