dnl Process this file with autoconf to produce a configure script.

dnl random file so autoconf can sync/verify the source dir
AC_INIT(src/common.h)
AC_CONFIG_AUX_DIR(config)

dnl Set various version strings - taken gratefully from the SDL sources
#
# Making releases:
#   METRIC_MICRO_VERSION += 1;
#   METRIC_INTERFACE_AGE += 1;
#   METRIC_BINARY_AGE += 1;
# if any functions have been added, set METRIC_INTERFACE_AGE to 0.
# if backwards compatibility has been broken,
# set METRIC_BINARY_AGE and METRIC_INTERFACE_AGE to 0.
#
METRIC_MAJOR_VERSION=0
METRIC_MINOR_VERSION=0
METRIC_MICRO_VERSION=1
METRIC_INTERFACE_AGE=0
METRIC_BINARY_AGE=0
METRIC_VERSION=$METRIC_MAJOR_VERSION.$METRIC_MINOR_VERSION.$METRIC_MICRO_VERSION

AC_SUBST(METRIC_MAJOR_VERSION)
AC_SUBST(METRIC_MINOR_VERSION)
AC_SUBST(METRIC_MICRO_VERSION)
AC_SUBST(METRIC_INTERFACE_AGE)
AC_SUBST(METRIC_BINARY_AGE)
AC_SUBST(METRIC_VERSION)

# libtool versioning
LT_RELEASE=$METRIC_MAJOR_VERSION.$METRIC_MINOR_VERSION
LT_CURRENT=`expr $METRIC_MICRO_VERSION - $METRIC_INTERFACE_AGE`
LT_REVISION=$METRIC_INTERFACE_AGE
LT_AGE=`expr $METRIC_BINARY_AGE - $METRIC_INTERFACE_AGE`

AC_SUBST(LT_RELEASE)
AC_SUBST(LT_CURRENT)
AC_SUBST(LT_REVISION)
AC_SUBST(LT_AGE)

dnl Detect the canonical host and target build environment
AC_CANONICAL_HOST
AC_CANONICAL_TARGET

AM_INIT_AUTOMAKE(metric, $METRIC_VERSION)
AM_CONFIG_HEADER(config.h)
AM_MAINTAINER_MODE

AC_PROG_CC
AC_PROG_CPP
AC_LIBTOOL_WIN32_DLL
AC_PROG_LIBTOOL
AC_PROG_LN_S
AC_ISC_POSIX
AC_PROG_LIBTOOL
AC_PROG_INSTALL

AC_ARG_WITH(pkgconfigdir,
[  --with-pkgconfigdir=DIR pkgconfig file in DIR @<:@LIBDIR/pkgconfig@:>@],
[pkgconfigdir=$withval],
[pkgconfigdir='${libdir}/pkgconfig'])
AC_SUBST(pkgconfigdir)

AC_ARG_WITH(gpak,
[  --without-gpak          Don't build pak file utility],
		with_gpak=$withval, with_gpak=yes)

AC_ARG_ENABLE(threads,
[  --disable-threads       Thread Library Support [default=enabled]],
		enable_threads=$enableval, enable_threads=yes)

AC_ARG_ENABLE(pipes,
[  --disable-pipes         Pipe Library Support [default=enabled]],
              enable_pipes=$enableval, enable_pipes=yes)

AC_ARG_ENABLE(sdlnet,
[  --enable-sdlnet         Use SDL_net for sockets.  [default=no]],
              enable_sdlnet=$enableval, enable_sdlnet=no)

if test x$with_gpak = xyes; then
    GPAK_BIN="gpak"
fi
AC_SUBST(GPAK_BIN)
AM_CONDITIONAL(WITH_GPAK, test "x$with_gpak" = "xyes")

if test x$enable_threads = xyes; then
    dnl Replace `main' with a function in -lpthread:
    AC_CHECK_LIB(pthread, main)
    USE_THREADS="-DUSE_THREADS"
    `sed 's/\/\*##sub##\*\//\/\*##sub##\*\/\n#define HAVE_METRIC_THREADS 1/' src/common.h > .common.h; mv .common.h src/common.h`
   dnl AC_DEFINE(HAVE_METRIC_THREADS, 1, [whether libmetric has thread support])
else
    `sed 's/#define HAVE_METRIC_THREADS 1//' src/common.h > .common.h; mv .common.h src/common.h`
fi
AC_SUBST(USE_THREADS)
AM_CONDITIONAL(WITH_THREADS, test "x$enable_threads" = "xyes")

if test x$enable_pipes = xyes; then
    USE_PIPES="-DUSE_PIPES"
    `sed 's/\/\*##sub##\*\//\/\*##sub##\*\/\n#define HAVE_METRIC_PIPES 1/' src/common.h > .common.h; mv .common.h src/common.h`
dnl    AC_DEFINE(HAVE_METRIC_PIPES, 1, [whether libmetric has pipe support])
else
    `sed 's/#define HAVE_METRIC_PIPES 1//' src/common.h > .common.h; mv .common.h src/common.h`
fi
AC_SUBST(USE_PIPES)
AM_CONDITIONAL(WITH_PIPES, test "x$enable_pipes" = "xyes")

if test x$enable_sdlnet = xyes; then
    AC_CHECK_LIB(SDL, main)
    AC_CHECK_LIB(SDL_net, main)
    USE_SDLNET=" `sdl-config --cflags` "
    `sed 's/\/\*##sub##\*\//\/\*##sub##\*\/\n#define HAVE_METRIC_SDLNET 1/' src/common.h > .common.h; mv .common.h src/common.h`
    `sed 's/\/\*##sub##\*\//\/\*##sub##\*\/\n#define HAVE_METRIC_SDL 1/' src/common.h > .common.h; mv .common.h src/common.h`
else
    `sed 's/#define HAVE_METRIC_SDLNET 1//' src/common.h > .common.h; mv .common.h src/common.h`
    `sed 's/#define HAVE_METRIC_SDL 1//' src/common.h > .common.h; mv .common.h src/common.h`
fi
AC_SUBST(USE_SDLNET)

dnl Checks for libraries.
dnl Replace `main' with a function in -ldl:
AC_CHECK_LIB(dl, main)
dnl Replace `main' with a function in -lm:
AC_CHECK_LIB(m, main)

# Checks for header files.
AC_HEADER_STDC
AC_HEADER_DIRENT
AC_CHECK_HEADERS(fcntl.h malloc.h sys/time.h unistd.h)

# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_TYPE_PID_T
AC_TYPE_SIZE_T
AC_STRUCT_ST_BLKSIZE
AC_HEADER_TIME

# Checks for library functions.
AC_FUNC_MMAP
#AC_FUNC_FORK
AC_CHECK_FUNCS(getcwd gettimeofday select socket strcspn)

AC_SUBST(METRIC_EXTRADIRS)

AC_CONFIG_FILES([ metric.pc Makefile src/Makefile ])
AC_OUTPUT

